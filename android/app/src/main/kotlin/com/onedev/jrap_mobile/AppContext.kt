package com.onedev.jrap_mobile

import android.app.Application
import io.flutter.app.FlutterApplication
import com.vk.sdk.VKSdk

/**
 * application context
 */
class AppContext : FlutterApplication() {

    /**
     * singleton for application
     */
    companion object : Application()

    override fun onCreate() {
        super.onCreate()
        AppContext.attachBaseContext(this)
        this.vkInit()
    }

    fun vkInit()
    {
        VKSdk.initialize(this)
    }
}