package com.onedev.jrap_mobile.player


/**
 *
 * PlayMode of MusicPlayer
 *
 * please ensure that this enum values index the same as Dart enum PlayMode
 *
 * @author YangBin
 */
enum class ShuffleMode {
    NoShuffle,

    Shuffle;
}

enum class RepeatMode {

    //REPEAT_MODE_OFF
    Off,

    //REPEAT_MODE_ONE
    Single,

    //REPEAT_MODE_ALL.
    Sequence;

}