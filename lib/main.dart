import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/service_locator.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/store.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitDown,
    DeviceOrientation.portraitUp,
  ]);

  var store = await createStore();
  setupLocator();

  runApp(new AppRootWidget(store));
}

class AppRootWidget extends StatelessWidget  {
  final Store<AppState> store;

  AppRootWidget(this.store);

  Widget build(BuildContext context)
  {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color.fromRGBO(31, 41, 51, 1),
      statusBarBrightness: Brightness.dark,
      systemNavigationBarColor: Colors.red,
      systemNavigationBarDividerColor: Colors.red,
    ));

    return  StoreProvider<AppState>(
      store: store,
      child: MaterialApp(
        title: 'Jrap',
        debugShowCheckedModeBanner: false,
        theme:  ThemeData(
          primaryColor: Color.fromRGBO(31, 41, 51, 1),
          accentColor: Color.fromRGBO(222,103,59,1),
          scaffoldBackgroundColor: Colors.grey[300],
          backgroundColor: Color.fromRGBO(31, 41, 51, 1),
          /*appBarTheme: AppBarTheme(
              textTheme: TextTheme(title: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Color.fromRGBO(222,103,59,1)))
          ),*/
        ),
        navigatorKey: Keys.navKey[KeyType.main],
        onGenerateRoute: AppRoutes.getRoute,
      ),
    );
  }

}

