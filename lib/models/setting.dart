class SettingUser
{
  String SettingUserId;
  String SettingName;
  String SettingVal;

  SettingUser({this.SettingUserId, this.SettingName, this.SettingVal });

  factory SettingUser.fromMap(Map<String, dynamic> data) => SettingUser(
    SettingUserId: data["SettingUserId"],
    SettingName: data["SettingName"],
    SettingVal: data["SettingVal"],
  );

}