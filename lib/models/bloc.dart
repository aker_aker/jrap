import 'dart:async';

class CounterProvider {
  bool count = false;

  void increaseCount(state){
    count= state;
  }
}

class BottomDragBloc{
  final counterController = StreamController(); // create a StreamController
  final CounterProvider provider = CounterProvider(); // create an instance of our CounterProvider

  Stream get getCount => counterController.stream; // create a getter for our stream

  void updateCount(bool state) {
    if(provider.count != state) {
      provider.increaseCount(state); // call the method to increase our count in the provider
      counterController.sink.add(provider.count); // add the count to our sink
    }
  }

  void dispose() {
    counterController.close(); // close our StreamController
  }
}

final bloc = BottomDragBloc();