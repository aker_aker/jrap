
class Profile{
  String firstName;
  String lastName;
  String imageUrl;

  Profile({this.firstName, this.lastName, this.imageUrl});

  factory Profile.fromMap(Map<String, dynamic> json) => new Profile(
    firstName: json["first_name"] ?? "",
    lastName: json["last_name"] ?? "",
    imageUrl: json["photo_400_orig"] ?? "",
  );

  Map<String, dynamic> toJson() => {
    "first_name": firstName,
    "last_name": lastName,
    "imageUrl": imageUrl,
  };
}