import 'package:jrap_mobile/utils/enums.dart';

class InfoQueue {
  final PlayType playType;
  final int Id;
  final Object  playObject;

  InfoQueue({this.playType, this.Id, this.playObject});

}