
import 'package:jrap_mobile/utils/config.dart';

class Artist {
  int ArtistId;
  String ArtistNickName;
  bool IsImage;
  ArtistInfo Info;

  Artist({ this.ArtistId, this.Info, this.ArtistNickName, this.IsImage});

  String get ImageUrl{
    return "${Config.api}/api/artist/${this.ArtistId}/image";
  }

  String get BriefInfo
  {
    return (Info!=null) ? "${Info.CountAlbum} альбомов \u2022 ${Info.CountSong} трэков" : "";
  }

  static Artist fromMapPlayer(Map map) {
    return Artist(
      ArtistId: map['ArtistId'],
      ArtistNickName:map['ArtistNickName'],
    );
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (ArtistId != null) {
      map['ArtistId'] = ArtistId;
    }
    map['ArtistNickName'] = ArtistNickName;
    map['IsImage'] = IsImage;
    return map;
  }


  factory Artist.fromMap(Map<String, dynamic> data) => new Artist(
    ArtistId: data['ArtistId'],
    ArtistNickName:data['ArtistNickName'],
    IsImage: (data['IsImage']==1),
  );

  factory Artist.fromJson(Map<String, dynamic> parsedJson)
  {
    return  Artist(
      ArtistId: parsedJson['ArtistId'],
      ArtistNickName:parsedJson['ArtistNickName'],
      IsImage:parsedJson['IsImage'],
      Info: parsedJson['Info'] == null ? ArtistInfo() : ArtistInfo.fromJson(parsedJson['Info']),
    );
  }
}

class ArtistInfo{
   int  Id;
   int  CountSong;
   int  CountAlbum;
   int  CountSingls;
   int  CountFeat;
   bool IsImage;

   ArtistInfo({this.Id, this.IsImage, this.CountAlbum, this.CountFeat, this.CountSingls, this.CountSong});

   int get cntSection {
     return ((CountAlbum > 0) ? 2 : 0)  +
            ((CountSingls > 0) ? 2 : 0) +
            ((CountFeat > 0) ? 2 : 0);
   }

   factory ArtistInfo.fromJson(Map<String, dynamic> parsedJson){

     return  ArtistInfo(
       Id:parsedJson['Id'],
       CountSong:parsedJson['CountSong'],
       CountAlbum: parsedJson['CountAlbum'],
       CountSingls: parsedJson['CountSingls'],
       CountFeat: parsedJson['CountFeat'],
       IsImage: parsedJson['IsImage'],
     );
   }
}