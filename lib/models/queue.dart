import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/part.dart';

class Queue {

  List<Song> songs;
  Song song;
  int index;

  Queue(){
    songs = List<Song>();
  }

  bool get isQueue{
    return songs!= null && songs.length > 0;
  }

  bool Contains(Song song)
  {
    if(!isQueue) return false;

    return songs.any((i) => i.SongId == song.SongId);
  }

  AddSongs(List<Song> songs){
    this.songs.addAll(songs);
  }

  SelectSong(Song song) async{
    var _song = songs.where((i) => i.SongId == song.SongId).first;
    int _index = songs.indexOf(_song);
    if(_index > 0){
      index  = _index;
      this.song = _song;
    }
    else{
      //Ничего не делаем
    }
  }

  SelectSongByIndex(int index) async{
    if(songs.length - 1  > index){
      this.index  = index;
      this.song = songs[index];
      //await quiet.play(music: this.song);
    }
    else{
      //Ничего не делаем
    }
  }
}