
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/task_info.dart';

class DownloadSong{
  Song song;
  Album album;
  //TaskInfo task;

  DownloadSong({ this.song, this.album});

  factory DownloadSong.fromMap(Map<String, dynamic> data)
  {
    Song _song = Song(
      SongId: data["SongId"],
      AlbumId: data["AlbumId"],
      SongDuration: data["SongDuration"],
      SongIndex: data["SongIndex"],
      SongName: data["SongName"],
      SongOwnerId: data["SongOwnerId"],
      SongUrl: data["SongUrl"],
      SongVKId: data["SongVKId"],
    );

    Album _album = Album.fromMap(data);

    /*TaskInfo _task = TaskInfo(
      SongId: data["SongId"],
      link: data["url"],
      id: data["id"],
      name: data["name"],
      progress: data["progress"],
      status: DownloadTaskStatus.from(data["status"]),
      taskId: data["task_id"]
    );*/

    return DownloadSong(song: _song, album: _album,);
  }
}