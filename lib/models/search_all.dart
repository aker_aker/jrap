import 'dart:core';

import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/search.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/utils/enums.dart';

class SearchAll
{
  /// <summary>
  /// Популярные ртистов
  /// </summary>
  List<SearchBlock> items;

  SearchAll({this.items}){
    if(this.items == null)
      {
        items = List<SearchBlock>();
      }
  }

  factory SearchAll.fromJson(Map<String, dynamic> parsedJson){

    var result = List<SearchBlock>();

    var list = parsedJson['artist'] as List;
    List<Artist> _list_artist =(list!=null) ? list.map((i) => Artist.fromJson(i)).toList() : [];
    result.add(SearchBlock<Artist>(_list_artist, MainObjType.artist, 2));

    var list_albums = parsedJson['album'] as List;
    List<Album> _list_albums = (list_albums!=null) ? list_albums.map((i) => Album.fromJson(i)).toList() : [];
    result.add(SearchBlock<Album>(_list_albums, MainObjType.album, 2));

    var list_singls = parsedJson['singl'] as List;
    List<Album> _list_singls = (list_singls!=null) ? list_singls.map((i) => Album.fromJson(i)).toList() : [];
    result.add(SearchBlock<Album>(_list_singls, MainObjType.singl, 2));

    return SearchAll(
        items: result,
    );
  }
}