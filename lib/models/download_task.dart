
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/task_info.dart';

class DownloadTaskInfo{
  Song song;
  Album album;
  TaskInfo task;

  Function(TaskInfo) deleteTask;
  Function(TaskInfo) cancelTask;
  Function(TaskInfo) resumeTask;
  Function(TaskInfo) pauseTask;
  Function(TaskInfo) retryTask;

  DownloadTaskInfo({this.deleteTask,
  this.cancelTask,
  this.pauseTask,
  this.resumeTask,
  this.retryTask,
  this.song, this.album, this.task});

  factory DownloadTaskInfo.fromMap(Map<String, dynamic> data)
  {
    Song _song = Song(
      SongId: data["SongId"],
      AlbumId: data["AlbumId"],
      SongDuration: data["SongDuration"],
      SongIndex: data["SongIndex"],
      SongName: data["SongName"],
      SongOwnerId: data["SongOwnerId"],
      SongUrl: data["SongUrl"],
      SongVKId: data["SongVKId"],
    );

    Album _album = Album(
        AlbumId: data["AlbumId"],
        AlbumName: data["AlbumName"],
        AlbumYear: data["AlbumYear"],
        AlbumType: data["AlbumType"],
        AlbumTracks: data["AlbumTracks"]
    );

    TaskInfo _task = TaskInfo(
      SongId: data["SongId"],
      link: data["url"],
      id: data["id"],
      name: data["name"],
      progress: data["progress"],
      status: DownloadTaskStatus.from(data["status"]),
      taskId: data["task_id"]
    );

    return DownloadTaskInfo(song: _song, album: _album, task: _task );
  }
}