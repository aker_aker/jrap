
import 'package:jrap_mobile/utils/enums.dart';
import 'dart:core';

class SearchBlock<T> {
  List<T> items;
  int cntAll;
  int cntVisibleBlock;
  MainObjType typeBlock;

  String get header {
    if(typeBlock==MainObjType.album) return "Альбомы";
    else if(typeBlock==MainObjType.artist) return "Артисты";
    else if(typeBlock==MainObjType.singl) return "Синглы";
    else if(typeBlock==MainObjType.song) return "Треки";

    return "";
  }

  String get titleMore {
    var str = "";
    if(typeBlock==MainObjType.album) str="альбом";
    else if(typeBlock==MainObjType.artist) str= "артист";
    else if(typeBlock==MainObjType.singl) str= "сингл";
    else if(typeBlock==MainObjType.song) str= "трек";

    if(plurals(cntAll)==1) return str;
    else if(plurals(cntAll)==2) return "$strа";
    else if(plurals(cntAll)==5) return "$strов";

    return str;
  }

  int plurals(int n){
    if (n==0) return 0;
    n = n.abs() % 100;
    int n1 = n % 10;
    if (n > 10 && n < 20) return 5;
    if (n1 > 1 && n1 < 5) return 2;
    if (n1 == 1) return 1;
    return 5;
  }

  SearchBlock(this.items, this.typeBlock, int cntVisibleBlock){
    cntAll = this.items?.length ?? 0;
    this.cntVisibleBlock = cntVisibleBlock ?? 0;
  }
}

