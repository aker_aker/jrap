
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/utils/config.dart';

class Album{
  int AlbumType;
  String AlbumName;

  int AlbumId ;
  int AlbumYear;
  int AlbumLike ;
  int AlbumTracks;

  List<Artist> AlbumArtists;
  num RowNum;
  bool IsPlaying;
  bool IsImage;

  Album({this.IsImage, this.AlbumArtists,this.AlbumId,this.AlbumLike,this.AlbumName,this.AlbumTracks,this.AlbumType,this.AlbumYear,this.IsPlaying,this.RowNum});

  String get ArtistTitle
  {
    String result = "";
    if (AlbumArtists != null && AlbumArtists.length > 0)
    {
      result = AlbumArtists.map((item) => item.ArtistNickName).join(", ");
      //AlbumArtists.forEach((element) => result = "$result ${element.ArtistNickName}");
    }

    return result.trim();
  }

  String get ImageUrl{
    return "${Config.api}/api/album/${this.AlbumId}/image";
  }

  factory Album.fromMap(Map<String, dynamic> data) => new Album(
    AlbumType:data['AlbumType'],
    AlbumName:data['AlbumName'],
    AlbumId:data['AlbumId'],
    AlbumYear:data['AlbumYear'],
    AlbumTracks:data['AlbumTracks'],
    IsPlaying: true,
  );

  factory Album.fromJson(Map<String, dynamic> parsedJson)
  {
    var list = parsedJson['AlbumArtists'] as List;
    List<Artist> _list_artist = (list!=null) ? list.map((i) => Artist.fromJson(i)).toList() : [];

    return  Album(
      AlbumType:parsedJson['AlbumType'],
      AlbumName:parsedJson['AlbumName'],
      AlbumId:parsedJson['AlbumId'],
      AlbumYear:parsedJson['AlbumYear'],
      AlbumLike:parsedJson['AlbumLike'],
      AlbumTracks:parsedJson['AlbumTracks'],
      RowNum:parsedJson['RowNum'],
      IsPlaying: parsedJson['IsPlaying'],
      //IsImage: parsedJson['IsImage'],
      AlbumArtists: _list_artist,
    );
  }
}