import 'dart:core';

import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/song.dart';

class DetailArtist
{
   String type;
   int id;

   /// <summary>
   /// Популярные трэки
   /// </summary>
   List<Song> tracks;


  /// <summary>
  /// Популярные трэки
  /// </summary>
   List<Album> singls_top;

  /// <summary>
  /// Популярные альбомы
  /// </summary>
   List<Album> albums_top;

  /// <summary>
  /// Последний альбом
  /// </summary>
  Album last;

  /// <summary>
  /// Похожие артисты
  /// </summary>
  List<Artist> artist_related;

   /// <summary>
   /// Информаци об артисте
   /// </summary>
   Artist artist;

  DetailArtist({this.artist_related, this.albums_top, this.singls_top, this.id, this.type, this.artist});


  factory DetailArtist.fromJson(Map<String, dynamic> parsedJson){
     var list = parsedJson['artist_related'] as List;
     List<Artist> _list_artist = (list!=null) ? list.map((i) => Artist.fromJson(i)).toList() : [];

     var list_albums = parsedJson['albums_top'] as List;
     List<Album> _list_albums = (list_albums!=null) ? list_albums.map((i) => Album.fromJson(i)).toList() : [];

     var list_singls = parsedJson['singls_top'] as List;
     List<Album> _list_singls = (list_singls!=null) ? list_singls.map((i) => Album.fromJson(i)).toList() : [];

     var _artist = Artist.fromJson(parsedJson['artist']);

     return DetailArtist(
       albums_top : _list_albums,
       singls_top: _list_singls,
       artist_related: _list_artist,
       id: parsedJson['id'] as int,
       type: parsedJson['type'] as String,
       artist: _artist,
     );
   }
}