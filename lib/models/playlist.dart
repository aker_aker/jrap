class Playlist
{
  int Id;
  String Name;
  int CountTracks;
  int Duration;
  bool IsPlaying;

  Playlist({this.Duration, this.IsPlaying, this.CountTracks, this.Id, this.Name});

  String get TracksTitle{
    return "$CountTracks треков";
  }

  String get AllDuration
  {
    /*var time =  new DateTime().fr. .FromSeconds(Duration);
    return time.ToString(@"mm\:ss");*/
    return "";
  }

  bool get IsTracks
  {
    return (CountTracks != 0);
  }

  factory Playlist.fromMap(Map<String, dynamic> data) =>  Playlist(
    Id: data["id"],
    Name: data["name"],
    CountTracks: data["counttracks"],
    Duration: data["duration"],
    IsPlaying: false,
  );

}