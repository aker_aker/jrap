
class Progress{
  final Duration duration;
  final Duration position;

  Progress({ this.duration, this.position });
}