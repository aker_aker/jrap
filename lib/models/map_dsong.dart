class MapDSong{
  int SongId;
  String SongUrl;

  MapDSong({this.SongId, this.SongUrl});

  factory MapDSong.fromMap(Map<String, dynamic> data) => new MapDSong(
    SongId: data['SongId'],
    SongUrl:data['SongUrl'],
  );
}