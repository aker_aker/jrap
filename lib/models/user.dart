
class User{
  String userId;
  String firstName;
  String lastName;
  String email;
  String imageUrl;
  String cookie;
  DateTime expire;
  bool isActive;
  String secret;
  String token;

  User({this.userId, this.firstName, this.lastName, this.email,this.token,
      this.imageUrl, this.isActive, this.expire, this.cookie, this.secret});

  factory User.fromMap(Map<String, dynamic> json) => new User(
    userId: json["userId"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    email: json["email"],
    isActive: json["isActive"] == 1 ? true : false,
    imageUrl: json["imageUrl"],
    cookie: json["cookie"],
    secret: json["secret"],
    token: json["token"],
    //expire:  DateTime.fromMillisecondsSinceEpoch(int.parse(json["expire"]), isUtc: true),
  );

  Map<String, dynamic> toJson() => {
    "first_name": firstName,
    "last_name": lastName,
    "userId": userId,
    "email": email,
    "isActive": isActive,
    "imageUrl": imageUrl,
    "cookie": cookie,
    "secret": secret,
    "token": token,
  };
}