import 'package:flutter_downloader/flutter_downloader.dart';


class TaskInfo {
  final String name;
  final String link;
  final num SongId;

  num id;
  String taskId;
  int progress = 0;
  String localpath;

  DownloadTaskStatus status = DownloadTaskStatus.undefined;

  TaskInfo({this.name, this.link, this.SongId, this.id, this.taskId, this.progress, this.status, this.localpath});
}

class ItemHolder {
  final String name;
  final TaskInfo task;

  ItemHolder({this.name, this.task});
}