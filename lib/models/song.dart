import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/utils/config.dart';

class Song {
  String SongName;
  num SongId;
  int SongIndex;
  num SongOwnerId;
  num SongVKId;
  String SongHash;
  int SongDuration;
  List<Artist> SongArtists;
  String SongUrl;
  int AlbumId;

  //Показывае что песня скачена
  bool IsLocalStore;

  //Показывает что песня скачивается
  bool IsDownloading;

  Song(
      {this.SongId,
      this.SongArtists,
      this.SongDuration,
      this.SongIndex,
      this.SongName,
      this.SongOwnerId,
      this.SongVKId,
      this.SongHash,
      this.AlbumId,
      this.SongUrl,
      this.IsLocalStore = false,
      this.IsDownloading = false});

  bool get IsVkLoad {
    return (this.SongVKId != null &&
        this.SongOwnerId != null &&
        this.SongHash != null);
  }

  bool get IsUrl {
    return (this.SongUrl!=null && this.SongUrl.isNotEmpty);
  }

  bool get IsPlay2 {
    return IsVkLoad && IsUrl;
  }

  String get ArtistTitle
  {
    String result = "";
    if (SongArtists != null && SongArtists.length > 0)
    {
      result = SongArtists.map((item) => item.ArtistNickName).join(", ");
    }

    return result.trim();
  }


  String get AlbymImageUrl => "${Config.api}/api/album/${this.AlbumId}/image";

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    if (SongId != null) {
      map['SongId'] = SongId;
    }
    map['SongIndex'] = SongIndex;
    map['SongOwnerId'] = SongOwnerId;
    map['SongVKId'] = SongVKId;
    map['SongDuration'] = SongDuration;
    map['SongName'] = SongName;
    map['SongUrl'] = SongUrl;
    map['SongHash'] = SongHash;
    map['AlbumId'] = AlbumId;
    map['IsLocalStore'] = IsLocalStore;
    return map;
  }

  static Song fromMapPlayer(Map map) {
    if (map == null) {
      return null;
    }
    return Song(
        SongName: map['SongName'],
        SongId: map['SongId'],
        SongDuration: map['SongDuration'],
        SongIndex: map['SongIndex'],
        SongOwnerId: map['SongOwnerId'],
        SongVKId: map['SongVKId'],
        AlbumId: map['AlbumId'],
        SongUrl: map['SongUrl'],
        SongHash: map['SongHash'],
        IsLocalStore: (map['IsLocalStore']!=null) ? (map['IsLocalStore']==1) ? true : false : false ,
        //SongArtists: (map["artist"] as List).cast<Map>().map(Artist.fromMapPlayer).toList()
        );
  }

  /*Map toMap() {
    return {
      "SongId": SongId,
      "SongName": SongName,
      "SongUrl": SongUrl,
      "SongDuration":SongDuration,
      "SongIndex":SongIndex,
      "SongOwnerId": SongOwnerId,
      "SongVKId":SongVKId,
      "AlbumId":AlbumId,
      "SongArtists": SongArtists.map((e) => e.toMap()).toList()
    };
  }*/

  factory Song.fromMap(Map<String, dynamic> data) {
    return Song(
      SongName: data['SongName'],
      SongId: data['SongId'],
      SongDuration: data['SongDuration'],
      SongIndex: data['SongIndex'],
      SongOwnerId: data['SongOwnerId'],
      SongVKId: data['SongVKId'],
      AlbumId: data['AlbumId'],
      SongUrl: data['SongUrl'],
      SongHash: data['SongHash'],
      IsLocalStore: (data['IsLocalStore']!=null) ? (data['IsLocalStore']==1) ? true : false : false ,
    );
  }

  factory Song.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['SongArtists'] as List;
    List<Artist> _list_artist = list.map((i) => Artist.fromJson(i)).toList();

    return Song(
        SongName: parsedJson['SongName'],
        SongId: parsedJson['SongId'],
        SongArtists: _list_artist,
        SongDuration: parsedJson['SongDuration'],
        SongIndex: parsedJson['SongIndex'],
        SongOwnerId: parsedJson['SongOwnerId'],
        SongVKId: parsedJson['SongVKId'],
        AlbumId: parsedJson['AlbumId'],
        SongUrl: null,
        IsLocalStore: (parsedJson['IsLocalStore']!=null) ? (parsedJson['IsLocalStore']==1) ? true : false : false ,
        SongHash: parsedJson["SongHash"]);
  }
}
