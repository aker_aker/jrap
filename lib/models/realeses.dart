
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';

class Realeses{

   List<Album> singls;
   List<Album> albums;
   List<Artist> artists;

   Realeses({
      this.singls,
      this.albums,
      this.artists
   });

   factory Realeses.fromJson(Map<String, dynamic> parsedJson){
     var list = parsedJson['singls'] as List;
     List<Album> _list_singls = list.map((i) => Album.fromJson(i)).toList();

     var list2 = parsedJson['albums'] as List;
     List<Album> _list_albums = list2.map((i) => Album.fromJson(i)).toList();

     var list3 = parsedJson['artists'] as List;
     List<Artist> _list_artis = list3.map((i) => Artist.fromJson(i)).toList();

     return  Realeses(
          singls: _list_singls ??  List<Album>(),
          albums : _list_albums ??  List<Album>(),
          artists : _list_artis ??  List<Artist>()
      );
   }
}