import 'package:flutter/material.dart';
import 'package:jrap_mobile/pages/albums_detail.dart';
import 'package:jrap_mobile/pages/artists_albums.dart';
import 'package:jrap_mobile/pages/artists_details.dart';
import 'package:jrap_mobile/pages/artists_singls.dart';
import 'package:jrap_mobile/pages/artists_songs.dart';
import 'package:jrap_mobile/pages/download.dart';
import 'package:jrap_mobile/pages/login.dart';
import 'package:jrap_mobile/pages/overlay.dart';
import 'package:jrap_mobile/pages/playlist.dart';
import 'package:jrap_mobile/pages/playlist_detail.dart';
import 'package:jrap_mobile/pages/search.dart';
import 'package:jrap_mobile/pages/settings.dart';
import 'package:jrap_mobile/pages/home.dart';
import 'package:jrap_mobile/pages/tasks.dart';
import 'package:jrap_mobile/utils/keys.dart';

/*Route<dynamic> generateRoute(RouteSettings settings) {

}*/

class AppRoutes {
  static const main = "/";
  static const login = "/login";
  static const home = "/home";

  static const realeses = "/realeses";
  static const albums = "/albums";
  static const singls = "/singls";
    static const album_detail = "/album_detail";
  static const artists = "/artists";
    static const artist_detail = "/artist_detail";
    static const artist_detail_albums = "/artist_detail_albums";
    static const artist_detail_singls = "/artist_detail_singls";
    static const artist_detail_tracks = "/artist_detail_tracks";

  static const playlists = "/playlists";
    static const playlist_detail = "/playlist_detail";
  static const download = "/download";
  static const tasks = "/tasks";
  static const search = "/search";
  static const setting = "/settings";

  static const overlay = "/";

  /*static final Map<String, WidgetBuilder> routes = {
    login: (context) => LoginPage(),
    home: (context) => HomePage(),
    albums: (context) => AlbumsPage(),
    album_detail: (context) => AlbumDetailPage(),
    singls: (context) => SinglsPage(),
    artists: (context) => ArtistsPage(),
    realeses: (context) => ReleasesPage(),
    playlists: (context) => PlaylistsPage(),
    artist_detail: (context) => ArtistDetailPage(),
  };*/

  static  Navigator createOverScreen() {
    return Navigator(
        key: Keys.navKey[KeyType.over],
        initialRoute: AppRoutes.overlay,
        onGenerateRoute: getRouteOver,
    );
  }

  static Route getRouteOver(RouteSettings settings) {
    switch (settings.name) {
      case overlay:
        return _buildRoute(settings, OverlayNavigator());
      case playlists:
        return _buildRoute(settings, PlaylistsPage());
      case download:
        return _buildRoute(settings, DownloadPage());
      case tasks:
        return _buildRoute(settings, TasksPage());
      case search:
        return _buildRoute(settings, SearchPage());
      case setting:
        return _buildRoute(settings, SettingsPage());
      case album_detail:
        return _buildNoAnimationRoute(settings, AlbumDetailPage(album: settings.arguments));
      case artist_detail:
        return _buildNoAnimationRoute(settings, ArtistDetailPage(artist: settings.arguments));
      case playlist_detail:
        return _buildNoAnimationRoute(settings, PlaylistDetailPage(playlist: settings.arguments));
      case artist_detail_albums:
        return _buildNoAnimationRoute(settings, ArtistAlbumsPage(artist: settings.arguments));
      case artist_detail_singls:
        return _buildNoAnimationRoute(settings, ArtistSinglsPage(artist: settings.arguments));
      case artist_detail_tracks:
        return _buildNoAnimationRoute(settings, ArtistSongsPage(artist: settings.arguments));
      default:
        return _buildRoute(settings, OverlayNavigator());
    }
  }

  static Route getRoute(RouteSettings settings) {

    switch (settings.name) {
      case login:
        return _buildRoute(settings, LoginPage(), maintainState: false);
      case home:
        return _buildRoute(settings, HomePage(), maintainState: false);
      default:
        return _buildRoute(settings, LoginPage(), maintainState: false);
    }
  }

  static MaterialPageRoute<dynamic> _buildRoute(RouteSettings settings, Widget page, {bool maintainState = true, bool fullscreenDialog = false}) =>
      new MaterialPageRoute(
        settings: settings,
        builder: (BuildContext context) => page,
        maintainState: maintainState,
        fullscreenDialog: fullscreenDialog,
      );

  static NoAnimationMaterialPageRoute<dynamic> _buildNoAnimationRoute(RouteSettings settings, Widget page, {bool maintainState = true, bool fullscreenDialog = false}) =>
      new NoAnimationMaterialPageRoute(
        settings: settings,
        builder: (BuildContext context) => page,
        maintainState: maintainState,
        fullscreenDialog: fullscreenDialog,
      );
}

class NoAnimationMaterialPageRoute<T> extends MaterialPageRoute<T> {
  NoAnimationMaterialPageRoute({
    @required WidgetBuilder builder,
    RouteSettings settings,
    bool maintainState = true,
    bool fullscreenDialog = false,
  }) : super(
            builder: builder,
            maintainState: maintainState,
            settings: settings,
            fullscreenDialog: fullscreenDialog);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}
