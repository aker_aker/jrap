
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/detail_artist.dart';
import 'package:jrap_mobile/models/profile.dart';
import 'package:jrap_mobile/models/realeses.dart';
import 'package:jrap_mobile/models/search_all.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/user.dart';
import 'package:jrap_mobile/utils/config.dart';
import 'package:jrap_mobile/utils/decoder.dart';
import 'package:vk_login_plugin/vk_login_plugin.dart';
import 'package:cookie_jar/cookie_jar.dart';

class NetworkUtil {

  static NetworkUtil _instance = new NetworkUtil.internal();

  final Duration _timeout = Duration(seconds: 10);

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();

  Future<String> _getBody(String url) async{
    try {
      print("START NETWORK $url");
      http.Response response = await http.get(
          url,
      ).timeout(_timeout);

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
      return  response.body;

    } on TimeoutException catch (e) {
      print('Timeout');
    } on SocketException catch (_) {
      print("SOCLET ERROR");
    } on Error catch (e) {
      print('Error: $e');
    }

    return null;
  }

  Future<String> _postBody(String url, {Map<String, String> headers}) async{
    try {
      http.Response response = await http.post(
          url,
          headers: headers,
      ).timeout(_timeout);

      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }

      return  response.body;

    } on TimeoutException catch (e) {
      print('Timeout');
    } on SocketException catch (_) {
      print("SOCLET ERROR");
    } on Error catch (e) {
      print('Error: $e');
    }

    return null;
  }

  Future getAuth() async{
    String url =  "https://oauth.vk.com/authorize?client_id=6932437&redirect_uri=https%3A%2F%2Foauth.vk.com%2Fblank.html&response_type=token&scope=208902&v=5.21&state=&revoke=1&nohttps=1&display=mobile";
    String res = await _postBody(url);
    if(res == null) return null;

  }

  Future getAuth2() async{
    String url =  "https://login.vk.com/?act=grant_access&client_id=6932437&settings=208902&redirect_uri=https%3A%2F%2Foauth.vk.com%2Fblank.html&response_type=token&group_ids=&token_type=1&v=5.21&state=&display=mobile&ip_h=ac40b91420ca9038f0&hash=1572590441_ece2ebdb7273fbd21c&https=1";
    String res = await _postBody(url);
    if(res == null) return null;

  }

  Future<Realeses> getRelease() async {
    String url =  "${Config.api}/api/new/all";
    String res = await _getBody(url);
    if(res == null) return null;

    return  Realeses.fromJson(_decoder.convert(res));
  }

  Future<List<Album>> getAlbums(int page) async {
    String url =  "${Config.api}/api/album/page/$page";
    String res = await _getBody(url);
    if(res == null) return null;

    var list = _decoder.convert(res) as List;
    List<Album> _list = list.map((i) => Album.fromJson(i)).toList();
    return _list;
  }

  Future<List<Album>> getSingls(int page) async{
    String url =  "${Config.api}/api/single/page/$page";
    String res = await _getBody(url);
    if(res == null) return null;

    var list = _decoder.convert(res) as List;
    List<Album> _list = list.map((i) => Album.fromJson(i)).toList();
    return _list;
  }

  Future<List<Artist>> getArtist(int page) async{
    String url =  "${Config.api}/api/artist/page/$page";
    String res = await _getBody(url);
    if(res == null) return null;

    var list = _decoder.convert(res) as List;
    List<Artist> _list = list.map((i) => Artist.fromJson(i)).toList();
    return _list;
  }

  Future<List<Song>> getAlbumTracks(int AlbumId) async{
    String url =  "${Config.api}/api/album/$AlbumId/track";
    String res = await _getBody(url);
    if(res == null) return [];

    var list = _decoder.convert(res) as List;
    List<Song> _list = list.map((i) => Song.fromJson(i)).toList();
    return _list;
  }


  Future<DetailArtist> getArtistDetail(int ArtistId) async{
    String url =  "${Config.api}/api/artist/$ArtistId/detail";
    String res = await _getBody(url);
    if(res == null) return null;

    return DetailArtist.fromJson(_decoder.convert(res));
  }

  Future<List<Album>> getArtistAlbums(int ArtistId) async{
    String url =  "${Config.api}/api/artist/$ArtistId/album";
    String res = await _getBody(url);
    if(res == null) return null;

    var list = _decoder.convert(res) as List;
    List<Album> _list = list.map((i) => Album.fromJson(i)).toList();
    return _list;
  }

  Future<List<Album>> getArtistSingls(int ArtistId) async{
    String url =  "${Config.api}/api/artist/$ArtistId/single";
    String res = await _getBody(url);
    if(res == null) return null;

    var list = _decoder.convert(res) as List;
    List<Album> _list = list.map((i) => Album.fromJson(i)).toList();
    return _list;
  }

  Future<List<Song>> getArtistSongs(int ArtistId) async{
    String url =  "${Config.api}/api/artist/$ArtistId/songs";
    String res = await _getBody(url);
    if(res == null) return null;

    var list = _decoder.convert(res) as List;
    List<Song> _list = list.map((i) => Song.fromJson(i)).toList();
    return _list;
  }

  Future<SearchAll> search(String txt) async {
    String url =  "${Config.api}/api/search/$txt";
    String res = await _getBody(url);
    if(res == null) return null;

    return SearchAll.fromJson(_decoder.convert(res));
  }

  Future<Map<num, String>> getVkAudio(String audios, String cookie) async {
    String url =  "https://vk.com/al_audio.php?act=reload_audio&al=1&ids="+audios;

    String res = await _postBody(url, headers: {'Cookie': 'remixsid=$cookie'});
    if(res == null) return null;

    Map<num, String> result = new Map<num, String>();
    var stringContent = res.replaceAll("<!--","");
    try {
      dynamic json = jsonDecode(stringContent);
      List<dynamic> j = json["payload"][1][0];
      for (var i = 0; i < j.length; i++) {
        if (!result.containsKey(j[i][0]))
          result[j[i][0]] = decode(j[i][2]);
      }
    }
    catch(ex){}

    return result;
  }

  Future<Profile> getVkUser(User user) async {
    String url =  "https://api.vk.com/method/users.get?user_ids=${user.userId}&fields=photo_400_orig&name_case=Nom&v=5.21&access_token=${user.token}";
    String res = await _postBody(url);
    if(res == null) return null;

    try {
      dynamic json = jsonDecode(res);
      return Profile.fromMap(json["response"][0]);
    }
    catch(ex){
      return null;
    }
  }

  Future setSongLike(String clientId, int SongId) {
    String url =  "${Config.api}/api/artist/like?clientId=$clientId&SongId=$SongId";

    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
    });
  }

  Future setSongDisLike(String clientId, int SongId) {
    String url =  "${Config.api}/api/artist/like?clientId=$clientId&SongId=$SongId";

    return http.get(url).then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;
      if (statusCode < 200 || statusCode > 400 || json == null) {
        throw new Exception("Error while fetching data");
      }
    });
  }
}