import 'dart:io';
import 'dart:async';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/profile.dart';
import 'package:jrap_mobile/models/setting.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/task_info.dart';
import 'package:jrap_mobile/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';
import 'package:vk_login_plugin/vk_login_plugin.dart';

class DatabaseClient {

  static final DatabaseClient _instance = new DatabaseClient._internal();

  factory DatabaseClient() => _instance;

  DatabaseClient._internal();

  Database _db;
  Song song;

  Future<Database> _init() async {

    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, 'database.db');
    //await deleteDatabase(path);
    return await openDatabase(
      path,
      version: 1,
      onCreate: _create,
    );
  }

  Future<Database> get _database async {
    if (_db == null) {
      _db = await _init();
    }

    return _db;
  }

  void close() async {
    Database instance = await _database;
    instance.close();
  }

  Future _create(Database db, int version) async {
    await db.execute("""
      CREATE TABLE IF NOT EXISTS songs(id NUMBER,title TEXT,duration NUMBER,albumArt TEXT,album TEXT,uri TEXT,artist TEXT,albumId NUMBER,isFav number NOT NULL default 0,timestamp number,count number not null default 0)
      """);
    await db.execute("""
      CREATE TABLE IF NOT EXISTS recents(id integer primary key autoincrement,title TEXT,duration NUMBER,albumArt TEXT,album TEXT,uri TEXT,artist TEXT,albumId NUMBER)
      """);

    await db.execute("""
      CREATE TABLE IF NOT EXISTS users(
          userId TEXT,
          firstName TEXT,
          lastName TEXT,
          email TEXT,
          token TEXT,
          secret,TEXT,
          imageUrl TEXT,
          cookie TEXT,
          expire NUMBER,
          isActive BIT NOT NULL default 0
        )
      """);

    await db.execute("""
      CREATE TABLE IF NOT EXISTS playlists(
          id integer primary key autoincrement,
          name TEXT
        )
      """);

    await db.execute("""
      INSERT INTO playlists(name) VALUES ('Мне нравится')
      """);

    //Добавляем таблицу с песнями
    await db.execute("""
      CREATE TABLE IF NOT EXISTS song(
          SongId integer,
          SongName TEXT,
          SongDuration NUMBER,
          SongIndex integer,
          SongOwnerId NUMBER,
          SongVKId NUMBER,
          SongUrl TEXT,
          SongHash TEXT,
          AlbumId integer,
          UNIQUE(SongId)
        )
      """);

    //Добавляем таблицу с альбомами
    await db.execute("""
      CREATE TABLE IF NOT EXISTS album(
          AlbumId integer,
          AlbumName TEXT,
          AlbumYear integer,          
          AlbumType integer,
          AlbumTracks integer,
          UNIQUE(AlbumId)
        )
      """);

    //Добавляем таблицу с артистами
    await db.execute("""
      CREATE TABLE IF NOT EXISTS artist(
          ArtistId integer,
          ArtistNickName TEXT,
          IsImage BIT NOT NULL default 0,    
          UNIQUE(ArtistId)     
        )
      """);

    //Добавляем таблицу связки песен с плелистом
    await db.execute("""
      CREATE TABLE IF NOT EXISTS song_by_playlist(
          PlaylistId integer,
          SongId integer,
          UNIQUE(PlaylistId,SongId)
        )
      """);


    //Добавляем таблицу связки песен с артистом
    await db.execute("""
      CREATE TABLE IF NOT EXISTS song_by_artist(
          ArtistId integer,
          SongId integer,
          UNIQUE(SongId,ArtistId)
        )
      """);

    //Добавляем таблицу связки альбома с артистом
    await db.execute("""
      CREATE TABLE IF NOT EXISTS album_by_artist(
          AlbumId integer,
          ArtistId integer,
          UNIQUE(AlbumId,ArtistId)
        )
      """);

    //таблица заданий на скачку
    await db.execute("""
        CREATE TABLE IF NOT EXISTS task_download (          
        `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
        `task_id`	VARCHAR ( 256 ),
        `url`	TEXT,
        `status`	INTEGER DEFAULT 0,
        `progress`	INTEGER DEFAULT 0,
        `file_name`	TEXT,
        `path`	TEXT,
         SongId integer,        
         AlbumId integer
        )
     """);

    //добавляем таблицу скаченных песене(SosngUrl - это путь до локальной папки)
    await db.execute("""
        CREATE TABLE IF NOT EXISTS song_download (          
         SongId  integer,        
         AlbumId integer,
         SongUrl TEXT
        )
     """);

    //настрйоки
    await db.execute("""
        CREATE TABLE IF NOT EXISTS setting (          
          SettingUserId TEXT,
          SettingName TEXT,
          SettingVal TEXT
        )
     """);
  }

  //Settings

  Future<List<SettingUser>> getSettings(String userId) async {
    Database db = await _database;
    var res = await db.query("setting", where: "SettingUserId = ?", whereArgs: [userId]);
    if(res.isEmpty) {
      db.insert("setting", { "SettingUserId": userId,  "SettingName": "store", "SettingVal": "store" });
      db.insert("setting", { "SettingUserId": userId,  "SettingName": "push", "SettingVal": "1" });
      db.insert("setting", { "SettingUserId": userId,  "SettingName": "sort", "SettingVal": "1" });
      db.insert("setting", { "SettingUserId": userId,  "SettingName": "mode", "SettingVal": "1" });
      res = await db.query("setting", where: "SettingUserId = ?", whereArgs: [userId]);
    }

    return  res.map((c) => SettingUser.fromMap(c)).toList();
  }

  Future<bool> updateSettings(SettingUser item) async {
    Database db = await _database;
    var res = await db.update("setting", {"SettingVal": item.SettingVal},  where: "SettingName = ? and SettingUserId = ?", whereArgs: [item.SettingName, item.SettingUserId]);
    return res > 0;
  }

  //Settings

  //User

  Future<User> getUser() async {
    Database db = await _database;
    var res = await db.query("users", where: "isActive = ?", whereArgs: [1]);
    return res.isNotEmpty ? User.fromMap(res.first) : null ;
  }

  Future<bool> isActiveUser(int userId, int date) async {
    Database db = await _database;
    var res = await db.query("users", where: "isActive = ? and userId = ? and expire > ?", whereArgs: [1, userId, date] );
    return res.isNotEmpty;
  }

  Future<bool> logOutUser() async {
    Database db = await _database;
    var res = await db.update("users", {
      "isActive":  false,
    }, where: "isActive = ?", whereArgs: [1]);
    return res > 0;
  }

 Future<bool> saveUserToke(VKAccessToken token) async {
    Database db = await _database;
    var res = await db.query("users", where: "userId = ?", whereArgs: [token.userId] );
    var _result = 0;
    try {
      if (res.isNotEmpty) {
        _result = await db.update("users", {
          "secret": token.secret,
          "cookie": token.cookies,
          "expire": token.expiresIn.millisecond,
          "isActive": true,
          "token": token.token
        }, where: "userId = ?", whereArgs: [token.userId]);
      }
      else
        _result = await db.insert("users", {
          "secret": token.secret,
          "cookie": token.cookies,
          "expire": token.expiresIn.millisecond,
          "isActive": true,
          "token": token.token,
          "userId": token.userId,
          "email": token.email
        });
    }
    catch(e){
    }

    return (_result > 0);
  }

  Future<bool> updateProfile(Profile profile, String userId) async {
    Database db = await _database;
    var _result = 0;
    try {
        _result = await db.update("users", {
          "firstName": profile.firstName,
          "lastName": profile.lastName,
          "imageUrl": profile.imageUrl,
        }, where: "userId = ?", whereArgs: [userId]);
    }
    catch(e){
    }

    return (_result > 0);
  }
  //User

  //region PLAYLIST

  getPlaylist() async {
    Database db = await _database;
    var query = "select t.id, t.name, count(t1.SongId) as counttracks,sum(t2.SongDuration) as duration from playlists t "
        "left outer join song_by_playlist t1 on t.id = t1.PlaylistId "
        "left outer join song t2 on t1.SongId = t2.SongId "
        "group by t.id, t.name";
    var res = await db.rawQuery(query);
    List<Playlist> list = res.isNotEmpty ? res.map((c) => Playlist.fromMap(c)).toList() : [];
    return list;
  }

  addPlaylist(String name) async{
    Database db = await _database;
    var res = await db.rawInsert(
        "INSERT INTO playlists (name) VALUES ('$name')");
    return res;

  }

  updatePlaylist(int id,String name) async{
    Database db = await _database;
    var res = await db.update("playlists", {"name": name}, where: "id = ?", whereArgs: [id]);
    return res;

  }

  deletePlaylist(int id) async{
    Database db = await _database;
    var res = await db.delete("playlists", where: "id = ?", whereArgs: [id]);
    return res;
  }

  /*songByPlaylist(int id) async{

  }*/

  Future<int> _getCount(String sql) async {
    Database db = await _database;
    return Sqflite.firstIntValue(await db.rawQuery(sql));
  }

  Future<int> _saveSong(Song song) async {
    Database db = await _database;
    var result = await db.insert("song", song.toMap());
    return result;
  }

  Future<int> _saveArtist(Artist artist) async {
    Database db = await _database;
    try {
      var result = await db.insert("artist", artist.toMap());
      if(result > 0)
        return artist.ArtistId;
    }
    catch(e){
      if(e!=null && e is DatabaseException){
          if((e as DatabaseException).isUniqueConstraintError())
            return artist.ArtistId;
      }
    }
    return 0;
  }

  Future<int> _saveArtistBySong(int ArtistId,int SongId) async {
    Database db = await _database;
    try {
      return await db.rawInsert("INSERT OR IGNORE INTO song_by_artist (ArtistId,SongId) VALUES ($ArtistId,$SongId)");
    }
    catch(e){
    }

    return 0;
  }

  Future<int> _saveArtistByAlbum(int ArtistId,int AlbumId) async {
    Database db = await _database;
    try {
      return await db.rawInsert("INSERT OR IGNORE INTO album_by_artist (ArtistId,AlbumId) VALUES ($ArtistId,$AlbumId)");
    }
    catch(e){
    }

    return 0;
  }

  Future _saveArtistListBySong(List<int> artists, int SongId) async {
    //Добавляем в таблицу связку
    Iterable<Future<int>> mappedList = artists.where((i)=> i > 0).map((i) async => await _saveArtistBySong(i, SongId));
    Future<List<int>> futureList = Future.wait(mappedList);
    await futureList;
  }

  Future _saveArtistListByAlbum(List<int> artists, int AlbumId) async {
    //Добавляем в таблицу связку
    Iterable<Future<int>> mappedList = artists.where((i)=> i > 0).map((i) async => await _saveArtistByAlbum(i, AlbumId));
    Future<List<int>> futureList = Future.wait(mappedList);
    await futureList;
  }

  Future<List<int>> _saveArtistList(List<Artist> artists) async {
    Iterable<Future<int>> mappedList = artists.map((i) async => await _saveArtist(i));
    Future<List<int>> futureList = Future.wait(mappedList);
    return await futureList;
  }


  Future<int> _saveSongByPlaylist(int id, int SongId ) async {
    Database db = await _database;
    var result = await db.rawInsert("INSERT OR IGNORE INTO song_by_playlist (PlaylistId,SongId) VALUES ($id, $SongId)");
    return result;
  }


  Future addSongPlaylist(int id, Song song) async {
    String query = "SELECT COUNT(1) FROM song_by_playlist where PlaylistId = $id and SongId = ${song.SongId}";
    Database db = await _database;
    int cnt = await _getCount(query);
    if(cnt == 0){
      //Добавляем песню
      query = "SELECT COUNT(1) FROM song where SongId = ${song.SongId}";
      int cnt = await _getCount(query);
      if(cnt == 0){
        var res = await _saveSong(song);
        if(res > 0) {
          List<int> _list_artist = await _saveArtistList(song.SongArtists);
          _saveArtistListBySong(_list_artist, song.SongId);

          _saveSongByPlaylist(id, song.SongId);
        }
      }
      else{
        _saveSongByPlaylist(id, song.SongId);
      }
    }
    else{
      await db.rawInsert("INSERT OR IGNORE INTO song_by_playlist (PlaylistId,SongId) VALUES ($id,${song.SongId})");
    }
  }

  Future<int> deleteSongPlaylist(int id, int SongId) async{
    String query = "DELETE FROM song_by_playlist WHERE PlaylistId = $id and SongId = $SongId";
    Database db = await _database;
    var res = await db.rawDelete(query);
    return res;
  }

  getAlbumBySong(int SongId) async
  {
    String query = "select t1.* from song_by_album as t inner join album as t1 on t1.AlbumId = t.AlbumId WHERE t.SongId = $SongId";
    Database db = await _database;
    var res = await db.rawQuery(query);
    Album _album = res.isNotEmpty ?  Album.fromMap(res.first) : null;

    if(_album!=null){
      query = "select t1.* from album_by_artist as t inner join artist as t1 on t1.ArtistId = t.ArtistId WHERE t.AlbumId = ${_album.AlbumId}";
      res = await db.rawQuery(query);
      _album.AlbumArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
    }

    return _album;
  }

  Future<List<Song>> getSongByPlaylist(int id) async{
    String query =  "select t1.SongId, t1.SongName, t1.SongDuration, t1.SongIndex,t1.SongOwnerId, t1.SongVKId, t2.SongUrl, t1.SongHash,t1.AlbumId, "
                    "(CASE WHEN t2.SongUrl IS NOT NULL THEN 1 ELSE 0 END) as IsLocalStore "
                    "from song_by_playlist as t "
                    "inner join song as t1 on t1.SongId = t.SongId "
                    "left outer join song_download as t2 on t1.SongId = t2.SongId "
                    "WHERE t.PlaylistId = $id";

    Database db = await _database;
    try {
      var res = await db.rawQuery(query);
      List<Song> list = res.isNotEmpty ? res.map((c) => Song.fromMap(c))
          .toList() : [];
      if (list.length > 0) {
        for (var i = 0; i < list.length; i++) {
          query = "select t1.* from song_by_playlist as t "
              "inner join song_by_artist t2 on  t2.SongId = t.SongId "
              "inner join artist as t1 on t1.ArtistId = t2.ArtistId WHERE t.PlaylistId = $id and t.SongId = ${list[i]
              .SongId}";
        
          res = await db.rawQuery(query);
          list[i].SongArtists =
          res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
        }
      }
      return list;
    }
    catch(e){

    }

    return [];
  }

  saveDownloadSongTask(Song song, Album album, TaskInfo task) async{
    Database db = await _database;
    String query =  "select * from song  where SongId = ${song.SongId}";
    var res = await db.rawQuery(query);
    if(!res.isNotEmpty) {
      await db.rawInsert(
          "INSERT OR IGNORE INTO song (SongId,SongName,SongDuration,SongIndex,SongOwnerId,SongVKId,SongHash,SongUrl,AlbumId) VALUES "
              "(${song.SongId},'${song.SongName}',${song.SongDuration},${song
              .SongIndex},${song.SongOwnerId},${song.SongVKId},'${song.SongHash}','${song
              .SongUrl}',${album.AlbumId})");
    }

    if(song.SongArtists!=null){
      List<int> _list_artist = await _saveArtistList(song.SongArtists);
      _saveArtistListBySong(_list_artist, song.SongId);
    }

    query =  "select * from album where AlbumId = ${album.AlbumId}";
    res = await db.rawQuery(query);
    if(!res.isNotEmpty) {
       await db.rawInsert(
          "INSERT OR IGNORE INTO album (AlbumId,AlbumName,AlbumYear,AlbumType,AlbumTracks) VALUES "
              "(${album.AlbumId},'${album.AlbumName}',${album.AlbumYear},${album.AlbumType},${album.AlbumTracks})");
    }

    if(album?.AlbumArtists!=null){
      List<int> _list_artist = await _saveArtistList(album?.AlbumArtists);
      _saveArtistListByAlbum(_list_artist, album.AlbumId);
    }

    query = "INSERT OR IGNORE INTO task_download (task_id,url,status,progress, file_name, path, SongId, AlbumId) VALUES ('${task.taskId}','${task.link}',${task.status.value},${task.progress},'${task.name}','${task.localpath}',${song.SongId},${album.AlbumId})";
    await db.rawInsert(query);
    db.query("task_download",where: "task_id = ?", whereArgs: [task.taskId]);
    return await getDownloadTask(task.taskId, song: song, album: album);
  }

  Future<DownloadTaskInfo> getDownloadTask(String task_id, {Song song = null, Album album = null }) async
  {
    String query =  "select "
        "song.*, album.*, task_download.id, task_download.task_id, task_download.status, task_download.progress, task_download.file_name, task_download.url "
        "from task_download "
        "inner join song on song.SongId = task_download.SongId "
        "inner join album on album.AlbumId = task_download.AlbumId "
        "where task_download.task_id = '$task_id'";

    Database db = await _database;
    var res = await db.rawQuery(query);
    DownloadTaskInfo task = res.isNotEmpty ? res.map((c) => DownloadTaskInfo.fromMap(c)).first : null;
    if(task!=null){

        if(song!=null && album!=null)
          {
            task.album = album;
            task.song = song;
            return task;
          }

        query = "select t1.* from song_download as t "
            "inner join song_by_artist t2 on  t2.SongId = t.SongId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.SongId = ${task.song.SongId}";
     
        res = await db.rawQuery(query);
        task.song.SongArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];

        query = "select t1.* from song_download as t "
              "inner join album_by_artist t2 on  t2.AlbumId = t.AlbumId "
              "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
              "WHERE t.SongId = ${task.album.AlbumId}";
      
          res = await db.rawQuery(query);
          task.album.AlbumArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
    }
    return task;
  }

  Future<List<DownloadTaskInfo>> loadDownloadTask() async
  {
    String query =  "select "
        "song.*, album.*, task_download.id, task_download.task_id, task_download.status, task_download.progress, task_download.file_name, task_download.url "
        "from task_download "
        "inner join song on song.SongId = task_download.SongId "
        "inner join album on album.AlbumId = task_download.AlbumId ";

    Database db = await _database;
    var res = await db.rawQuery(query);
    List<DownloadTaskInfo> list = res.isNotEmpty ? res.map((c) => DownloadTaskInfo.fromMap(c)).toList() : [];
    if(list.length > 0){
      for( var i =0 ; i < list.length; i++ ) {
        query = "select t1.* from task_download as t "
            "inner join song_by_artist t2 on t2.SongId = t.SongId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.SongId = ${list[i].song.SongId}";
        res = await db.rawQuery(query);
        list[i].song.SongArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
      }

      for( var i =0 ; i < list.length; i++ ) {
        query = "select t1.* from task_download as t "
            "inner join album_by_artist t2 on  t2.AlbumId = t.AlbumId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.AlbumId = ${list[i].album.AlbumId}";

        res = await db.rawQuery(query);
        list[i].album.AlbumArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
      }
    }
    return list;
  }

  Future<List<DownloadSong>> loadDownloadSong() async{
    String query =  "select  "
        "song.SongId, song.SongName, song.SongDuration, song.SongIndex,song.SongOwnerId, song.SongVKId, song_download.SongUrl,song.SongHash, album.* "
        "from song_download "
        "inner join song on song.SongId = song_download.SongId "
        "inner join album on album.AlbumId = song_download.AlbumId ";

    Database db = await _database;
    var res = await db.rawQuery(query);
    List<DownloadSong> list = res.isNotEmpty ? res.map((c) => DownloadSong.fromMap(c)).toList() : [];
    if(list.length > 0){
      for( var i =0 ; i < list.length; i++ ) {
        query = "select t1.* from song_download as t "
            "inner join song_by_artist t2 on  t2.SongId = t.SongId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.SongId = ${list[i].song.SongId}";

        res = await db.rawQuery(query);
        list[i].song.SongArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
      }

      for( var i =0 ; i < list.length; i++ ) {
        query = "select t1.* from song_download as t "
            "inner join album_by_artist t2 on  t2.AlbumId = t.AlbumId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.SongId = ${list[i].album.AlbumId}";

        res = await db.rawQuery(query);
        list[i].album.AlbumArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
      }
    }

    return list;
  }

  Future<DownloadSong> getDownloadSong(num SongId) async{
    String query =  "select  "
        "song.SongId, song.SongName, song.SongDuration, song.SongIndex,song.SongOwnerId, song.SongVKId, song_download.SongUrl,song.SongHash, album.* "
        "from song_download "
        "inner join song on song.SongId = song_download.SongId "
        "inner join album on album.AlbumId = song_download.AlbumId "
        "where song_download.SongId = $SongId";

    Database db = await _database;
    var res = await db.rawQuery(query);
    DownloadSong dsong = res.isNotEmpty ? res.map((c) => DownloadSong.fromMap(c)).first : null;
    if(dsong!=null){

        query = "select t1.* from song_download as t "
            "inner join song_by_artist t2 on  t2.SongId = t.SongId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.SongId = ${dsong.song.SongId}";

        res = await db.rawQuery(query);
        dsong.song.SongArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];

        query = "select t1.* from song_download as t "
            "inner join album_by_artist t2 on  t2.AlbumId = t.AlbumId "
            "inner join artist as t1 on t1.ArtistId = t2.ArtistId "
            "WHERE t.SongId = ${dsong.album.AlbumId}";

        res = await db.rawQuery(query);
        dsong.album.AlbumArtists = res.isNotEmpty ? res.map((c) => Artist.fromMap(c)).toList() : [];
    }

    return dsong;
  }

  Future<bool> deleteDownloadSong(DownloadSong dsong) async {
    Database db = await _database;
    //String query = "delete from song_download where id = ${task.id}";
    var result = await db.delete("song_download", where: "SongId = ?", whereArgs: [dsong.song.SongId]);
    return (result > 0);
  }

  Future<Map<num, String>> downloadSong(List<num> songs) async
  {
    Map<int, String> result = Map<int, String>();
    String query =  "select t.SongId, t.SongUrl "
        "from song_download as t "
        "where t.SongId in (${songs.join(",")})";

    Database db = await _database;
    var res = await db.rawQuery(query);
    if(res.isNotEmpty)
       res.forEach((c) => result.putIfAbsent(c["SongId"], () => c["SongUrl"]));
    return result;
  }

  Future<DownloadSong> transferTask(String taskId) async
  {
    String query = "INSERT INTO song_download (SongId,AlbumId,SongUrl) "
        "SELECT SongId,AlbumId,path FROM task_download "
        "WHERE task_download.task_id = '$taskId'";

    Database db = await _database;
    var res = await db.rawDelete(query);
    DownloadSong result = null;
    if(res > 0){
      int SongId  = Sqflite.firstIntValue(await db.rawQuery("select SongId from task_download where task_id = '$taskId'"));
      result = await getDownloadSong(SongId);
    }

    return result;
  }

  Future<bool> deleteTask(String taskId) async
  {
    Database db = await _database;
    var res = await db.delete("task_download",where: "task_id = ?", whereArgs: [taskId]);

    return (res > 0);
  }

  Future<bool> updateTaskDownload(String taskId, String newTaskId) async
  {
    Database db = await _database;
    var res = await db.update("task_download", {"task_id": newTaskId}, where: "task_id = ?", whereArgs: [taskId]);

    return (res > 0);
  }



  //endregion
}
