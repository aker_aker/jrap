import 'dart:async';

import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/models/progress.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/keys.dart';

class PlayerService
{
  static final _streamController = StreamController<Progress>();
  static const _channel = const MethodChannel("com.onedev.jrap_mobile/player");
//текущий и единственный токен
  static const String _KEY_TOKEN_PLAYER = "current_playlist_token";

  Stream<Progress> get possitionUpdates => _streamController.stream;

  PlayerService()
  {
    _init();
  }

  void _init() async {
    //_channel.setMethodCallHandler(null);
    _channel.setMethodCallHandler((MethodCall call)
    {
      print("call.method ${call.method}");
      switch (call.method) {
        case "onPlayerStateChanged":
          PlaybackState newState;
          bool playWhenReady = call.arguments["playWhenReady"];
          switch (call.arguments["playbackState"]) {
            case 1:
              newState = PlaybackState.none;
              break;
            case 2:
              newState = PlaybackState.buffering;
              break;
            case 3:
              newState = PlaybackState.ready;
              break;
            case 4:
              newState = PlaybackState.ended;
              break;
          }
          StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
              PlayerStateChanged(newState, playWhenReady)
          );

          break;
        case "onPlayerError":
          print(call.arguments["message"]);
          /*value = value.copyWith(
              errorMsg: method.arguments["message"],
              playWhenReady: false,
              playbackState: PlaybackState.none);*/
          break;
        case "onMusicChanged":
          StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
              ChangeSong(Song.fromMapPlayer(call.arguments), true)
          );
          break;
        case "onPlaylistUpdated":
          /*var map = method.arguments as Map;
          value = value.copyWith(
              playingList: (map["list"] as List).cast<Map>().map(Song.fromMapPlayer).toList(),
              token: map["token"]);*/
          break;
        case "onPositionChanged":
          Progress p = new Progress(
              duration: Duration(milliseconds: call.arguments["duration"]),
              position: Duration(milliseconds: call.arguments["position"])
          );

          if(_streamController.hasListener)
            _streamController.add(p);

          break;
        case "onPlayModeChanged":
          StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
              ChangePlayModeComplete(RepeatMode.values[call.arguments % 3])
          );
          break;
        case "onShuffleModeChanged":
          StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
              ChangeShuffleModeCompete(ShuffleMode.values[call.arguments % 2])
          );
          break;
      }

      return null;
    });

    await _channel.invokeMethod("init", {
      "list": null,
      "music": null,
      "token": _KEY_TOKEN_PLAYER,
      "playMode": RepeatMode.sequence.index,
      "shuffleMode": ShuffleMode.noshuffle.index
    });
  }

  ///play next music
  Future<void> playNext() {
    return _channel.invokeMethod("playNext");
  }

  ///play previous music
  Future<void> playPrevious() {
    return _channel.invokeMethod("playPrevious");
  }

  Future<Song> getPrevious() async {
    return Song.fromMap(await _channel.invokeMethod("getPrevious"));
  }

  ///return the next of current playing music
  Future<Song> getNext() async {
    return Song.fromMap(await _channel.invokeMethod("getNext"));
  }

  ///start player
  ///try to play current music if player is not available
  Future<void> setPlayWhenReady(bool playWhenReady) {
    return _channel.invokeMethod("setPlayWhenReady", playWhenReady);
  }

  Future<void> playWith(Song music) {
    assert(music != null);
    return _channel.invokeMethod("playWithQinDing", music.toMap());
  }

  Future<void> updatePlaylist(List<Song> musics) {
    assert(musics != null && musics.isNotEmpty);
    //assert(token != null);

    return _channel.invokeMethod("updatePlaylist", {
      "list": musics.map((m) => m.toMap()).toList(),
      "token": _KEY_TOKEN_PLAYER,
    });
  }

  Future<void> setPlayMode(RepeatMode playMode) {
    return _channel.invokeMethod("setPlayMode", playMode.index);
  }

  Future<void> setShuffleMode(ShuffleMode shuffleMode) {
    return _channel.invokeMethod("setShuffleMode", shuffleMode.index);
  }


  Future<void> seekTo(int position) async {
    //value = value.copyWith(position: Duration(milliseconds: position));
    await _channel.invokeMethod("seekTo", position);
  }

  Future<void> setVolume(double volume) {
    return _channel.invokeMethod("setVolume", volume);
  }

  ///the position of current media
  Future<Duration> get position async {
    return Duration(milliseconds: await _channel.invokeMethod("position"));
  }

  void dispose() async{
    await _channel.invokeMethod("quiet");
  }
}