import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';

class DownloadService {
  DownloadService()
  {
    FlutterDownloader.registerCallback((id, status, progress) {
      if(status == DownloadTaskStatus.complete)
        {
          StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(TaskDownloadFinish(id));
        }
      else{
        StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(RefreshTaskCompleet(id, status, progress,));
      }

    });
  }

  void dispose() {
    FlutterDownloader.registerCallback(null);
  }
}