
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';


class PlaylistLoadStartAction{
  PlaylistLoadStartAction();
}

class PlaylistLoadEndAction{
  PlaylistLoadEndAction();
}

class LoadPlaylistsAction{
  LoadPlaylistsAction();
}


class PlaylistLoadCompletAction{
  final List<Playlist> data;
  PlaylistLoadCompletAction(this.data);
}

class PlaylistLoadFailedAction{
  PlaylistLoadFailedAction();
}

class AddPlaylistAction{
  final String name;
  AddPlaylistAction(this.name);
}

//Создать новый плейлист и добавить в него трэк
class AddPlaylistAndAddSongAction{
  final String name;
  final Song song;
  AddPlaylistAndAddSongAction(this.name, this.song);
}

//Создать новый плейлист и добавить в него трэки (уже загруженный альбом)
class AddPlaylistAndAddSongsAction{
  final String name;
  final List<Song> song;
  AddPlaylistAndAddSongsAction(this.name, this.song);
}
//Создать новый плейлист и добавить в него трэки из альбома (альбом)
class AddPlaylistAndAddAlbumAction{
  final String name;
  final int AlbumId;
  AddPlaylistAndAddAlbumAction(this.name, this.AlbumId);
}


class AddPlaylistCompletAction{
  final Playlist pl;
  AddPlaylistCompletAction(this.pl);
}

class AddPlaylistFailedAction{}

class UpdatePlaylistAction{
  final String name;
  final int id;
  UpdatePlaylistAction(this.id, this.name);
}

class DeletePlaylistAction{
  final int id;
  DeletePlaylistAction(this.id);
}

class AddSongByPlaylist
{
  final int id;
  final Song song;
  AddSongByPlaylist(this.id, this.song);
}

class AddAlbumByPlaylist
{
  final int id;
  final int AlbumId;
  AddAlbumByPlaylist(this.id, this.AlbumId);
}

class DeleteSongByPlaylist
{
  final int id;
  final int SongId;
  DeleteSongByPlaylist(this.id, this.SongId);
}