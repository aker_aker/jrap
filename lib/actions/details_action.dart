
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/detail_artist.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';

/*
class SetAlbumDetailAction{
  final Album album;
  SetAlbumDetailAction(this.album);
}
class SetArtistDetailAction{
  final Artist artist;
  SetArtistDetailAction(this.artist);
}
class SetPlaylistDetailAction{
  final Playlist playlist;
  SetPlaylistDetailAction(this.playlist);
}
*/

class LoadAlbumDetailAction{
  final Album album;
  LoadAlbumDetailAction(this.album);
}

class AlbumDetailLoadCompletAction{
  final List<Song> tracks;
  final Album album;
  AlbumDetailLoadCompletAction(this.tracks, this.album);
}

class AlbumDetailFailedAction{
  AlbumDetailFailedAction();
}

class LoadArtistDetailAction{
  final Artist artist;
  LoadArtistDetailAction(this.artist);
}

class ArtistDetailLoadCompletAction{
  final DetailArtist detail;
  final Artist artist;
  ArtistDetailLoadCompletAction(this.detail, this.artist);
}

class ArtistDetailFailedAction{
  ArtistDetailFailedAction();
}


class LoadPlaylistDetailAction{
  final Playlist playlist;
  LoadPlaylistDetailAction(this.playlist);
}

class PlaylistDetailLoadCompletAction{
  final List<Song> tracks;
  final Playlist playlist;
  PlaylistDetailLoadCompletAction(this.tracks, this.playlist);
}

class PlaylistDetailFailedAction{
  PlaylistDetailFailedAction();
}


