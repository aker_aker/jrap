
import 'dart:ui';

import 'package:vk_login_plugin/vk_login_plugin.dart';
//import 'package:flutter_vk_login/flutter_vk_login.dart';
import 'package:jrap_mobile/models/auth_request.dart';
import 'package:jrap_mobile/models/screen_status.dart';
import 'package:jrap_mobile/models/signup_request.dart';

class SignInAction{
  final AuthRequest request;
  SignInAction(this.request);
}

class SignUpAction{
  final SignUpRequest request;
  SignUpAction(this.request);
}

class SaveTokenAction{
  final VKAccessToken token;
  SaveTokenAction(this.token);
}

class ChangeScreenStateAction{
  final ScreenState type;
  ChangeScreenStateAction(this.type);
}

class AuthVKAction{
  final VkLoginResult result;
  final VoidCallback hasTokenCallback;
  final VoidCallback noTokenCallback;
  AuthVKAction({this.hasTokenCallback, this.noTokenCallback, this.result});
}

class AuthCompleetAction{
  final bool isAuthenticated;
  AuthCompleetAction(this.isAuthenticated);
}

class AuthOffline{
  AuthOffline();
}

class LogOut{
  LogOut();
}


class ClearErrorsAction{}

class NavigateToRegistrationAction{}