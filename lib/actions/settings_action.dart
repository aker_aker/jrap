import 'package:jrap_mobile/models/setting.dart';

class LoadSettingUserAction{
  final String userId;
  LoadSettingUserAction(this.userId);
}

class LoadSettingUserCompletAction{
  final List<SettingUser> data;
  LoadSettingUserCompletAction(this.data);
}

class UpdateSettingUser
{
  final SettingUser data;
  UpdateSettingUser(this.data);
}
