
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/realeses.dart';

class LoadRealesesAction{
  LoadRealesesAction();
}

class RealesesLoadCompletAction{
  final Realeses data;
  final LoadingStatus loadingStatus;
  RealesesLoadCompletAction(this.data, this.loadingStatus);
}


class RealesesLoadFailedAction{
  RealesesLoadFailedAction();
}