import 'dart:ui';

import 'package:jrap_mobile/models/user.dart';

class GetUserAction{
   final VoidCallback hasUserCallback;
   final VoidCallback noUserCallback;

   GetUserAction({this.hasUserCallback, this.noUserCallback});
}

class SetUserAction{
   final User user;
   SetUserAction(this.user);
}
