import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';

class LoadArtistsAction{
  final int page;
  LoadArtistsAction(this.page);
}

class ArtistsLoadCompletAction{
  final List<Artist> data;
  final LoadingStatus loadingStatus;
  ArtistsLoadCompletAction(this.data, this.loadingStatus);
}

class ArtistsLoadFailedAction{
  ArtistsLoadFailedAction();
}

/**********Загрузка альбомов артиста***************/

class LoadArtistAlbumsAction
{
  final int ArtistId;
  LoadArtistAlbumsAction(this.ArtistId);
}
class ArtistAlbumsLoadCompletAction
{
  final List<Album> data;
  final LoadingStatus loadingStatus;
  ArtistAlbumsLoadCompletAction(this.data,this.loadingStatus);
}

/**********Загрузка синглов артиста***************/
class LoadArtistSinglsAction
{
  final int ArtistId;
  LoadArtistSinglsAction(this.ArtistId);
}
class ArtisSinglstLoadCompletAction
{
  final List<Album> data;
  final LoadingStatus loadingStatus;
  ArtisSinglstLoadCompletAction(this.data,this.loadingStatus);
}

/**********Загрузка песни артиста***************/
class LoadArtistSongsAction
{
  final int ArtistId;
  LoadArtistSongsAction(this.ArtistId);
}

class ArtistSongsLoadCompletAction
{
  final List<Song> data;
  final LoadingStatus loadingStatus;
  ArtistSongsLoadCompletAction(this.data,this.loadingStatus);
}