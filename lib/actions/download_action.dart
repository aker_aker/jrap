
import 'package:jrap_mobile/models/download_song.dart';


class DeleteDownloadSongAction{
  final DownloadSong dsong;
  DeleteDownloadSongAction(this.dsong);
}

class ComleetDeleteDownloadSongAction{
  final DownloadSong dsong;
  ComleetDeleteDownloadSongAction(this.dsong);
}


class LoadDownloadAction{
  LoadDownloadAction();
}

class LoadDownloadCompleetAction {
  final List<DownloadSong> data;
  LoadDownloadCompleetAction(this.data);
}

//Обновление списка после скаченной песни
class RefreshDownloadAction {
  final DownloadSong dsong;
  RefreshDownloadAction(this.dsong);
}