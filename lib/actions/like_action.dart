import 'package:jrap_mobile/models/song.dart';

class LikeSong{
  final Song song;
  LikeSong(this.song);
}

class DisLikeSong{
  final Song song;
  DisLikeSong(this.song);
}

class LikeAlbum{
  final int AlbumId;
  LikeAlbum(this.AlbumId);
}

class DisLikeAlbum{
  final int AlbumId;
  DisLikeAlbum(this.AlbumId);
}
