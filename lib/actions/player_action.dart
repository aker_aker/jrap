import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/models/queue.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/utils/enums.dart';

class AddQueueComplete {
  final Queue queue;

  AddQueueComplete(this.queue);
}

class AddQueue {
  final List<Song> songs;

  AddQueue(this.songs);
}

//Очищаем текущий плейлист и добавляем новый
class AddQueueAndClear {
  final List<Song> songs;
  final InfoQueue info;

  AddQueueAndClear(this.songs, this.info);
}

class AddQueueAndClearBySelectSong {
  final List<Song> songs;
  final Song selectSong;
  final InfoQueue info;

  AddQueueAndClearBySelectSong(this.songs, this.selectSong, this.info);
}

class AddQueueSong {
  final Song song;

  AddQueueSong(this.song);
}

class DeleteQueueSong {
  final Song song;

  DeleteQueueSong(this.song);
}

class DeleteQueueSongById {
  final int SongId;

  DeleteQueueSongById(this.SongId);
}

class NextSong {
  NextSong();
}

class NextSongComplete {
  final Queue queue;

  NextSongComplete(this.queue);
}

class PrevSong {
  PrevSong();
}

class PrevSongComplete {
  final Queue queue;
  PrevSongComplete(this.queue);
}

class PlaySong {
  PlaySong();
}

class PauseSong {
  PauseSong();
}

class ChangePlayerState {
  final Queue queue;
  final InfoQueue info;
  ChangePlayerState(this.queue, this.info);
}

class ChangeSong {
  final Song song;
  final bool isPlaylist;

  ChangeSong(this.song, this.isPlaylist);
}

class ChangeSongCompleet {
  final Song selectSong;

  ChangeSongCompleet(this.selectSong);
}

class ChangeSongAndPlaylistCompleet {
  final Song selectSong;
  final List<Song> songs;

  ChangeSongAndPlaylistCompleet(this.songs, this.selectSong);
}


class ChangePlayMode{
  ChangePlayMode();
}
class ChangePlayModeComplete{
  final RepeatMode mode;
  ChangePlayModeComplete(this.mode);
}

class ChangeShuffleMode{
  ChangeShuffleMode();
}
class ChangeShuffleModeCompete{
  final ShuffleMode shuffleMode;
  ChangeShuffleModeCompete(this.shuffleMode);
}


class PlayerStateChanged{
  PlaybackState newState;
  bool playWhenReady;
  PlayerStateChanged(this.newState, this.playWhenReady);
}

class SeekTo{
  final int val;
  SeekTo(this.val);
}

class SelectSong{
  final Song selectSong;
  final InfoQueue info;
  SelectSong(this.selectSong, this.info);
}