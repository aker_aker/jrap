
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/task_info.dart';

class RequestDownloadSong{
  TaskInfo task;
  final Song song;
  final Album album;

  RequestDownloadSong(this.song, this.album){
    task = TaskInfo(
        name: "${song.SongId}.mp3",
        link: song.SongUrl,
        SongId: song.SongId,
        progress: 0,
        status:  DownloadTaskStatus.undefined
    );
  }
}

class DownloadResume{
  final TaskInfo task;
  DownloadResume(this.task);
}

class DownloadPause{
  final TaskInfo task;
  DownloadPause(this.task);
}

class DownloadCancel{
  final TaskInfo task;
  DownloadCancel(this.task);
}

class DownloadRetry{
  final TaskInfo task;
  DownloadRetry(this.task);
}

class AddTaskCompleet
{
  final DownloadTaskInfo task;
  AddTaskCompleet(this.task);
}

class RefreshTaskCompleet{
  final String id;
  final DownloadTaskStatus status;
  final int progress;
  RefreshTaskCompleet(this.id,this.status,this.progress);
}


class RefreshTaskAfterDeleteAction
{
  final String taskId;
  RefreshTaskAfterDeleteAction(this.taskId);
}

//Действие посылается после удачной скачки файла
class TaskDownloadFinish
{
  final String taskId;
  TaskDownloadFinish(this.taskId);
}
/**********Управление загрузкой******************/

class TaskDownloadDelete {
  final TaskInfo task;
  TaskDownloadDelete(this.task);
}

class TaskDownloadRetry {
  final TaskInfo task;
  TaskDownloadRetry(this.task);
}

class TaskDownloadResume {
  final TaskInfo task;
  TaskDownloadResume(this.task);
}

class TaskDownloadResumeComplete{
  final String taskId;
  final String newTaskId;
  TaskDownloadResumeComplete(this.taskId,this.newTaskId);
}


class TaskDownloadPause {
  final TaskInfo task;
  TaskDownloadPause(this.task);
}

class TaskDownloadCancel {
  final TaskInfo task;
  TaskDownloadCancel(this.task);
}

/********PAGE TASK*********/

class LoadDownloadTaskAction{
  LoadDownloadTaskAction();
}

class LoadDownloadTaskCompleetAction {
  final List<DownloadTaskInfo> data;
  LoadDownloadTaskCompleetAction(this.data);
}
