
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';

class LoadSinglsAction{
  final int page;
  LoadSinglsAction(this.page);
}

class SinglsLoadCompletAction{
  final List<Album> data;
  final LoadingStatus loadingStatus;
  SinglsLoadCompletAction(this.data, this.loadingStatus);
}

class SinglsPullRefreshCompletAction{
  final List<Album> data;
  final LoadingStatus loadingStatus;
  SinglsPullRefreshCompletAction(this.data, this.loadingStatus);
}


class SinglsLoadFailedAction{
  SinglsLoadFailedAction();
}