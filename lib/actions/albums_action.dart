
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';

class LoadAlbumsAction{
  final int page;
  LoadAlbumsAction(this.page);
}

class AlbumsLoadCompletAction{
  final List<Album> data;
  final LoadingStatus loadingStatus;
  AlbumsLoadCompletAction(this.data, this.loadingStatus);
}


class AlbumsPullRefreshCompletAction{
  final List<Album> data;
  final LoadingStatus loadingStatus;
  AlbumsPullRefreshCompletAction(this.data, this.loadingStatus);
}

class AlbumsLoadFailedAction{
  AlbumsLoadFailedAction();
}