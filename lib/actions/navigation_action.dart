import 'package:flutter/cupertino.dart';
import 'package:jrap_mobile/utils/keys.dart';

class NavigateReplaceAction {
  final String routeName;

  NavigateReplaceAction(this.routeName);

  @override
  String toString() {
    return 'MainMenuNavigateAction{routeName: $routeName}';
  }
}

class NavigatePushAction {
  final String routeName;
  final KeyType keyNav;
  final Object arguments;
  NavigatePushAction({@required this.keyNav, @required this.routeName, this.arguments});

  @override
  String toString() {
    return 'NavigatePushAction{routeName: $routeName}';
  }
}

class NavigatePopAction {

  @override
  String toString() {
    return 'NavigatePopAction';
  }
}

class NavigatePopUntilAction {
  final String routeName;

  NavigatePopUntilAction(this.routeName);

  @override
  String toString() {
    return 'NavigatePopUntilAction';
  }
}