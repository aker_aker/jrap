import 'package:jrap_mobile/models/search_all.dart';

class SearchQuery {
  final String text;
  SearchQuery(this.text);
}

class SearcCompletAction{
  final SearchAll data;
  final String text;
  SearcCompletAction(this.data, this.text);
}

class SearcClearTextAction{
  SearcClearTextAction();
}

class FirstLoadSearchPage{
  FirstLoadSearchPage();
}