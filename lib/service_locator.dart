import 'package:get_it/get_it.dart';
import 'package:jrap_mobile/services/download.dart';
import 'package:jrap_mobile/services/player.dart';

// ambient variable to access the service locator
GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerSingleton<DownloadService>( DownloadService(), signalsReady: true);
  locator.registerSingleton<PlayerService>( PlayerService(), signalsReady: true);
}