import 'package:flutter/widgets.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';


class NavigationMiddleware extends MiddlewareClass<AppState>{
  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    if(action is NavigatePushAction){
      Keys.navKey[action.keyNav].currentState.pushNamed(action.routeName, arguments: action.arguments);
    }
    else if(action is NavigateReplaceAction){
      Keys.navKey[KeyType.main].currentState.pushReplacementNamed(action.routeName);

    }
    else if(action is NavigatePopAction){

    }
    else if(action is NavigatePopUntilAction){
      var _state = Keys.navKey[KeyType.over].currentState;
      if(_state.canPop()) {
        _state.popUntil(ModalRoute.withName(action.routeName));
      }
    }
    next(action);
  }
}