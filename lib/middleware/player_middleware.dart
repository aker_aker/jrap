import 'package:get_it/get_it.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/models/queue.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:jrap_mobile/services/player.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/states/player.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:redux/redux.dart';

class PlayerMiddleware extends MiddlewareClass<AppState>{

  PlayerService get service => GetIt.I<PlayerService>();

  PlayerMiddleware();

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async{
    if(action is ChangeSong)
    {
      if(store.state.playerState.queue.Contains(action.song)) {
        store.dispatch(new ChangeSongCompleet(action.song));
      }
      else {
        List<Song> _list = null;
        if(action.isPlaylist)
           _list = store.state.playlistDetailState.tracks;
        else if(action.isPlaylist)
           _list = store.state.albumDetailState.tracks;

        store.dispatch(new ChangeSongAndPlaylistCompleet(_list, action.song));
      }
    }
    else if(action is AddQueueAndClear){
      Queue queue = store.state.playerState.queue;
      _addSongsAndClear(store, action.songs, action.info, queue);
    }
    else if(action is PlaySong){
      Queue queue = store.state.playerState.queue;
      _play(store, queue, null);
    }
    else if(action is PauseSong){
      _pause();
    }
    else if(action is NextSong){
      Queue queue = store.state.playerState.queue;
      _playNext(store, queue);
    }
    else if(action is PrevSong){
      Queue queue = store.state.playerState.queue;
      _playPrevious(store,  queue);
    }
    else if(action is ChangePlayMode){
      RepeatMode curMode = store.state.playerState.mode;
      _changePlayMode(curMode);
    }
    else if(action is ChangeShuffleMode){
      ShuffleMode shuffleMode = store.state.playerState.shuffleMode;
      _changeShuffleMode(shuffleMode);
    }
    else if(action is SeekTo){
      _seekTo(action.val);
    }
    else if(action is SelectSong){
      _selectSong(store, action.selectSong, action.info, store.state.playerState.copyWith());
    }
    else
      next(action);
  }


  Future<void> _seekTo(int position) async {
    await service.seekTo(position);
  }

  Future<void> _changePlayMode(RepeatMode mode) async {
    RepeatMode next = RepeatMode.values[(mode.index + 1) % 3];
    await service.setPlayMode(next);
  }


  Future<void> _changeShuffleMode(ShuffleMode mode) async {
    ShuffleMode next = ShuffleMode.values[(mode.index + 1) % 2];
    await service.setShuffleMode(next);
  }


  Future<void> _selectSong(Store<AppState> store, Song song, InfoQueue info, JRPlayerState _state) async {
    List<Song> musicList;
    if(info.playType == PlayType.album)
      musicList = store.state.albumDetailState.tracks.where((item)=>item.IsVkLoad).toList();
    else if(info.playType == PlayType.playlist)
      musicList = store.state.playlistDetailState.tracks.where((item)=>item.IsVkLoad).toList();

    if(
        (!_state.queue.isQueue) ||
        (_state.info.playType != info.playType) ||
        (_state.info.playType == info.playType && _state.info.Id != info.Id) || (!song.IsUrl)
    )
    {
      var queue = _state.queue;
      if(musicList!=null)
      {
        queue.songs.clear();

        queue.index = musicList.indexOf(song);
       //проверяем можно ли ее играть
        if(!song.IsUrl){
          var newList = await getSongNotParser(musicList, song);
          var vk_audio = await getVkAudio(store, newList);
          if(vk_audio!=null){
            //простовляем ссылки к трекам
            for (var i = 0; i < musicList.length; i++) {
              if(vk_audio.containsKey(musicList[i].SongVKId))
                musicList[i].SongUrl =  vk_audio[musicList[i].SongVKId];
            }
          }
        }

        queue.songs.addAll(musicList);
        queue.song = song;

        await service.updatePlaylist(queue.songs);
        await service.playWith(queue.song);
        store.dispatch(ChangePlayerState(queue, info));
      }
    }
    else {
      if (song.IsUrl) {
        //store.dispatch(ChangeSong(song, false));
        await service.playWith(song);
      }
    }

  }

  Future<List<Song>> getSongNotParser(List<Song> musicList, Song song) async{
    var index = musicList.indexOf(song) + 1;
    var _top_song = musicList.where((item)=> item.SongIndex < index  && item.IsUrl==false).take(4);
    var _top_bottom = musicList.where((item)=> item.SongIndex > index  && item.IsUrl==false).take(4);
    //если все сверху уэе  с url
    if(_top_song == 0){
      _top_bottom = musicList.where((item)=> item.SongIndex > index  && item.IsUrl==false).take(9);
    }
    if(_top_bottom==0){
      _top_bottom = musicList.where((item)=> item.SongIndex < index  && item.IsUrl==false).take(9);
    }

    return [..._top_bottom, ..._top_song, ...[song]];
  }

  Future< Map<num, String>> getVkAudio(Store<AppState> store, List<Song> data) async{
    NetworkUtil _client = new NetworkUtil.internal();
    //собираем ключи для запроса в VK
    var _audios = data.where((item) => item.SongOwnerId != null && item.SongVKId != null && item.SongHash!=null && item.SongUrl == null)
        .map((x) => "${x.SongOwnerId}_${x.SongVKId}_${x.SongHash}")
        .join(',');

    if(_audios.isNotEmpty)
    {
      Map<num, String> urls = await  _client.getVkAudio(_audios, store.state.authState.user.cookie);
      return urls;

    }

    return null;
  }

  Future<void> _play(Store<AppState> store, Queue queue, Song song) async {
    song = song ?? queue.song;
    if (song == null) return;

    if (!queue.songs.contains(song)) {
      //playing list do not contain music
      //so we insert this music to next of current playing
      await _insertToNext([song],queue);
    }
    if (queue.song == song /*&& service.value.playbackState != PlaybackState.none*/) {
       await service.setPlayWhenReady(true);
    }
    //assert(music.SongUrl != null && music.SongUrl.isNotEmpty, "music url can not be null");
     await service.playWith(song);
  }

  Future<void> _insertToNext(List<Song> list, Queue queue) async {
    final playingList = List.of(queue.songs);
    playingList.removeWhere((m) => list.contains(m));

    final index = playingList.indexOf(queue.song) + 1;
    playingList.insertAll(index, list);
    await service.updatePlaylist(playingList);
  }


  void _addSongsAndClear(Store<AppState> store, List<Song> songs, InfoQueue info, Queue queue) async{

    var _lenght = songs.where((item)=>item.IsUrl != false).length;
    //проверяем можно ли ее играть
    if(_lenght==0){
      var newList = await getSongNotParser(songs, songs.first);
      var vk_audio = await getVkAudio(store, newList);
      if(vk_audio!=null){
        //простовляем ссылки к трекам
        for (var i = 0; i < songs.length; i++) {
          if(vk_audio.containsKey(songs[i].SongVKId))
            songs[i].SongUrl =  vk_audio[songs[i].SongVKId];
        }
      }
    }

    _lenght = songs.where((item)=>item.IsUrl != false).length;

    if(_lenght > 0)
    {
      queue.songs.clear();
      queue.songs.addAll(songs);
      queue.song = queue.songs.first;
      queue.index = 0;

      await service.updatePlaylist(queue.songs);
      await service.playWith(queue.song);
      store.dispatch(ChangePlayerState(queue, info));
      //store.dispatch(AddQueueComplete(queue));
    }
  }

  void _playNext(Store<AppState> store, Queue queue) async{
    if(queue.songs.length == queue.index + 1) {
      queue.index = 0;
    }
    else{
      queue.index++;
    }

    queue.song = queue.songs[queue.index];

    await service.playNext();
    store.dispatch(NextSongComplete(queue));
  }

  void _playPrevious(Store<AppState> store, Queue queue) async{

    if(queue.songs.length == 0) {
      queue.index = queue.songs.length-1;
    }
    else{
      queue.index--;
    }
    queue.song = queue.songs[queue.index];

    await  service.playPrevious();
    store.dispatch(PrevSongComplete(queue));
  }

  Future<void> _pause() async {
     await service.setPlayWhenReady(false);
  }

}
