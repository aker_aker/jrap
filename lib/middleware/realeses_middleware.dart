import 'package:jrap_mobile/actions/realeses_actions.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/realeses.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:redux/redux.dart';


class RealesesMiddleware extends MiddlewareClass<AppState>{

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next)
  {
    if(action is LoadRealesesAction)
    {
      _loadReleasesAction(store);
    }
    else
    {
      next(action);
    }
  }

  void _loadReleasesAction(Store<AppState> store) async {
    NetworkUtil _client = new NetworkUtil.internal();
    Realeses data = await _client.getRelease();
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;
    store.dispatch(RealesesLoadCompletAction(data, loadingStatus));
  }
}
