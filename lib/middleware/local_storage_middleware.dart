import 'package:jrap_mobile/actions/auth_actions.dart';
import 'package:jrap_mobile/actions/db_actions.dart';
import 'package:jrap_mobile/actions/settings_action.dart';
import 'package:jrap_mobile/models/profile.dart';
import 'package:jrap_mobile/models/user.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vk_login_plugin/vk_login_plugin.dart';
import 'dart:async';

class LocalStorageMiddleware extends MiddlewareClass<AppState>{

  final SharedPreferences preferences;
  final VkLoginPlugin vkSignIn = new VkLoginPlugin();

  LocalStorageMiddleware(this.preferences);

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async{

    if(action is LogOut) {
        _logOut();
    }
    else if(action is AuthVKAction) {
      VkLoginResult result = await _login(action);
       switch (result.status)
       {
         case VKLoginStatus.loggedIn:
          {
             bool _result =  await _saveUser(result.token);
             if(_result) {
               var _user = await _getUser();
               if(_user != null) {
                 Profile profile = await _loadVkUser(_user);

                 if(profile!=null){
                   _user.firstName = profile.firstName;
                   _user.lastName = profile.lastName;
                   _user.imageUrl = profile.imageUrl;
                   _updateProfile(profile,_user.userId);
                 }

                 store.dispatch(SetUserAction(_user));
                 store.dispatch(LoadSettingUserAction(_user.userId));
               }
               else
                 action.noTokenCallback();

               action.hasTokenCallback();
             }
             else
               action.noTokenCallback();
             break;
          }
         case VKLoginStatus.cancelledByUser:
         //_showMessage('Login cancelled by the user.');
           action.noTokenCallback();
           break;
         case VKLoginStatus.error:
         //_showMessage('Something went wrong with the login process.\n''Here\'s the error VK gave us: ${result.errorMessage}');
           action.noTokenCallback();
           break;
       }
    }
    else if (action is GetUserAction) {
      //action.noUserCallback();
      var _user = await _getUser();
      if(_user != null){
        store.dispatch(SetUserAction(_user));
        store.dispatch(LoadSettingUserAction(_user.userId));
        action.hasUserCallback();
      }
      else{
        action.noUserCallback();
      }
    }
    else
    {
      next(action);
    }
  }

  Future<Null> _logOut() async {
    DatabaseClient _db = new DatabaseClient();
    bool res = await _db.logOutUser();
    await vkSignIn.logOut();
    if(res){
      Keys.navKey[KeyType.main].currentState.pushNamedAndRemoveUntil('/', (_) => false);
    }
  }

  Future<bool> _check() async{
    await new Future.delayed(const Duration(milliseconds: 10));
    return true;
  }


  Future<VkLoginResult> _login(AuthVKAction action) async {
    return await vkSignIn.logIn(['email','photo_50','offline', 'https']);
  }

  Future<bool> _saveUser(VKAccessToken token) async {
    DatabaseClient _db = new DatabaseClient();
    return await _db.saveUserToke(token);
  }

  Future<User> _getUser() async {
    DatabaseClient _db = new DatabaseClient();
    return await _db.getUser();
  }

  Future<bool> _updateProfile(Profile profile, String userId) async {
    DatabaseClient _db = new DatabaseClient();
    return await _db.updateProfile(profile,userId);
  }

  Future<Profile> _loadVkUser(User user) async{
    NetworkUtil _client = new NetworkUtil.internal();
    return await _client.getVkUser(user);
  }

}
