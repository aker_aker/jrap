import 'package:jrap_mobile/actions/albums_action.dart';
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class AlbumsMiddleware extends MiddlewareClass<AppState>{

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    if(action is LoadAlbumsAction)
    {
      _loadAlbumsAction(store, action.page);
    }
    else if(action is LoadAlbumDetailAction)
    {
      _loadAlbumDetailAction(store, action.album);
    }
    else
    {
      next(action);
    }
  }

  Future<void>  _loadAlbumsAction(Store<AppState> store,int page) async {
    NetworkUtil _client = new NetworkUtil.internal();
    List<Album> data = await _client.getAlbums(page);
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;

    if(page == 0 && store.state.albumsState.data!=null && store.state.albumsState.data.length  > 0){
      //значит pull refresh
      store.dispatch(AlbumsPullRefreshCompletAction(data,loadingStatus));
    }else{
      store.dispatch(AlbumsLoadCompletAction(data,loadingStatus));
    }
  }

  Future<void> _loadAlbumDetailAction(Store<AppState> store,Album album) async
  {
    //получаем треки в альбоме
    NetworkUtil _client = new NetworkUtil.internal();
    List<Song> data = await  _client.getAlbumTracks(album.AlbumId);

    //получаем скаченные треки
    DatabaseClient _db = new DatabaseClient();
    Map<num, String> dsongs = await _db.downloadSong(data.map((s)=> s.SongId).toList());

    data.forEach((s) => s.SongUrl = dsongs.containsKey(s.SongId) ? dsongs[s.SongId] : null);
    data.forEach((s) => s.IsLocalStore = dsongs.containsKey(s.SongId) ? true : false);

    try {

      String _audios;
      //если больше 9 то собираем не все ключи
      if(data.length > 9) {
        _audios = data.where((item) => item.SongOwnerId != null && item.SongVKId != null && item.SongHash!=null && item.SongUrl == null).take(9)
            .map((x) => "${x.SongOwnerId}_${x.SongVKId}_${x.SongHash}")
            .join(',');
      }
      else{
        //собираем ключи для запроса в VK
        _audios = data.where((item) => item.SongOwnerId != null && item.SongVKId != null && item.SongHash!=null && item.SongUrl == null)
            .map((x) => "${x.SongOwnerId}_${x.SongVKId}_${x.SongHash}")
            .join(',');
      }

      if(_audios.isNotEmpty)
      {
        /*var s = data.where((item) =>item.SongOwnerId != null && item.SongVKId != null).first;
        if(s != null)
          _audios = "$_audios,${s.SongOwnerId}_${s.SongVKId}";*/

        Map<num, String> urls = await  _client.getVkAudio(_audios, store.state.authState.user.cookie);

        //простовляем ссылки к трекам
        for (var i = 0; i < data.length; i++) {
          if(urls.containsKey(data[i].SongVKId))
            data[i].SongUrl =  urls[data[i].SongVKId];
        }
      }
    }
    catch(ex){}
    finally{
      store.dispatch(new AlbumDetailLoadCompletAction(data, album));
    }
  }
}
