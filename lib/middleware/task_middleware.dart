import 'dart:io';

import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/actions/download_action.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/task_info.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:path_provider/path_provider.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class TaskMiddleware extends MiddlewareClass<AppState>{

  String localPath;

  TaskMiddleware() {
     _init();
  }

  Future  _init() async {
    localPath = (await _findLocalPath()) + '/Download';

    final savedDir = Directory(localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }
  }



  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async{
    if(action is RequestDownloadSong)
    {
      var _taskId =  await _requestDownload(action.task);
      action.task.localpath = "$localPath/${action.song.SongId}.mp3";
      action.task.taskId = _taskId;
      DownloadTaskInfo task = await _saveDownloadTask(action.song, action.album, action.task);
      //await _pauseDownload(action.task);
      store.dispatch(new AddTaskCompleet(task));
    }
    else if(action is DownloadCancel)
    {
      _cancelDownload(action.task);
    }
    else if(action is LoadDownloadTaskAction)
    {
      List<DownloadTaskInfo> data = await _load();
      store.dispatch(new LoadDownloadTaskCompleetAction(data));
    }
    else if(action is TaskDownloadFinish)
    {
      DownloadSong res = await _transferTaskToDownload(action.taskId);
      if(res!=null) {
        if(store.state.downloadState.loadingStatus == LoadingStatus.success)
          store.dispatch(new RefreshDownloadAction(res));

        await _deleteTask(action.taskId);
        store.dispatch(new RefreshTaskAfterDeleteAction(action.taskId));
      }
    }
    else if(action is TaskDownloadResume)
    {
       String newTaskId = await _resumeDownload(action.task);
       store.dispatch(TaskDownloadResumeComplete(action.task.taskId, newTaskId));
    }
    else if(action is TaskDownloadPause)
    {
       _pauseDownload(action.task);
    }
    else if(action is TaskDownloadCancel)
    {
        _cancelDownload(action.task);
    }
    else if(action is TaskDownloadRetry)
    {
      String newTaskId = await _retryDownload(action.task);
      store.dispatch(TaskDownloadResumeComplete(action.task.taskId, newTaskId));
    }
    else if(action is TaskDownloadDelete)
    {
      bool res = await _delete(action.task);
      if(res)
        store.dispatch(new RefreshTaskAfterDeleteAction(action.task.taskId));
    }
    else
    {
      next(action);
    }
  }

  Future<String> _requestDownload(TaskInfo task) async {
    task.taskId = await FlutterDownloader.enqueue(
        url: task.link,
        fileName: task.name,
        savedDir: localPath,
        showNotification: true,
        openFileFromNotification: true);

    return task.taskId;
  }

  Future<DownloadTaskInfo> _saveDownloadTask(Song song, Album album, TaskInfo task) async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.saveDownloadSongTask(song, album, task);
  }


  void _cancelDownload(TaskInfo task) async {
    await FlutterDownloader.cancel(taskId: task.taskId);
  }

  void _pauseDownload(TaskInfo task) async {
    await FlutterDownloader.pause(taskId: task.taskId);
  }

  Future<String> _resumeDownload(TaskInfo task) async {
    String newTaskId = await FlutterDownloader.resume(taskId: task.taskId);
    DatabaseClient _db = new DatabaseClient();
    await _db.updateTaskDownload(task.taskId, newTaskId);
    return newTaskId;
  }

  Future<String> _retryDownload(TaskInfo task) async {
    String newTaskId = await FlutterDownloader.retry(taskId: task.taskId);
    DatabaseClient _db = new DatabaseClient();
    await _db.updateTaskDownload(task.taskId, newTaskId);
    return newTaskId;
  }

  Future<bool> _openDownloadedFile(TaskInfo task) {
    return FlutterDownloader.open(taskId: task.taskId);
  }

  Future<bool>  _delete(TaskInfo task) async {
    await FlutterDownloader.remove(taskId: task.taskId, shouldDeleteContent: true);
    return await _deleteTask(task.taskId);
  }

  Future<String> _findLocalPath() async {
    /*final directory = widget.platform == TargetPlatform.android
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();*/
    final directory = await getExternalStorageDirectory();
    return directory.path;
  }

  //Переносим из очереди загрузок
  Future<DownloadSong> _transferTaskToDownload(String taskId) async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.transferTask(taskId);
  }

  //Удаляем таску
  Future<bool> _deleteTask(String taskId) async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.deleteTask(taskId);
  }


  /*PAGE*/
  Future<List<DownloadTaskInfo>> _load() async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.loadDownloadTask();
  }

/**/

/*
  Future<Null> _prepare() async {
    final tasks = await FlutterDownloader.loadTasks();

    int count = 0;
    _tasks = [];
    _items = [];

    _tasks.addAll(_documents.map((document) =>
        TaskInfo(name: document['name'], link: document['link'])));

    _items.add(ItemHolder(name: 'Documents'));
    for (int i = count; i < _tasks.length; i++) {
      _items.add(ItemHolder(name: _tasks[i].name, task: _tasks[i]));
      count++;
    }

    _tasks.addAll(_images
        .map((image) => TaskInfo(name: image['name'], link: image['link'])));

    _items.add(ItemHolder(name: 'Images'));
    for (int i = count; i < _tasks.length; i++) {
      _items.add(ItemHolder(name: _tasks[i].name, task: _tasks[i]));
      count++;
    }

    _tasks.addAll(_videos
        .map((video) => _TaskInfo(name: video['name'], link: video['link'])));

    _items.add(_ItemHolder(name: 'Videos'));
    for (int i = count; i < _tasks.length; i++) {
      _items.add(_ItemHolder(name: _tasks[i].name, task: _tasks[i]));
      count++;
    }

    tasks?.forEach((task) {
      for (_TaskInfo info in _tasks) {
        if (info.link == task.url) {
          info.taskId = task.taskId;
          info.status = task.status;
          info.progress = task.progress;
        }
      }
    });

    _permissisonReady = await _checkPermission();

    _localPath = (await _findLocalPath()) + '/Download';

    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }

    setState(() {
      _isLoading = false;
    });
  }
*/

}
