import 'dart:io';

import 'package:jrap_mobile/actions/download_action.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class DownloadMiddleware extends MiddlewareClass<AppState>{

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async{
    if(action is LoadDownloadAction)
    {
      List<DownloadSong> data = await _load();
      store.dispatch(LoadDownloadCompleetAction(data));
    }
    else if(action is DeleteDownloadSongAction)
    {
      bool result = await _deleteDownloadSong(action.dsong);
      if(result)
          store.dispatch(ComleetDeleteDownloadSongAction(action.dsong));
    }
    else
    {
      next(action);
    }
  }

  /*PAGE*/
  Future<bool> _deleteDownloadSong(DownloadSong dsong) async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.deleteDownloadSong(dsong);
  }

  Future<List<DownloadSong>> _load() async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.loadDownloadSong();
  }

  /**/

/*
  Future<Null> _prepare() async {
    final tasks = await FlutterDownloader.loadTasks();

    int count = 0;
    _tasks = [];
    _items = [];

    _tasks.addAll(_documents.map((document) =>
        TaskInfo(name: document['name'], link: document['link'])));

    _items.add(ItemHolder(name: 'Documents'));
    for (int i = count; i < _tasks.length; i++) {
      _items.add(ItemHolder(name: _tasks[i].name, task: _tasks[i]));
      count++;
    }

    _tasks.addAll(_images
        .map((image) => TaskInfo(name: image['name'], link: image['link'])));

    _items.add(ItemHolder(name: 'Images'));
    for (int i = count; i < _tasks.length; i++) {
      _items.add(ItemHolder(name: _tasks[i].name, task: _tasks[i]));
      count++;
    }

    _tasks.addAll(_videos
        .map((video) => _TaskInfo(name: video['name'], link: video['link'])));

    _items.add(_ItemHolder(name: 'Videos'));
    for (int i = count; i < _tasks.length; i++) {
      _items.add(_ItemHolder(name: _tasks[i].name, task: _tasks[i]));
      count++;
    }

    tasks?.forEach((task) {
      for (_TaskInfo info in _tasks) {
        if (info.link == task.url) {
          info.taskId = task.taskId;
          info.status = task.status;
          info.progress = task.progress;
        }
      }
    });

    _permissisonReady = await _checkPermission();

    _localPath = (await _findLocalPath()) + '/Download';

    final savedDir = Directory(_localPath);
    bool hasExisted = await savedDir.exists();
    if (!hasExisted) {
      savedDir.create();
    }

    setState(() {
      _isLoading = false;
    });
  }
*/

}
