import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/detail_artist.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:redux/redux.dart';

class ArtistsMiddleware extends MiddlewareClass<AppState>{

  ArtistsMiddleware();

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next){
    if(action is LoadArtistsAction)
    {
      _loadArtistsAction(store,action.page);
    }
    else if(action is LoadArtistDetailAction)
    {
      _loadArtistDetailAction(store,action.artist);
    }
    else if(action is LoadArtistAlbumsAction) {
      _loadArtistAlbumsAction(store,action.ArtistId);
    }
    else if(action is LoadArtistSinglsAction) {
      _loadArtistSinglsAction(store,action.ArtistId);
    }
    else if(action is LoadArtistSongsAction) {
      _loadArtistSongsAction(store,action.ArtistId);
    }
    else
    {
      next(action);
    }
  }


  Future<void> _loadArtistsAction(Store<AppState> store, int page) async {
    NetworkUtil _client = new NetworkUtil.internal();
    List<Artist> data = await _client.getArtist(page);
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;
    store.dispatch(new ArtistsLoadCompletAction(data,loadingStatus));
  }

  Future<void> _loadArtistDetailAction(Store<AppState> store, Artist artist) async {
    NetworkUtil _client = new NetworkUtil.internal();
    DetailArtist data = await _client.getArtistDetail(artist.ArtistId);
    if(artist.Info == null)
      artist.Info = data.artist.Info;

    store.dispatch(ArtistDetailLoadCompletAction(data, artist));
  }

  Future<void> _loadArtistAlbumsAction(Store<AppState> store,int ArtistId) async {
    NetworkUtil _client = new NetworkUtil.internal();
    List<Album> data = await _client.getArtistAlbums(ArtistId);
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;
    store.dispatch(new ArtistAlbumsLoadCompletAction(data, loadingStatus));
  }

  Future<void> _loadArtistSinglsAction(Store<AppState> store,int ArtistId) async {
    NetworkUtil _client = new NetworkUtil.internal();
    List<Album> data = await _client.getArtistSingls(ArtistId);
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;
    store.dispatch(new ArtisSinglstLoadCompletAction(data,loadingStatus));
  }

  Future<void> _loadArtistSongsAction(Store<AppState> store,int ArtistId) async {
    NetworkUtil _client = new NetworkUtil.internal();
    List<Song> data = await _client.getArtistSongs(ArtistId);
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;
    store.dispatch(new ArtistSongsLoadCompletAction(data,loadingStatus));
  }

}
