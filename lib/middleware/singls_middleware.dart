import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/actions/singls_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class SinglsMiddleware extends MiddlewareClass<AppState>{


  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    if(action is LoadSinglsAction)
    {
      _loadSinglsAction(store, action.page);
    }
    else
    {
      next(action);
    }
  }

  void _loadSinglsAction(Store<AppState> store,int page) async {
    NetworkUtil _client = new NetworkUtil.internal();
    List<Album> data = await _client.getSingls(page);
    var loadingStatus = (data == null) ? LoadingStatus.error : LoadingStatus.success;
    if(page == 0 && store.state.singlsState.data!=null && store.state.singlsState.data.length  > 0){
      //значит pull refresh
      store.dispatch(SinglsPullRefreshCompletAction(data,loadingStatus));
    }else{
      store.dispatch(SinglsLoadCompletAction(data,loadingStatus));
    }

  }

}
