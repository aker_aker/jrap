import 'package:jrap_mobile/actions/db_actions.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:redux/redux.dart';

class DataBaseMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next){
    if (action is GetUserAction)
    {
      _getUserAction(store, action.hasUserCallback , action.noUserCallback);
    }
    else {
      next(action);
    }
  }

  Future<void> _getUserAction(Store<AppState> store,Function hasUserCallback ,Function noUserCallback) async
  {
    DatabaseClient _db = new DatabaseClient();
    var _user = await _db.getUser();
    if(_user != null)
    {
      store.dispatch(new SetUserAction(_user));
      hasUserCallback();
    }
    else{
      noUserCallback();
    }
  }
}