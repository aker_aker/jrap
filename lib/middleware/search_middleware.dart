import 'package:jrap_mobile/actions/search_action.dart';
import 'package:jrap_mobile/models/search_all.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class SearchMiddleware extends MiddlewareClass<AppState>{

  SearchMiddleware();

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next){
    if(action is SearchQuery)
    {
     _load(store, action.text);
    }

    next(action);
  }

  Future<void> _load(Store<AppState> store, String txt) async{
    NetworkUtil _client = new NetworkUtil.internal();
    SearchAll data = await _client.search(txt);
    store.dispatch(new SearcCompletAction(data, txt));
  }
}
