
import 'package:jrap_mobile/actions/like_action.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class LikeMiddleware extends MiddlewareClass<AppState> {

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async {
    if (action is LikeSong) {
      await _like(store.state.authState.user.userId, action.song.SongId);
      _addSongPlaylistFavorit(action.song);
    }
    else if (action is DisLikeSong) {
      await _dislike(store.state.authState.user.userId, action.song.SongId);
    }
    else if (action is LikeAlbum) {
      await _likeAlbum(store.state.authState.user.userId, action.AlbumId);
    }
    else if (action is DisLikeAlbum) {
      await _dislikeAlbum(store.state.authState.user.userId, action.AlbumId);
    }
    else {
      next(action);
    }
  }

  Future _like(String userId,int SongId) async{
    NetworkUtil _client = new NetworkUtil.internal();
    return await _client.setSongLike(userId,SongId);
  }

  Future _dislike(String userId,int SongId) async{
    NetworkUtil _client = new NetworkUtil.internal();
    return await _client.setSongDisLike(userId,SongId);
  }

  Future _likeAlbum(String userId,int AlbumId) async{
    NetworkUtil _client = new NetworkUtil.internal();
    //return await _client.setSongLike(userId,SongId);
  }

  Future _dislikeAlbum(String userId,int AlbumId) async{
    NetworkUtil _client = new NetworkUtil.internal();
    //return await _client.setSongDisLike(userId,SongId);
  }

  Future<int> _addSongPlaylistFavorit(Song song) async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.addSongPlaylist(1, song);
  }
}