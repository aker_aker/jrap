import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/services/network.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:redux/redux.dart';
import 'dart:async';

import 'package:vk_login_plugin/vk_login_plugin.dart';

class PlaylistsMiddleware extends MiddlewareClass<AppState>{

  PlaylistsMiddleware();

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async{
    if(action is LoadPlaylistsAction)
    {
      store.dispatch(new PlaylistLoadStartAction());
      var data = await _load();
      store.dispatch(new PlaylistLoadCompletAction(data));
    }
    else if(action is AddPlaylistAction){
      _add(action.name);
      var data = await _load();
      store.dispatch(new PlaylistLoadCompletAction(data));
    }
    else if(action is UpdatePlaylistAction){
      _update(action.id, action.name);
      var data = await _load();
      store.dispatch(new PlaylistLoadCompletAction(data));
    }
    else if(action is DeletePlaylistAction){
      _deleted(action.id);
      var data = await _load();
      store.dispatch(new PlaylistLoadCompletAction(data));
    }
    else if(action is AddSongByPlaylist){
      await _addSong(action.id, action.song);
    }
    else if(action is AddPlaylistAndAddSongAction){
      int id = await _add(action.name);
      await _addSong(id, action.song);
    }
    else if(action is AddPlaylistAndAddSongsAction){
      int id = await _add(action.name);
      await _addSongs(id, action.song);
    }
    else if(action is AddPlaylistAndAddAlbumAction){
      int id = await _add(action.name);
      await _addAlbum(id, action.AlbumId);
    }
    else if(action is AddAlbumByPlaylist){
      await _addAlbum(action.id, action.AlbumId);
    }
    else if(action is DeleteSongByPlaylist){
      await _deleteSong(action.id, action.SongId);
    }
    else if(action is LoadPlaylistDetailAction){
      _getSongbyPlaylist(store, action.playlist);
    }
    else
      next(action);
  }



  Future<List<Playlist>> _load() async{
    DatabaseClient _db = new DatabaseClient();
    return await _db.getPlaylist();
  }

  Future<int> _add(String name) async{

    DatabaseClient _db = new DatabaseClient();
    return await _db.addPlaylist(name);
  }

  Future<int> _update(int id, String name) async{

    DatabaseClient _db = new DatabaseClient();
    return await _db.updatePlaylist(id, name);
  }

  Future<int> _deleted(int id) async{

    DatabaseClient _db = new DatabaseClient();
    return await _db.deletePlaylist(id);
  }

  Future _addSong(int id, Song song) async{
    DatabaseClient _db = new DatabaseClient();
    await _db.addSongPlaylist(id, song);
  }


  Future _addSongs(int id, List<Song> songs) async{
    DatabaseClient _db = new DatabaseClient();
    for( var i =0 ; i < songs.length; i++ ) {
      await _db.addSongPlaylist(id, songs[i]);
    }
  }

  Future _addAlbum(int id, int AlbumId) async{
    NetworkUtil _client = new NetworkUtil.internal();
    var songs =  await _client.getAlbumTracks(AlbumId);
    if(songs.length > 0){
      await _addSongs(id, songs);
    }
  }

  Future _deleteSong(int id, int SongId) async{
    DatabaseClient _db = new DatabaseClient();
    await _db.deleteSongPlaylist(id, SongId);
  }

  Future<void> _getSongbyPlaylist(Store<AppState>store,Playlist playlist) async{
    List<Song> data;
    try {
      DatabaseClient _db = new DatabaseClient();
      data = await _db.getSongByPlaylist(playlist.Id);
      var _audios = data.where((item) =>
      item.SongOwnerId != null && item.SongVKId != null && item.SongHash!=null && item.SongUrl == null).map((x) => "${x
          .SongOwnerId}_${x.SongVKId}_${x.SongHash}").join(',');

      if(_audios.isNotEmpty) {
        var s = data
            .where((item) =>
        item.SongOwnerId != null && item.SongVKId != null)
            .first;
        if (s != null)
          _audios = "$_audios,${s.SongOwnerId}_${s.SongVKId}";

        Map<num, String> urls = await _loadVkAudio(_audios, store.state.authState.user.cookie);

        for (var i = 0; i < data.length; i++) {
          if(urls.containsKey(data[i].SongVKId))
            data[i].SongUrl =  urls[data[i].SongVKId];
        }
      }
    }
    catch(ex){}
    store.dispatch(new PlaylistDetailLoadCompletAction(data,playlist));
  }

  Future<Map<num, String>> _loadVkAudio(String songs,String cookie) async{
    NetworkUtil _client = new NetworkUtil.internal();
    return await _client.getVkAudio(songs, cookie);
  }
}
