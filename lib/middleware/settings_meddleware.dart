import 'package:jrap_mobile/actions/settings_action.dart';
import 'package:jrap_mobile/models/setting.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/services/database.dart';
import 'package:redux/redux.dart';
import 'dart:async';

class SettingsMiddleware extends MiddlewareClass<AppState>{

  SettingsMiddleware();

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async{
    if(action is LoadSettingUserAction)
    {
      var data = await _load(action.userId);
      store.dispatch(new LoadSettingUserCompletAction(data));
    }
    else if(action is UpdateSettingUser){
      var res = await _update(action.data);
      if(res) {
        var data = await _load(action.data.SettingUserId);
        store.dispatch(new LoadSettingUserCompletAction(data));
      }
    }
    else
    {
      next(action);
    }
  }

  Future<List<SettingUser>> _load(String userId) async{
    DatabaseClient _db = DatabaseClient();
    return await _db.getSettings(userId);
  }

  Future<bool> _update(SettingUser item) async{
    DatabaseClient _db = DatabaseClient();
    return await _db.updateSettings(item);
  }
}
