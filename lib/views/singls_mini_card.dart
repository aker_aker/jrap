import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/sheet/album_sheet.dart';

class SinglsMiniCardView extends StatelessWidget {
  final Album singls;
  final Function selectSong;

  SinglsMiniCardView(this.singls, this.selectSong);

  void _showModalSheet() {

    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: AlbumBottomSheet(singls)
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      child: Stack(
        children: <Widget>[
          Positioned.fill(
              child:  Row(
                  children: <Widget>[
                    Container(
                      width: 50.0,
                      height: 50.0,
                      margin: EdgeInsets.only(left: 16.0 , right: 16.0),
                      decoration: new BoxDecoration(
                        border: Border.all(width: 1.5, color: Colors.white),
                      ),
                      child:  CachedNetworkImage(
                        imageUrl: singls.ImageUrl,
                        fit: BoxFit.fill,
                        fadeInCurve: Curves.linear,
                        placeholder: (context, url) => Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                        errorWidget: (context, url, error) =>  Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(bottom: 5.0),
                              child: Text(singls.AlbumName, style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold))
                            ),
                            Padding(
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Text(singls.ArtistTitle, style: TextStyle(color: Colors.white70))
                            ),
                          ],
                        ),
                      ),
                    ),
                  ])),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(onTap: () => selectSong()),
            ),
          ),
          Positioned.fill(
              right: 0,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(20),
                      onTap: () => _showModalSheet(),
                      child: Container(
                        height: 40,
                        width: 40,
                        child: Center(
                          child: Icon(Icons.more_vert, color: Colors.grey, size: 20.0),
                        ),
                      ),
                    ),
                  )
              )
          )
        ],
      )
    );

  }
}