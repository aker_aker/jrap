import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/search.dart';
import 'package:jrap_mobile/pages/search_detail.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/albums.dart';
import 'package:jrap_mobile/views/artist.dart';

class SearchPanelView extends StatelessWidget {

  SearchBlock item;
  SearchPanelView(this.item);
  @override
  Widget build(BuildContext context) {

     List<Widget> _model = List<Widget>();
    _model.add(
        Container(
            padding: EdgeInsets.only(left:16.0, top:8.0, bottom: 8.0, right: 8.0),
            child: Text(item.header, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize:  26.0))
        )
    );

      if(item.items is List<Album>){
        _model.addAll(
            item.items.map((item) =>
                Container(
                  padding: EdgeInsets.only( bottom: 16.0),
                  child: AlbumView(item, () =>  StoreProvider.of<AppState>(
                      Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
                      NavigatePushAction(keyNav: KeyType.over,
                          routeName: AppRoutes.album_detail,
                          arguments: item)
                  ))
                )).take(2).toList()
        );
      }
      else if(item.items is List<Artist>)
      {
        _model.addAll(item.items.map((item) => Container(
            padding: EdgeInsets.only( bottom: 16.0),
            child: ArtistView(item,() =>  StoreProvider.of<AppState>(
                Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
                NavigatePushAction(keyNav: KeyType.over,
                    routeName: AppRoutes.artist_detail,
                    arguments: item)
            )),
          )).take(2).toList()
        );
      }

    return Card(
        color: Color(0xff354657),
        margin: EdgeInsets.all(12.0),
        child:  Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ..._model,
            (item.cntAll > item.cntVisibleBlock) ? Container(
              color: Colors.transparent,
              padding: EdgeInsets.all(12.0),
              child: Center(
                    child: InkWell(
                      child: Text("Ещё ${item.cntAll} ${item.titleMore}" , style: TextStyle(color: Theme.of(context).accentColor, fontWeight: FontWeight.bold, fontSize:  16.0)),
                      onTap: (){
                        Navigator.push(
                          context,
                          NoAnimationMaterialPageRoute(
                            builder: (context) => SearchDetailPage(block: item),
                          ),
                        );
                      },
                    ),
                  )
              ) : Container(),
          ],
        )
    );

  }
}