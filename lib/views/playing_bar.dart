import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/viewmodels/player.dart';
import 'package:jrap_mobile/views/playing_indicator.dart';
import 'package:jrap_mobile/views/sheet/track_sheet.dart';

class PlayerBar extends StatefulWidget {
  PlayerBar();
  @override
  createState() => _PlayerBarState();
}

class _PlayerBarState extends State<PlayerBar> {
  bool panelOpen;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: StoreConnector<AppState, PlayerViewModel>(
          converter: (store) => PlayerViewModel.fromStore(store),
          builder: (_, viewModel) => content(viewModel),
        )
    );
  }

  void showModalSheet(Song song, {Album album}) {

    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: SongBottomSheet(item: song, album: album,)
          );
        }
    );
  }

  Widget content(PlayerViewModel viewModel)
  {
    if(viewModel.queue?.song == null ) return Container();

    final iconPlayPause = PlayingIndicator(
      playing: IconButton(
          tooltip: "Пауза",
          iconSize: 30,
          icon: Icon(
            Icons.pause,
            color: Theme.of(context).primaryIconTheme.color,
          ),
          onPressed: () =>  viewModel.pause()
      ),
      pausing: IconButton(
          tooltip: "Играть",
          iconSize: 30,
          icon: Icon(
            Icons.play_arrow,
            color: Theme.of(context).primaryIconTheme.color,
          ),
          onPressed: () =>  viewModel.play()
      ),
      buffering: Container(
        height: 56,
        width: 56,
        child: Center(
          child: Container(
              height: 24, width: 24, child: CircularProgressIndicator()),
        ),
      ),
      value: viewModel,
    );

    return Card(
      margin: const EdgeInsets.all(0),
      child: Container(
        height: 60,
        color: Theme.of(context).backgroundColor,
        child: Row(
          children: <Widget>[
            IconButton(
                icon: Icon(Icons.more_vert, color: Colors.grey),
                onPressed: () {
                  showModalSheet(viewModel.queue.song);
                }
            ),
            Expanded(
              child: DefaultTextStyle(
                style: TextStyle(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textAlign: TextAlign.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Spacer(),
                    Text(
                        viewModel.queue.song.SongName,
                        //style: Theme.of(context).textTheme.body1,
                        style: TextStyle(color:Colors.white)
                    ),
                    Padding(padding: const EdgeInsets.only(top: 2)),
                    DefaultTextStyle(
                        child:  Text(viewModel.queue.song.ArtistTitle),
                        maxLines: 1,
                        style: TextStyle(color:Colors.white70)
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ),
            //_PauseButton(),
            iconPlayPause,
          ],
        ),
      ),
    );
  }
}