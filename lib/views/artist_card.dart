import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/artist.dart';

class ArtistCardView extends StatelessWidget{

  Artist artist;
  ArtistCardView({@required this.artist});

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 100,
        height: 100,
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        alignment: Alignment.topCenter,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 80,
                height: 80,
                decoration:  BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  border: new Border.all(
                    width: 2.0,
                    color: Colors.white,
                  ),
                ),
                child: ClipOval(
                  child: artist.IsImage ? CachedNetworkImage(
                    imageUrl: artist.ImageUrl,
                    fit: BoxFit.cover,
                    fadeInCurve: Curves.linear,
                    placeholder: (context, url) => Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                    errorWidget: (context, url, error) =>  Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                  ) :  Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                  /*child: new FadeInImage.assetNetwork(
                    placeholder: "assets/images/nophoto.png",
                    image: artist.IsImage ? artist.ImageUrl : "assets/images/nophoto.png",
                    fit: BoxFit.cover,
                    height: 80.0,
                    width: 80.0,
                    //image: new CachedNetworkImageProvider(photos[int].url),
                    alignment: Alignment.topLeft,
                    fadeInDuration: new Duration(milliseconds: 100),
                    fadeInCurve: Curves.linear,
                  ),*/
                ),
              ),
              Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(top: 4.0),
                    child:  new Text(artist.ArtistNickName, textAlign: TextAlign.center, overflow: TextOverflow.ellipsis, maxLines: 2,
                        style: TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                    ),
                  )
              ),
            ],
        ),
      );
  }
}
