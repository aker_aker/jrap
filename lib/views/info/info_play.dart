import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';


class InfoPlayView extends StatefulWidget {

  const InfoPlayView({
    Key key,
    this.color,
    this.size = 50.0,
    this.itemBuilder,
    this.duration = const Duration(milliseconds: 2000),
    this.controller,
  })  : assert(!(itemBuilder is IndexedWidgetBuilder && color is Color) && !(itemBuilder == null && color == null),
  'You should specify either a itemBuilder or a color'),
        assert(size != null),
        super(key: key);

  final Color color;
  final double size;
  final IndexedWidgetBuilder itemBuilder;
  final Duration duration;
  final AnimationController controller;

  @override
  _InfoPlayViewState createState() => _InfoPlayViewState();
}

// #docregion print-state
class _InfoPlayViewState extends State<InfoPlayView> with  SingleTickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _controller = (widget.controller ?? AnimationController(vsync: this, duration: widget.duration))
      ..addListener(() => setState(() {}))
      ..repeat(reverse: true);
    _animation = Tween(begin: -0.8, end: 0.2).animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOut));
  }


  @override
    Widget build(BuildContext context) {

    return Center(
      child: Stack(
        children: <Widget>[
          Transform.scale(
              scale: (0.5  - _animation.value).abs(),
              child: ClipOval(
                child: Container(color: widget.color, width: widget.size, height: widget.size),
             )
          )
        ]
      ),
    );
  }
}