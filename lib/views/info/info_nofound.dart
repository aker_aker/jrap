import 'package:flutter/material.dart';
import 'package:jrap_mobile/utils/strings.dart';

class InfoNotFoundView extends StatelessWidget {

  InfoNotFoundView();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[ Container(
        child: Column(
          children: <Widget>[
            Icon(Icons.search, size: 48.0, color: Colors.white),
            ListTile(
              title: Center(
                  child: Text(info_search_not_fount_title,style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold))
              ),
              subtitle: Center(
                  child: Text(info_search_not_fount_subtitle,style: TextStyle(color: Colors.white70, fontSize: 14.0))
              ),
            )
          ],
        ),
      )],
    );
  }
}