import 'package:flutter/material.dart';
import 'package:jrap_mobile/utils/strings.dart';

class InfoNetworkView extends StatelessWidget {

  Function refresh;
  InfoNetworkView(this.refresh);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[ Container(
        child: Column(
            children: <Widget>[
              Icon(Icons.wifi, size: 48.0, color: Colors.white),
              ListTile(
                title: Center(
                    child: Text(info_network_no_connection_title,style: TextStyle(color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.bold))
                ),
                subtitle: Center(
                  child: Padding(
                   padding: EdgeInsets.only(top: 10),
                   child: Text(info_network_no_connection_subtitle,style: TextStyle(color: Colors.white70, fontSize: 18.0), textAlign: TextAlign.center,),
                  )
                ),
              ),
              Material(
                color: Colors.transparent,
                child: FlatButton(
                  child: Text("Обновить", style: TextStyle(color: Theme.of(context).accentColor)),
                  onPressed: (){
                    refresh();
                  },
                ),
              )
            ],
          ),
      )],
    );
  }
}