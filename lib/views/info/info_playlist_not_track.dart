import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/pages/addtracks.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/utils/strings.dart';

class InfoPlaylistNotTrackView extends StatelessWidget {

  final Playlist playlist;
  InfoPlaylistNotTrackView(this.playlist);

  @override
  Widget build(BuildContext context) {

    void addTrackByPlaylist()
    {
      Navigator.push(
        context,
        NoAnimationMaterialPageRoute(
          builder: (context) => AddTracksPage(playlist: playlist),
        ),
      );
    }

    return Container(
      child: Column(
        children: <Widget>[
          Padding(
              child:  Text(
                  info_playlist_empty_playlist,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.center,
                  style:TextStyle(color: Colors.white, fontSize: 18.0, fontWeight: FontWeight.bold)

              ),
            padding: EdgeInsets.only(bottom:8.0),
          ),
          FlatButton(
            onPressed: (){
              addTrackByPlaylist();
            },
            child:
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(playlist_add_tracks_to_current_playlist, style: TextStyle(color: Colors.white)),
                  )
                ],
              ),
            ),
            color: Color.fromRGBO(222,103,59,1),
            splashColor: Colors.red,
            shape: StadiumBorder(),
          ),
        ],
      ),
    );
  }
}