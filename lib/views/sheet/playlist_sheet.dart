import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';

class PlaylistBottomSheet extends StatelessWidget{
  Playlist item;
  Function(Playlist) _edit;
  Function(Playlist) _delete;
  PlaylistBottomSheet(this.item, this._edit, this._delete);


  void _detailPlaylist(BuildContext context){
    StoreProvider.of<AppState>(
        Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
        NavigatePushAction(keyNav: KeyType.over,
            routeName: AppRoutes.playlist_detail,
            arguments: item)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.white70,))
          ),
          child: ListTile(
              leading:  Container(
                width: 50,
                height: 50,
                color: Colors.white,
                child: (item.Id == 1)
                    ? Icon(Icons.favorite,
                    size: 25.0, color: Theme.of(context).accentColor)
                    : Icon(Icons.playlist_play,
                    size: 30.0, color: Theme.of(context).accentColor),
              ),
              onTap: () => _detailPlaylist(context),
              title: Text(item.Name, style: TextStyle(color:Colors.white)),
              subtitle: Text(item.TracksTitle, style: TextStyle(color:Colors.white)),
              trailing: IconButton(
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  alignment: Alignment.centerRight,
                  icon: Icon(Icons.keyboard_arrow_right, color: Colors.grey, size: 25.0),
                  onPressed: () =>  _detailPlaylist(context),
              )
          )
        ),
        ListTile(
          leading: Icon(Icons.edit, color: Colors.white),
          title: Text("Изменить название", style: TextStyle(color:Colors.white)),
          onTap: () {
              Navigator.of(context).pop();
             _edit(item);
           },
        ),
        ListTile(
          leading: Icon(Icons.delete, color: Colors.white) ,
          title: Text("Удалить", style: TextStyle(color:Colors.white)),
          onTap: () {
            _delete(item);
            Navigator.of(context).pop();
          }
        ),
      ],
    );
  }
}