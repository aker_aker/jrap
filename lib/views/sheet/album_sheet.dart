import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/pages/albums_detail.dart';
import 'package:jrap_mobile/pages/artists_details.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/pop.dart';

class AlbumBottomSheet extends StatelessWidget{
  Album item;
  AlbumBottomSheet(this.item);

  void action_nav_artist(BuildContext context){
    Navigator.of(context).pop();
    StoreProvider.of<AppState>(
        Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
        NavigatePushAction(keyNav: KeyType.over,
            routeName: AppRoutes.artist_detail,
            arguments: item.AlbumArtists.first)
    );
  }

  void nav_album(BuildContext context) {
    Navigator.of(context).pop();
    StoreProvider.of<AppState>(
        Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
        NavigatePushAction(keyNav: KeyType.over,
            routeName: AppRoutes.album_detail,
            arguments: item)
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.white70,))
          ),
          child: ListTile(
              leading:CachedNetworkImage(
                height: 50.0,
                width: 50.0,
                imageUrl: item.ImageUrl,
                fit: BoxFit.fill,
                fadeInCurve: Curves.linear,
                placeholder: (context, url) => Image.asset(
                    "assets/images/nophoto.png",
                    fit: BoxFit.cover),
                errorWidget: (context, url, error) => Image.asset(
                    "assets/images/nophoto.png",
                    fit: BoxFit.cover),
              ),
              onTap: () => nav_album(context),
              title: Text(item.AlbumName, style: TextStyle(color:Colors.white)),
              subtitle: Text(item.ArtistTitle, style: TextStyle(color:Colors.white70)),
              trailing: IconButton(
                  padding: EdgeInsets.symmetric(horizontal: 2.0),
                  alignment: Alignment.centerRight,
                  icon: Icon(Icons.chevron_right, color: Colors.grey, size: 25.0),
                  onPressed: () => nav_album(context)
              )
          )
        ),
        ListTile(
          leading: Icon(Icons.favorite_border, color: Colors.white),
          title: Text("Нравиться", style: TextStyle(color:Colors.white)),
        ),
        ListTile(
            onTap: () => action_nav_artist(context),
            leading: Icon(Icons.person, color: Colors.white),
            title: Text("Перейти к исполнителю", style: TextStyle(color:Colors.white)),
        ),
        ListTile(
          leading: Icon(Icons.add, color: Colors.white),
          title: Text("Добавить в плейлист", style: TextStyle(color:Colors.white)),
            onTap: () async {
              Navigator.of(context).pop();
              var playlistId = await asyncEditDialog(Keys.scaffoldKeys[KeyType.over].currentContext);
              if(playlistId == -1) {
                String playlistName = await asyncInputDialog(Keys.scaffoldKeys[KeyType.over].currentContext);
                if(playlistName!= null && playlistName.isNotEmpty)
                  StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(AddPlaylistAndAddAlbumAction(playlistName, item.AlbumId));
              }
              else if(playlistId > 0){
                StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(AddAlbumByPlaylist(playlistId, item.AlbumId));
              }
            }
        )
      ],
    );
  }
}