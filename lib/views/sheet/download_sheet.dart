import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/pop.dart';
import 'package:jrap_mobile/utils/time.dart';

class DownloadBottomSheet extends StatelessWidget {
  final DownloadSong item;
  DownloadBottomSheet(this.item);

  _addSongByPlaylist(BuildContext context) async
  {
    StoreProvider.of<AppState>(context).dispatch(LoadPlaylistsAction());
    int playlisId = await asyncEditDialog(context);
    if(playlisId == -1) {
      String playlistName = await asyncInputDialog(context);
      if(playlistName!= null && playlistName.isNotEmpty){

      }
      StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(new AddPlaylistAndAddSongAction(playlistName, item.song));
    }
    else if(playlisId > 0){
      StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(new AddSongByPlaylist(playlisId, item.song));
    }
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.white70,))
          ),
          child: ListTile(
              leading:  FadeInImage.assetNetwork(
                placeholder: "assets/images/nophoto.png",
                image: item.album.ImageUrl,
                fit: BoxFit.fill,
                height: 50.0,
                width: 50.0,
                //image: new CachedNetworkImageProvider(photos[int].url),
                alignment: Alignment.topLeft,
                fadeInDuration: new Duration(milliseconds: 100),
                fadeInCurve: Curves.linear,
              ),
              title: Text(item.song.SongName, style: TextStyle(color:Colors.white)),
              subtitle: (item.album.AlbumType != 1) ? Text(item.album.AlbumName, style: TextStyle(color:Colors.white70)) : null,
              trailing: Text(getTimeStampBySeconds(item.song.SongDuration),
                  style: TextStyle(fontSize: 12, color: Colors.white70))
          )
        ),
        ListTile(
            leading: Icon(Icons.add, color: Colors.white) ,
            title: Text("Добавить в плейлист", style: TextStyle(color:Colors.white)),
            onTap: () {
              Navigator.of(context).pop();
              _addSongByPlaylist(context);
            }
        ),
        ListTile(
            leading: Icon(Icons.album, color: Colors.white) ,
            title: Text("Перейти к альбому", style: TextStyle(color:Colors.white)),
            onTap: () {
              Navigator.of(context).pop("nav_album_detail");
            }
        ),
        ListTile(
            leading: Icon(Icons.perm_identity, color: Colors.white) ,
            title: Text("Перейти к исполнитею", style: TextStyle(color:Colors.white)),
            onTap: () {
              Navigator.of(context).pop("nav_artist_detail");
            }
        ),
        ListTile(
          leading: Icon(Icons.delete, color: Colors.white) ,
          title: Text("Удалить", style: TextStyle(color:Colors.white)),
          onTap: () {
            Navigator.of(context).pop("nav_delete_track");
          }
        ),
      ],
    );
  }
}