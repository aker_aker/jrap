import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/actions/like_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/pages/artists_details.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/pop.dart';
import 'package:jrap_mobile/utils/time.dart';

class SongBottomSheet extends StatelessWidget with WidgetsBindingObserver{
  Song item;
  Album album;
  Function(Song) like;
  Function(Song) dislike;
  Function(Song) download;
  Function(Song) add_playlist;
  Function(Song) nav_album;
  Function(Song) nav_artist;
  Function(Song) text;
  SongBottomSheet({
      @required this.item,
      @required this.album,
      this.like,
      this.dislike,
      this.add_playlist,
      this.download,
      this.nav_album,
      this.nav_artist,
      this.text});

  _addSongByPlaylist() async
  {
    var key =  Keys.scaffoldKeys;
    int playlistId = await asyncEditDialog(Keys.scaffoldKeys[KeyType.home].currentContext);
    if(playlistId == -1) {
      String playlistName = await asyncInputDialog(Keys.scaffoldKeys[KeyType.home].currentContext);
      if(playlistName!= null && playlistName.isNotEmpty)
        StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(AddPlaylistAndAddSongAction(playlistName, item));
    }
    else if(playlistId > 0){
        StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(AddSongByPlaylist(playlistId, item));
    }
  }

  _downloadSong(BuildContext context, Song song, Album album) async {
    StoreProvider.of<AppState>(context).dispatch(RequestDownloadSong(song, album));
  }

  @override
  Widget build(BuildContext context)
  {
    if(!item.IsVkLoad) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.white70,))
              ),
              child: ListTile(
                leading: CachedNetworkImage(
                  height: 50.0,
                  width: 50.0,
                  imageUrl: album?.ImageUrl ?? item.AlbymImageUrl,
                  fit: BoxFit.fill,
                  fadeInCurve: Curves.linear,
                  placeholder: (context, url) => Image.asset(
                      "assets/images/nophoto.png",
                      fit: BoxFit.cover),
                  errorWidget: (context, url, error) => Image.asset(
                      "assets/images/nophoto.png",
                      fit: BoxFit.cover),
                ),
                title: Text(item.SongName, style: TextStyle(color:Colors.white)),
                subtitle: Text(item.ArtistTitle, style: TextStyle(color:Colors.white)),
                trailing: Text(getTimeStampBySeconds(item.SongDuration), style: TextStyle(color:Colors.white70)),
              )
          ),
          ListTile(
              leading: Icon(Icons.add, color: Colors.white) ,
              title: Text("Перейти к исполнитею", style: TextStyle(color:Colors.white)),
              onTap: () {
                //_delete(item);
                Navigator.of(context).pop();
                StoreProvider.of<AppState>(
                    Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
                    NavigatePushAction(keyNav: KeyType.over,
                        routeName: AppRoutes.artist_detail,
                        arguments: item.SongArtists.first)
                );
              }
          ),
        ],
      );
    }
    else

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(color: Colors.white70,))
          ),
          child: ListTile(
              leading: CachedNetworkImage(
                height: 50.0,
                width: 50.0,
                imageUrl: album?.ImageUrl ?? item.AlbymImageUrl,
                fit: BoxFit.fill,
                fadeInCurve: Curves.linear,
                placeholder: (context, url) => Image.asset(
                    "assets/images/nophoto.png",
                    fit: BoxFit.cover),
                errorWidget: (context, url, error) => Image.asset(
                    "assets/images/nophoto.png",
                    fit: BoxFit.cover),
              ),
              title: Text(item.SongName, style: TextStyle(color:Colors.white)),
              subtitle: Text(item.ArtistTitle, style: TextStyle(color:Colors.white70)),
              trailing: Text(getTimeStampBySeconds(item.SongDuration), style: TextStyle(color:Colors.white70)),
          )
        ),
        ListTile(
          leading: Icon(Icons.favorite_border, color: Colors.white),
          title: Text("Нравится", style: TextStyle(color:Colors.white)),
          onTap: () {
              Navigator.of(context).pop();
              StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(LikeSong(item));
           },
        ),
        ListTile(
          leading: Icon(
              Icons.file_download,
              color: (item.IsLocalStore==false && item.IsDownloading==false && item.IsVkLoad == true) ? Colors.white : Colors.white12
          ) ,
          enabled: (item.IsLocalStore==false && item.IsDownloading==false && item.IsVkLoad == true),
          title: Text(
              "Скачать",
              style: TextStyle(
                  color: (item.IsLocalStore==false && item.IsDownloading==false && item.IsVkLoad == true) ? Colors.white : Colors.white12
              )
          ),
          onTap: () {
             checkPermission().then((hasGranted) {
              if(hasGranted) {
                _downloadSong(context, item, album);
                Navigator.of(context).pop();
              }
            });
          }
        ),
        ListTile(
            leading: Icon(Icons.add, color: Colors.white) ,
            title: Text("Добавить в плейлист", style: TextStyle(color:Colors.white)),
            onTap: () {
              Navigator.of(context).pop();
              _addSongByPlaylist();
            }
        ),
        /*ListTile(
            leading: Icon(Icons.add, color: Colors.white) ,
            title: Text("Перейти к альбому", style: TextStyle(color:Colors.white)),
            onTap: () {
              //_delete(item);
              Navigator.of(context).pop();
            }
        ),*/
        ListTile(
            leading: Icon(Icons.person, color: Colors.white) ,
            title: Text("Перейти к исполнитею", style: TextStyle(color:Colors.white)),
            onTap: () {
              Navigator.of(context).pop();
              StoreProvider.of<AppState>(
                  Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
                  NavigatePushAction(keyNav: KeyType.over,
                      routeName: AppRoutes.artist_detail,
                      arguments: item.SongArtists.first)
              );
            }
        ),
        /*ListTile(
            leading: Icon(Icons.add, color: Colors.white) ,
            title: Text("Текст", style: TextStyle(color:Colors.white)),
            onTap: () {
              //_delete(item);
              Navigator.of(context).pop();
            }
        ),*/
        ListTile(
            leading: Icon(Icons.not_interested, color: Colors.white) ,
            title: Text("Не нравится", style: TextStyle(color:Colors.white)),
            onTap: () {
              Navigator.of(context).pop();
              StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(DisLikeSong(item));
            }
        ),
      ],
    );
  }
}