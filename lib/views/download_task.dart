import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/task_info.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/sheet/download_sheet.dart';
import 'package:jrap_mobile/views/download_button.dart';

class DownloadTaskView extends StatelessWidget {

  final DownloadTaskInfo _taskinfo;

  DownloadTaskView(this._taskinfo);

  /*void _showModalSheet() {

    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: DownloadBottomSheet(_taskinfo.song,_delete)
          );
        });
   }*/

  @override
  Widget build(BuildContext context) {
    final Orientation orientation = MediaQuery.of(context).orientation;

    return  Container(
      height: 100,
      child: Stack(
        children: <Widget>[
          Positioned.fill(child:Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 4, bottom: 4),
            child: Container(
                height: 60,
                color: Colors.transparent,
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 100.0,
                      height: 100.0,
                      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1.5, color: Colors.white),
                      ),
                      child: CachedNetworkImage(
                        imageUrl: _taskinfo.album.ImageUrl,
                        fit: BoxFit.cover,
                        fadeInCurve: Curves.linear,
                        placeholder: (context, url) => Image.asset(
                            "assets/images/nophoto.png",
                            fit: BoxFit.cover),
                        errorWidget: (context, url, error) => Image.asset(
                            "assets/images/nophoto.png",
                            fit: BoxFit.cover),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Text(_taskinfo.song.SongName,
                                    style: TextStyle(
                                        color: Colors.white, fontWeight: FontWeight.bold))
                            ),
                            Padding(
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Text(_taskinfo.song.ArtistTitle,
                                    style: TextStyle(color: Colors.white70))
                            ),
                            Padding(
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Text(_taskinfo.album.AlbumName,
                                    style: TextStyle(color: Theme.of(context).accentColor))
                            ),
                            //,Text(_dsong.album.AlbumName, style: TextStyle(color: Theme.of(context).accentColor))
                          ],
                        ),
                      ),
                    ),
                  ],
                )
            ),
          )),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                  onTap: null
              ),
            ),
          ),
          Positioned.fill(
              right: 0,
              child: Align(
                  alignment: Alignment.centerRight,
                  child:   DownloadControllView(
                    task: _taskinfo.task,
                    cancelDownload: _taskinfo.cancelTask,
                    deleteDownload: _taskinfo.deleteTask,
                    pauseDownload:  _taskinfo.pauseTask,
                    resumeDownload: _taskinfo.resumeTask,
                    retryDownload:  _taskinfo.retryTask,
                  ),
              )
          )
        ],
      ),
    );

  }
}