import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/models/task_info.dart';


class DownloadControllView extends StatefulWidget {
  final TaskInfo task;
  final Function(TaskInfo) deleteDownload;
  final Function(TaskInfo) cancelDownload;
  final Function(TaskInfo) resumeDownload;
  final Function(TaskInfo) pauseDownload;
  final Function(TaskInfo) retryDownload;

  DownloadControllView({this.task, this.pauseDownload, this.retryDownload, this.resumeDownload, this.deleteDownload, this.cancelDownload});

  @override
  _DownloadControllState createState() => _DownloadControllState();
}

class _DownloadControllState extends State<DownloadControllView> {


  @override
  Widget build(BuildContext context) {
    return (widget.task == null) ? Container() : _buildActionForTask();
  }

  Widget _buildActionForTask() {
    /*if (widget.task.status == DownloadTaskStatus.undefined) {
      return new RawMaterialButton(
        onPressed: () {
          //_requestDownload(task);
        },
        child: new Icon(Icons.file_download),
        shape: new CircleBorder(),
        constraints: new BoxConstraints(minHeight: 32.0, minWidth: 32.0),
      );
    } else*/ if (widget.task.status == DownloadTaskStatus.running) {
      return new RawMaterialButton(
        onPressed: () {
          widget.pauseDownload(widget.task);
        },
        child: new Icon(
          Icons.pause,
          color: Colors.red,
        ),
        shape: new CircleBorder(),
        constraints: new BoxConstraints(minHeight: 32.0, minWidth: 32.0),
      );
    } else if (widget.task.status == DownloadTaskStatus.paused) {
      return new RawMaterialButton(
        onPressed: () {
          widget.resumeDownload(widget.task);
        },
        child: new Icon(
          Icons.play_arrow,
          color: Colors.green,
        ),
        shape: new CircleBorder(),
        constraints: new BoxConstraints(minHeight: 32.0, minWidth: 32.0),
      );
    } else if (widget.task.status == DownloadTaskStatus.complete) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          new Text(
            'Ready',
            style: new TextStyle(color: Colors.green),
          ),
          RawMaterialButton(
            onPressed: () {
              widget.deleteDownload(widget.task);
            },
            child: Icon(
              Icons.delete_forever,
              color: Colors.red,
            ),
            shape: new CircleBorder(),
            constraints: new BoxConstraints(minHeight: 32.0, minWidth: 32.0),
          )
        ],
      );
    } else if (widget.task.status == DownloadTaskStatus.canceled) {
      return new Text('Canceled', style: new TextStyle(color: Colors.red));
    } else if (widget.task.status == DownloadTaskStatus.failed) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          new Text('Failed', style: new TextStyle(color: Colors.red)),
          RawMaterialButton(
            onPressed: () {
              widget.retryDownload(widget.task);
            },
            child: Icon(
              Icons.refresh,
              color: Colors.green,
            ),
            shape: new CircleBorder(),
            constraints: new BoxConstraints(minHeight: 32.0, minWidth: 32.0),
          )
        ],
      );
    } else {
      return null;
    }
  }

  Future<bool> _openDownloadedFile() {
    return FlutterDownloader.open(taskId: widget.task.taskId);
  }

}