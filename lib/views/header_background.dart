import 'dart:ui';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';


class HeaderBackground extends StatelessWidget {
  final String imageUrl;

  const HeaderBackground({Key key, @required this.imageUrl})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        (this.imageUrl==null) ? FadeInImage.assetNetwork(
          placeholder: "assets/images/nophoto.png",
          image: "assets/images/nophoto.png",
          fit: BoxFit.cover,
        ) :
        CachedNetworkImage(
          imageUrl: imageUrl,
          fit: BoxFit.cover,
          fadeInCurve: Curves.linear,
          placeholder: (context, url) =>
              Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
          errorWidget: (context, url, error) =>
              Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
        ),
        BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 24, sigmaY: 24),
          child: Container(color: Colors.black.withOpacity(0.1)),
        )
      ],
    );
  }
}