import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/info/info_play.dart';
import 'package:jrap_mobile/views/sheet/track_sheet.dart';

class TrackView extends StatelessWidget{
  Song song;
  Album album;
  Function(Song) play;
  Function(Song) pause;
  Function() download;
  Function selectSong;
  bool _isPlay;
  final int index;

  TrackView({@required this.song, @required this.album, @required this.index, this.download, this.pause, this.play,this.selectSong})
  {
    _isPlay = false;
  }

  void _showModalSheet() {
    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: SongBottomSheet(item: song, album: album,)
          );
        }
    );
  }

  @override
  Widget build(BuildContext context)
  {
    return StoreConnector<AppState, bool>(
      onInit: (store) {
        _isPlay =  (store.state.playerState.queue.song!=null && store.state.playerState.queue.song.SongId == this.song.SongId);
      },
      converter: (store) => (store.state.playerState.queue.song!=null && store.state.playerState.queue.song.SongId == this.song.SongId),
      builder: (_, isPlay) => content(context, isPlay),
    );
  }

  Widget content(BuildContext context, bool isPlay) {
    _isPlay = isPlay;


      return Container(
        height: 60,
        child:  Stack(
            children: <Widget>[
              Positioned.fill(
                  child: Container(
                    height: 56,
                    color: _isPlay ?  Color(0xff354657) : Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(left: 8, right: 8),
                          width: 40,
                          height: 40,
                          child: Center(
                            child: _isPlay ? InfoPlayView(
                              color: Theme.of(context).accentColor,
                              size: 10,
                              duration: const Duration(milliseconds: 2000),
                            ) : Text(
                                index.toString(),
                                style:  TextStyle(color:Colors.white)
                            ),
                          ),
                        ),
                        Expanded(
                            child: Container(
                              height: 56,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Spacer(),
                                          Text(
                                              song.SongName,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                                          ),
                                          Padding(padding: EdgeInsets.only(top: 3)),
                                          Text(
                                              song.ArtistTitle,
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style:  TextStyle(color:Colors.white70)
                                          ),
                                          Spacer(),
                                        ],
                                      )),
                                ],
                              ),
                            )
                        ),
                      ],
                    ),
                  )
              ),
              Positioned.fill(
                child: Material(
                  color: Colors.transparent,
                  child: InkWell(
                      onTap: song.IsVkLoad ? () => selectSong() : null
                  ),
                ),
              ),
              Positioned.fill(
                  right: 0,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child:  song.IsVkLoad ? IconButton(
                          icon: Icon(Icons.more_vert, color: Colors.grey, size: 20.0),
                          onPressed: () => _showModalSheet()
                      )
                          :Padding(
                        padding: const EdgeInsets.only(left:16.0, right: 16.0),
                        child: Icon(Icons.lock, color: Colors.grey, size: 15.0),
                      )
                  )
              )
            ]
        ),
      );

  }
}