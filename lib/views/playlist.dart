import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/sheet/playlist_sheet.dart';

class PlaylistView extends StatelessWidget {
  Playlist _playlist;
  Function _edit;
  Function _delete;
  Function _selectPlaylist;

  PlaylistView(this._playlist, this._edit, this._delete, this._selectPlaylist);

  void _showModalSheet() {
    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context) {
          return Container(
              color: Theme.of(context).primaryColor,
              child: PlaylistBottomSheet(_playlist, this._edit, this._delete));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 120,
        child: Stack(children: <Widget>[
          Positioned.fill(
              child: Row(
            children: <Widget>[
              Container(
                margin: new EdgeInsets.only( top:8, bottom: 8, left: 16.0, right: 16.0),
                decoration: new BoxDecoration(
                  border: Border.all(width: 1.5, color: Colors.white),
                  color: Colors.white,
                ),
                width: 100,
                height: 100,
                child: (_playlist.Id == 1)
                    ? Icon(Icons.favorite,
                        size: 30.0, color: Theme.of(context).accentColor)
                    : Icon(Icons.playlist_play,
                        size: 40.0, color: Theme.of(context).accentColor),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(right: 32.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text(_playlist.Name,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold))),
                      Text(
                          (_playlist.CountTracks > 0)
                              ? "${_playlist.CountTracks} - трека"
                              : "нет треков",
                          style: TextStyle(color: Colors.white70))
                    ],
                  ),
                ),
              ),
              (this._edit == null && this._delete == null)
                  ? InkWell(
                      child: IconButton(
                          padding: EdgeInsets.symmetric(horizontal: 2.0),
                          alignment: Alignment.centerRight,
                          icon: Icon(Icons.more_vert,
                              color: Colors.grey, size: 20.0),
                          onPressed: null),
                      onTap: _showModalSheet,
                    )
                  : SizedBox.shrink()
            ],
          )),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(onTap: () => _selectPlaylist()),
            ),
          ),
          Positioned.fill(
              right: 0,
              child: Align(
                  alignment: Alignment.centerRight,
                  child: Material(
                    color: Colors.transparent,
                    child: InkWell(
                      borderRadius: BorderRadius.circular(20),
                      onTap: () => _showModalSheet(),
                      child: Container(
                        height: 40,
                        width: 40,
                        child: Center(
                          child: Icon(Icons.more_vert,
                              color: Colors.grey, size: 20.0),
                        ),
                      ),
                    ),
                  )))
        ]));
  }
}
