import 'package:flutter/material.dart';

class TitleSection extends StatelessWidget {
  final String title;
  final double pTop;
  final double pBottom;
  final double pLeft;
  final double pRight;
  final Color color;

  const TitleSection({@required this.title, this.pLeft = 16.0, this.pTop = 8.0, this.pBottom = 8.0, this.pRight = 16.0, this.color });

  @override
  Widget build(BuildContext context) {


    return Container(
        padding: EdgeInsets.only(left:this.pLeft, top:this.pTop, bottom: this.pBottom, right: this.pRight),
        child: Text(title,
            //style: TextStyle(color: Theme.of(context).accentColor, fontWeight: FontWeight.bold, fontSize:  24.0)
          style: Theme.of(context).textTheme.title.merge(
            TextStyle(
              color: color ?? Theme.of(context).accentColor,
              fontWeight: FontWeight.bold,
            )
          ),
        )
    );
  }
}