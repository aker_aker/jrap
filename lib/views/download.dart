import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/pages/albums_detail.dart';
import 'package:jrap_mobile/pages/artists_details.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/sheet/download_sheet.dart';

class DownloadView extends StatelessWidget {

  final DownloadSong _dsong;
  final Function(DownloadSong) _delete;
  final Function selectSong;
  bool _isPlay;

  DownloadView(this._dsong, this._delete, this.selectSong) {
    _isPlay = false;
  }

  void _showModalSheet(BuildContext context) async {

    var res = await showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext _context)  {
          return Container(
              color: Theme.of(_context).primaryColor,
              child: DownloadBottomSheet(_dsong)
          );
        });

    if (res == "nav_album_detail") {
      if(_dsong.album!=null) {
        StoreProvider.of<AppState>(
            Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
            NavigatePushAction(keyNav: KeyType.over,
                routeName: AppRoutes.album_detail,
                arguments: _dsong.album)
        );
      }
    } else if (res == "nav_artist_detail") {
      StoreProvider.of<AppState>(
          Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
          NavigatePushAction(keyNav: KeyType.over,
              routeName: AppRoutes.artist_detail,
              arguments: _dsong.song.SongArtists.first)
      );
    }
    else if(res=="nav_delete_track"){
      _delete(_dsong);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, bool>(
      onInit: (store) {
        _isPlay = (store.state.playerState.queue.song != null &&
            store.state.playerState.queue.song.SongId ==
                this._dsong.song.SongId);
      },
      converter: (store) => (store.state.playerState.queue.song != null &&
          store.state.playerState.queue.song.SongId == this._dsong.song.SongId),
      builder: (_, isPlay) => content(context, isPlay),
    );
  }

  Widget content(BuildContext context, bool isPlay) {
    _isPlay = isPlay;

    return   Container(
        height: 80,
        color: _isPlay ? Color(0xff354657) : Colors.transparent,
        child: Stack(
          children: <Widget>[
            Positioned.fill(child:Padding(
              padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 4, bottom: 4),
              child: Container(
                  height: 60,
                  child: Row(
                    children: <Widget>[
                      Container(
                          decoration: new BoxDecoration(
                            border: Border.all(width: 1.5, color: Colors.white),
                          ),
                          width: 50.0,
                          height: 50.0,
                          child: CachedNetworkImage(
                            imageUrl: _dsong.album.ImageUrl,
                            fit: BoxFit.fill,
                            fadeInCurve: Curves.linear,
                            placeholder: (context, url) => Image.asset(
                                "assets/images/nophoto.png",
                                fit: BoxFit.cover),
                            errorWidget: (context, url, error) => Image.asset(
                                "assets/images/nophoto.png",
                                fit: BoxFit.cover),
                          )
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Padding(
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  child: Text(_dsong.song.SongName,
                                      style: TextStyle(
                                          color: Colors.white, fontWeight: FontWeight.bold))
                              ),
                              Padding(
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  child: Text(_dsong.song.ArtistTitle,
                                      style: TextStyle(color: Colors.white70))
                              ),
                              //,Text(_dsong.album.AlbumName, style: TextStyle(color: Theme.of(context).accentColor))
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
              ),
            )),
            Positioned.fill(
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                    onTap: selectSong
                ),
              ),
            ),
            Positioned.fill(
                right: 0,
                child: Align(
                    alignment: Alignment.centerRight,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(20),
                        onTap: () => _showModalSheet(context),
                        child: Container(
                          height: 40,
                          width: 40,
                          child: Center(
                            child: Icon(Icons.more_vert, color: Colors.grey, size: 20.0),
                          ),
                        ),
                      ),
                    )
                )
            )
          ],
        )
    );
  }
}