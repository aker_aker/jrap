import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/sheet/track_sheet.dart';

class TrackPlaylistView extends StatelessWidget{
  Song song;
  Album album;
  Function(Song) play;
  Function(Song) pause;
  Function() download;
  Function selectSong;
  bool _isPlay;
  final int index;


  TrackPlaylistView({@required this.song, @required this.album, @required this.index, this.selectSong, this.download, this.pause, this.play})
  {
    _isPlay = false;
  }


  void _showModalSheet() {
    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: SongBottomSheet(item: song, album: album,)
          );
        }
    );
  }

  @override
  Widget build(BuildContext context)
  {
    return StoreConnector<AppState, bool>(
      onInit: (store) {
        _isPlay =  (store.state.playerState.queue.song!=null && store.state.playerState.queue.song.SongId == this.song.SongId);
      },
      converter: (store) => (store.state.playerState.queue.song!=null && store.state.playerState.queue.song.SongId == this.song.SongId),
      builder: (_, isPlay) => content(context, isPlay),
    );
  }

  Widget content(BuildContext context, bool isPlay) {
    _isPlay = isPlay;

    return  Container(
      height: 100,
      child: Stack(
        children: <Widget>[
          Positioned.fill(child:Padding(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 4, bottom: 4),
            child: Container(
                height: 60,
                color: _isPlay ? Color(0xff354657) : Colors.transparent,
                child: Row(
                  children: <Widget>[
                    CachedNetworkImage(
                      width: 50.0,
                      height: 50.0,
                      imageUrl: song.AlbymImageUrl,
                      fit: BoxFit.fill,
                      fadeInCurve: Curves.linear,
                      placeholder: (context, url) => Image.asset(
                          "assets/images/nophoto.png", width: 50, height: 50,
                          fit: BoxFit.fill),
                      errorWidget: (context, url, error) => Image.asset(
                          "assets/images/nophoto.png", width: 50, height: 50,
                          fit: BoxFit.fill),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 16.0, right: 16.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Padding(
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Text(song.SongName,
                                    style: TextStyle(
                                        color: Colors.white, fontWeight: FontWeight.bold))
                            ),
                            Padding(
                                padding: EdgeInsets.only(bottom: 5.0),
                                child: Text(song.ArtistTitle,
                                    style: TextStyle(color: Colors.white70))
                            ),
                            //,Text(_dsong.album.AlbumName, style: TextStyle(color: Theme.of(context).accentColor))
                          ],
                        ),
                      ),
                    ),
                  ],
                )
            ),
          )),
          Positioned.fill(
            child: Material(
              color: Colors.transparent,
              child: InkWell(
                  onTap: song.IsVkLoad ? () => selectSong() : null
              ),
            ),
          ),
          Positioned.fill(
              right: 0,
              child: Align(
                  alignment: Alignment.centerRight,
                  child:  song.IsVkLoad ? IconButton(
                      icon: Icon(Icons.more_vert, color: Colors.grey, size: 20.0),
                      onPressed: () => _showModalSheet()
                  )
                      :Padding(
                    padding: const EdgeInsets.only(left:16.0, right: 16.0),
                    child: Icon(Icons.lock, color: Colors.grey, size: 15.0),
                  )
              )
          )
        ],
      ),
    );
  }
}