import 'dart:async';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:jrap_mobile/models/duration_range.dart';
import 'package:jrap_mobile/models/progress.dart';
import 'package:jrap_mobile/services/player.dart';
import 'package:jrap_mobile/utils/time.dart';
import 'package:jrap_mobile/viewmodels/player.dart';

///a seek bar for current position
class DurationProgressBar extends StatefulWidget {
  final PlayerViewModel viewModel;

  DurationProgressBar(this.viewModel);

  @override
  State<StatefulWidget> createState() => DurationProgressBarState();
}

class DurationProgressBarState extends State<DurationProgressBar> {
  bool isUserTracking = false;
  double trackingPosition = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    /*
    if (state.initialized) {

      var duration = state.duration.inMilliseconds;
      var position = isUserTracking
          ? trackingPosition.round()
          : state.position.inMilliseconds;

      durationText = getTimeStampByMilliSeconds(duration);
      positionText = getTimeStampByMilliSeconds(position);

      int maxBuffering = 0;
      for (DurationRange range in state.buffered) {
        final int end = range.end.inMilliseconds;
        if (end > maxBuffering) {
          maxBuffering = end;
        }
      }

      progressIndicator = Stack(
        fit: StackFit.passthrough,
        children: <Widget>[
          SliderTheme(
            data: SliderTheme.of(context).copyWith(
              activeTrackColor : Theme.of(context).accentColor.withOpacity(1),
              inactiveTrackColor: theme.caption.color.withOpacity(0.3),
              trackHeight: 3.0,
              thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0.0),
              thumbColor: Theme.of(context).accentColor,
              overlayColor: Theme.of(context).accentColor.withOpacity(0.3),
              overlayShape: RoundSliderOverlayShape(overlayRadius: 14.0)
            ),
            child: Slider(
              value: position.toDouble().clamp(0.0, duration.toDouble()),
              min: 0.0,
              max: duration.toDouble(),
              onChangeStart: (value) {
                setState(() {
                  isUserTracking = true;
                  trackingPosition = value;
                });
              },
              onChanged: (value) {
                setState(() {
                  trackingPosition = value;
                });
              },
              onChangeEnd: (value) async {
                isUserTracking = false;
//                quiet.seekTo(value.round());
//                if (!quiet.value.playWhenReady) {
//                  quiet.play();
//                }
              },
            ),
          )
        ],
      );
    } else {
      progressIndicator = Slider(
          value: 1.0,
          min: 0.0,
          max: 1.0,
          activeColor: theme.body1.color.withOpacity(0.75),
          inactiveColor: theme.caption.color.withOpacity(0.3),
          onChanged: (_) => {});
    }*/
    var theme =  Theme.of(context).primaryTextTheme;

    int maxBuffering = 0;
    for (DurationRange range in widget.viewModel.buffered) {
      final int end = range.end.inMilliseconds;
      if (end > maxBuffering) {
        maxBuffering = end;
      }
    }
    String durationText;
    String positionText;

    return
      StreamBuilder<Progress>(
          initialData: Progress(
            duration: Duration(milliseconds: 0),
            position: Duration(milliseconds: 0),
          ),
          stream: GetIt.I<PlayerService>().possitionUpdates,
          builder: (context, p) {
            Progress _progress = Progress(
              duration: Duration(milliseconds: 0),
              position: Duration(milliseconds: 0),
            );

            if (p != null && p.hasData) {
              _progress = p.data;
            }

            var duration = _progress.duration.inMilliseconds;
            var position = isUserTracking
                ? trackingPosition.round()
                : _progress.position.inMilliseconds;

            durationText = getTimeStampByMilliSeconds(duration);
            positionText = getTimeStampByMilliSeconds(position);
            return  Column(
              children: <Widget>[
                Stack(
                  fit: StackFit.passthrough,
                  children: <Widget>[
                    SliderTheme(
                      data: SliderTheme.of(context).copyWith(
                          activeTrackColor : Theme.of(context).accentColor.withOpacity(1),
                          inactiveTrackColor: theme.caption.color.withOpacity(0.3),
                          trackHeight: 3.0,
                          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0.0),
                          thumbColor: Theme.of(context).accentColor,
                          overlayColor: Theme.of(context).accentColor.withOpacity(0.3),
                          overlayShape: RoundSliderOverlayShape(overlayRadius: 14.0)
                      ),
                      child: Slider(
                        value: position.toDouble().clamp(0.0, duration.toDouble()),
                        min: 0.0,
                        max: duration.toDouble(),
                        onChangeStart: (value) {
                          setState(() {
                            isUserTracking = true;
                            trackingPosition = value;
                          });
                        },
                        onChanged: (value) {
                          setState(() {
                            trackingPosition = value;
                          });
                        },
                        onChangeEnd: (value) async {
                          isUserTracking = false;
                          widget.viewModel.seekTo(value.round());
                          if (!widget.viewModel.playWhenReady) {
                            widget.viewModel.play();
                          }
                        },
                      ),
                    )
                  ],
                ),
                Padding(
                    padding: EdgeInsets.symmetric(vertical: 2, horizontal: 12),
                    child:
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(positionText ?? "00:00", style: TextStyle(color: Colors.white70, fontSize: 13.0)),
                        Padding(padding: EdgeInsets.only(left: 4)),
                        Padding(padding: EdgeInsets.only(left: 4)),
                        Text(durationText ?? "00:00", style: TextStyle(color: Colors.white70, fontSize: 13.0)),
                      ],
                    )
                ),
              ],
            );
      });
  }
}
