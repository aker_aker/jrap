
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/strings.dart';

class BottomBar extends StatelessWidget
{
  final Stream<double> stream;
  final double bottomHeight;
  final Function tabClick;
  BottomBar({this.stream, this.tabClick, this.bottomHeight = 80.0});


  @override
  Widget build(BuildContext context)
  {
    return StoreConnector<AppState, int>(
      converter: (store) {
        return store.state.homeState.tabIndex;
      },
      builder: (_, viewModel) => content(viewModel),
    );
  }

  Widget content(int currentTab)
  {
    return StreamBuilder<double>(
        initialData: 0.0,
        stream: stream,
        builder: (context, position) {
          double _hBottom = 0.0;
          if (position != null && position.hasData) {
            _hBottom = position.data * -80.0;
          }

          return Positioned(
            bottom: _hBottom,
            height: bottomHeight,
            width:  MediaQuery.of(context).size.width,
            child:BottomNavigationBar(
                items: [
                  BottomNavigationBarItem( title: Text(realeses_title), icon: Icon(Icons.home)),
                  BottomNavigationBarItem( title: Text(albums_title), icon: Icon(Icons.album)),
                  BottomNavigationBarItem( title: Text(singls_title), icon: Icon(Icons.menu)),
                  BottomNavigationBarItem( title: Text(artists_title), icon: Icon(Icons.album))
                ],
                type:  BottomNavigationBarType.fixed,
                currentIndex:  currentTab,
                unselectedItemColor: Colors.white,
                selectedItemColor: Theme.of(context).accentColor,
                backgroundColor: Theme.of(context).primaryColor,
                onTap: (index) async {
                  //Keys.navKey[KeyType.over].currentState.popUntil(ModalRoute.withName('/'));
                  //Keys.navKey[KeyType.over].currentState.pop();
                  tabClick(index,currentTab);
                }
            ),
          );
        }
    );
  }
}