import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/pop.dart';
import 'package:jrap_mobile/viewmodels/player.dart';
import 'package:jrap_mobile/views/playing_indicator.dart';
import 'package:jrap_mobile/views/playing_progress.dart';
import 'package:jrap_mobile/views/sheet/track_sheet.dart';

class PlayingPage extends StatefulWidget {
  final PlayerViewModel viewModel;
  final Function close;

  PlayingPage({@required this.viewModel, this.close});

  @override
  _PlayingPageState createState() {
    return _PlayingPageState();
  }
}

class _PlayingPageState extends State<PlayingPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: StoreConnector<AppState, PlayerViewModel>(
        converter: (store) => PlayerViewModel.fromStore(store),
        builder: (_, viewModel) => content(viewModel),
      )
    );
  }

  void showModalSheet(Song song, {Album album}) {

    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: SongBottomSheet(item: song, album: album,)
          );
        }
    );
  }

  Widget content(PlayerViewModel viewModel){
    return
      (viewModel.queue?.songs.length > 0)
         ?
            Material(
          color: Theme.of(context).backgroundColor,
          child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 60,
                  child:
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      IconButton(
                          icon: Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 36),
                          onPressed: () {
                            //Свернуть
                            widget.close();
                          }
                      ),
                      Icon(Icons.more_horiz, color: Colors.transparent, size: 24),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width *0.8,
                  height: MediaQuery.of(context).size.width *0.8,
                  child: CachedNetworkImage(
                    width: MediaQuery.of(context).size.width *0.8,
                    height: MediaQuery.of(context).size.width *0.8,
                    imageUrl: viewModel.queue.song.AlbymImageUrl,
                    fit: BoxFit.fill,
                    fadeInCurve: Curves.linear,
                    placeholder: (context, url) => Image.asset(
                        "assets/images/nophoto.png",
                        fit: BoxFit.fill),
                    errorWidget: (context, url, error) => Image.asset(
                        "assets/images/nophoto.png",
                        fit: BoxFit.fill),
                  )
                ),
                Expanded(
                    flex: 1,
                    child:Padding(
                      padding: EdgeInsets.only(left: 40, right: 40, top: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Expanded(
                            child:  Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  viewModel.queue.song.SongName,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(fontSize: 17, color: Colors.white),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top:8),
                                  child: Text(
                                    viewModel.queue.song.ArtistTitle,
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .body1
                                        .copyWith(fontSize: 14, color:Colors.white70),
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            width: 20,
                            child: IconButton(
                                icon: Icon(Icons.more_vert, color: Colors.grey, size: 20.0),
                                onPressed: () => showModalSheet(viewModel.queue.song)
                            ),
                          )
                        ],
                      ),
                    )
                ),
                DurationProgressBar(viewModel),
                Expanded(
                  flex: 1,
                  child: _ControllerBar(viewModel),
                ),
                Expanded(
                    flex: 1,
                    child:_OperationBar(music:viewModel.queue.song, viewModel: viewModel)
                ),
              ]
          )
      )
         :
      SizedBox.shrink();
  }
}

///player controller
/// pause,play,play next,play previous...
class _ControllerBar extends StatelessWidget {

   PlayerViewModel viewModel;
  _ControllerBar(this.viewModel);

  @override
  Widget build(BuildContext context) {
    var color = Colors.white70;
    var colorMain = Colors.white;
    final iconSize = 24.0;
    final iconPlayPause = PlayingIndicator(
      playing: IconButton(
          tooltip: "Пауза",
          iconSize: 80,
          padding: EdgeInsets.all(0),
          icon: Icon(
            Icons.pause_circle_filled,
            color: colorMain,
          ),
          onPressed: () => viewModel.pause()
      ),
      pausing: IconButton(
            tooltip: "Играть",
            iconSize: 80,
            padding: EdgeInsets.all(0),
            icon: Icon(
              Icons.play_circle_filled,
              color: colorMain,
            ),
            onPressed: () => viewModel.play()
        ),
      buffering: Container(
        height: 56,
        width: 56,
        padding: EdgeInsets.all(0),
        child: Center(
          child: Container(
              height: 24, width: 24, child: CircularProgressIndicator()
          ),
        ),
      ),
      value: viewModel,
    );

    return Container(
      padding: EdgeInsets.symmetric(vertical: 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
              icon: Icon(
                Icons.not_interested,
                color: color,
                size: iconSize,
              ),
              onPressed: () {
                /*if (liked) {
                LikedSongList.of(context).dislikeMusic(music);
              } else {
                LikedSongList.of(context).likeMusic(music);
              }*/
          }),
          Row(
            children: <Widget>[
              IconButton(
                  iconSize: 42,
                  tooltip: "Предыдущий трек",
                  icon: Icon(
                    Icons.skip_previous,
                    color: colorMain,
                  ),
                  onPressed: () {
                    viewModel.prev();
                  }),
              iconPlayPause,
              IconButton(
                  tooltip: "Следующий трек",
                  iconSize: 42,
                  icon: Icon(Icons.skip_next, color: colorMain),
                  onPressed: () {
                    viewModel.next();
                  }),
            ],
          ),
          IconButton(
              icon: Icon(
                Icons.favorite_border,
                color: color,
                size: iconSize,
              ),
              onPressed: () {
                /*if (liked) {
                LikedSongList.of(context).dislikeMusic(music);
              } else {
                LikedSongList.of(context).likeMusic(music);
              }*/
          }),
        ],
      ),
    );
  }
}

///player bar
/// download,playmode,shut....
class _OperationBar extends StatelessWidget {

  final PlayerViewModel viewModel;
  static const iconColor = Colors.white70;
  static const iconDisabledColor = Colors.white12;
  static const iconSize = 24.0;
  final Song music;

  _OperationBar({Key key, this.music, @required this.viewModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    //final liked = LikedSongList.contain(context, music);

    Widget getPlayModeIcon(context, Color color, RepeatMode playMode)
    {
      switch (playMode) {
        case RepeatMode.off:
          return Icon(
            Icons.repeat_one,
            color: iconDisabledColor,
            size: iconSize,
          );
        case RepeatMode.single:
          return Icon(
            Icons.repeat_one,
            color: color,
            size: iconSize,
          );
        case RepeatMode.sequence:
          return Icon(
            Icons.repeat,
            color: color,
            size: iconSize,
          );
      }
    }

    Widget getShuffleModeIcon(context, ShuffleMode shuffleMode)
    {
      switch (shuffleMode) {
        case ShuffleMode.noshuffle:
          return const Icon(
            Icons.shuffle,
            color: iconDisabledColor,
            size: iconSize,
          );
        case ShuffleMode.shuffle:
          return const Icon(
            Icons.shuffle,
            color: iconColor,
            size: iconSize,
          );
      }
    }

    if(music == null) return SizedBox.shrink();

    return Container(
      padding: EdgeInsets.symmetric(vertical: 0.0),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
              icon: getPlayModeIcon(context, iconColor, viewModel.mode),
              onPressed: () {
                viewModel.changePlayMode();
              }
           ),
          IconButton(

              icon: Icon(
                Icons.file_download,
                color: music.IsLocalStore ? iconDisabledColor : iconColor,
                size: iconSize,
              ),
              onPressed: music.IsLocalStore ? null : (){
                checkPermission().then((hasGranted) {
                  if(hasGranted) {
                    viewModel.downloadSong(music, null);
                  }
                });
              }),
          IconButton(
              icon: getShuffleModeIcon(context, viewModel.shuffleMode),
              onPressed: () {
                if (music == null) {
                  return;
                }
              }),
        ],
      )
    );
  }
}

