import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/views/sheet/album_sheet.dart';

class AlbumView extends StatelessWidget {
  final Album album;
  final Function navAlbumDetail;

  const AlbumView(this.album, this.navAlbumDetail);

  void _showModalSheet() {
    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context) {
          return Container(
              color: Theme.of(context).primaryColor,
              child: AlbumBottomSheet(album));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 120,
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              child: Row(children: <Widget>[
                Container(
                  width: 100.0,
                  height: 100.0,
                  margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1.5, color: Colors.white),
                  ),
                  child: Hero(
                    tag: "album_image_${album.AlbumId}",
                    child: CachedNetworkImage(
                      imageUrl: album.ImageUrl,
                      fit: BoxFit.cover,
                      fadeInCurve: Curves.linear,
                      placeholder: (context, url) => Image.asset(
                          "assets/images/nophoto.png",
                          fit: BoxFit.cover),
                      errorWidget: (context, url, error) => Image.asset(
                          "assets/images/nophoto.png",
                          fit: BoxFit.cover),
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(bottom: 5.0),
                            child: Text(album.AlbumName,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 2,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold))),
                        Padding(
                            padding: EdgeInsets.only(bottom: 5.0),
                            child: Text(album.ArtistTitle,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(color: Colors.white70))),
                        Padding(
                            padding: EdgeInsets.only(bottom: 5.0),
                            child: Text(album.AlbumYear.toString(),
                                style: TextStyle(
                                    color: Theme.of(context).accentColor))),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
            Positioned.fill(
              child: Material(
                color: Colors.transparent,
                child: InkWell(onTap: () => navAlbumDetail()),
              ),
            ),
            Positioned.fill(
                right: 0,
                child: Align(
                    alignment: Alignment.centerRight,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        borderRadius: BorderRadius.circular(20),
                        onTap: () => _showModalSheet(),
                        child: Container(
                          height: 40,
                          width: 40,
                          child: Center(
                            child: Icon(Icons.more_vert, color: Colors.grey, size: 20.0),
                          ),
                        ),
                      ),
                    )
                )
            )
          ],
        ));
  }
}
