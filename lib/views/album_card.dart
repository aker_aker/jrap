import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';

class AlbumCardView extends StatelessWidget{
  final Album album;
  final double width;
  final double height;

  AlbumCardView({Key key, @required this.album, this.width = 120,  this.height = 120.0 }):super(key: key);


  @override
  Widget build(BuildContext context) {

    return
      Container(
        width: width,
        height: height,
        padding: EdgeInsets.symmetric(horizontal: 8.0),
        alignment: Alignment.topLeft,
        child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                width: width,
                height: height,
                margin: EdgeInsets.only(bottom: 4.0),
                  decoration:  BoxDecoration(
                      border: Border.all(width: 1.5, color: Colors.white)
                  ),
                  child:  /*FadeInImage.assetNetwork(
                    placeholder: "assets/images/nophoto.png",
                    image: album.ImageUrl,
                    fit: BoxFit.cover,
                    width: width,
                    height: height,
                    //image: new CachedNetworkImageProvider(photos[int].url),
                    fadeInDuration: new Duration(milliseconds: 100),
                    fadeInCurve: Curves.linear,
                  )*/
                  CachedNetworkImage(
                    imageUrl: album.ImageUrl,
                    fit: BoxFit.cover,
                    fadeInCurve: Curves.linear,
                    placeholder: (context, url) => Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                    errorWidget: (context, url, error) =>  Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                  ),
              ),
              Expanded(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(album.AlbumName,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(fontSize: 14,color:Colors.white, fontWeight: FontWeight.bold)
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 4.0),
                      child: Text(album.ArtistTitle, style: TextStyle(fontSize: 12, color: Colors.white70)),
                    )

                  ],
                ),
              ),
            ],
        ),
      );
  }
}
