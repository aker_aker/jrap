import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/artist.dart';

class ArtistView extends StatelessWidget {
  final Artist artist;
  final Function navArtistDetail;

  const ArtistView(this.artist, this.navArtistDetail);

  @override
  Widget build(BuildContext context) {

    return Container(
      height: 90,
      margin: EdgeInsets.symmetric(vertical: 4.0),
      child: Stack(
          children: <Widget>[
            Positioned.fill(
                child: Row(
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(left: 16.0),
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                          // Circle shape
                          shape: BoxShape.circle,
                          color: Colors.white,
                          // The border you want
                          border: new Border.all(
                            width: 2.0,
                            color: Colors.white,
                          ),
                        ),
                        child: ClipOval(
                          child: artist.IsImage ? CachedNetworkImage(
                            imageUrl: artist.ImageUrl,
                            fit: BoxFit.cover,
                            fadeInCurve: Curves.linear,
                            placeholder: (context, url) => Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                            errorWidget: (context, url, error) =>  Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                          ):
                          Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                        ),
                      ),
                      Expanded(
                          child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 8.0),
                              child: ListTile(
                                title: new Row(
                                  children: <Widget>[
                                    new Expanded(
                                        child: new Text(artist.ArtistNickName,
                                            style: TextStyle(color: Colors.white))),
                                  ],
                                ),
                                subtitle: Text(artist.BriefInfo,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(color: Colors.white70)),
                              )))
                    ]
                )),
            Positioned.fill(
              child: Material(
                color: Colors.transparent,
                child: InkWell(
                    onTap: () => navArtistDetail()
                ),
              ),
            ),
          ])
    );
  }
}
