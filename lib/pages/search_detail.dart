import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/models/search.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/viewmodels/search_detail.dart';
import 'package:jrap_mobile/views/albums.dart';
import 'package:jrap_mobile/views/artist.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class SearchDetailPage extends StatefulWidget {
  SearchBlock<dynamic> block;
  SearchDetailPage({Key key, @required this.block}) : super(key: key);

  @override
  _SearchDetailPageState createState() => _SearchDetailPageState();
}


class _SearchDetailPageState extends State<SearchDetailPage> {

  @override
  Widget build(BuildContext context) {

    return  StoreConnector<AppState, SearchDetailViewModel>(
        onInit: (store) {
          //нуно запихнуть албум в стате
        },
        converter: (store) => SearchDetailViewModel.fromStore(store, widget.block),
        builder: (_, viewModel) =>
            Scaffold(
              backgroundColor: Theme.of(context).primaryColor,
              appBar:  AppBar(
                title:  Text(viewModel.search),
              ),
              body: content(viewModel, context)
          )
    );
  }

  Widget content(SearchDetailViewModel viewModel, BuildContext context)
  {
    if(!viewModel.isLoading) {
      return Container(
          child: Stack(
            fit: StackFit.expand,
            alignment: Alignment.topCenter,
            children: <Widget>[
              LazyLoadScrollView(
                onEndOfPage: () => viewModel.loadMore(viewModel.data.length),
                child: ListView.builder(
                  itemCount: viewModel.block.cntAll,
                  itemBuilder: (_context, index) {
                    if(viewModel.block.typeBlock == MainObjType.album || viewModel.block.typeBlock == MainObjType.singl){
                      return AlbumView(viewModel.data[index],  () => viewModel.navAlbumDetail(viewModel.data[index]));
                    }
                    else
                    {
                      return Padding(
                          padding:  EdgeInsets.symmetric(vertical: 8.0),
                          child: ArtistView(viewModel.data[index],  () => viewModel.navArtistDetail(viewModel.data[index]))
                      );
                    }
                  },
                ),
              ),
            ],
          )
      );
    }

    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}