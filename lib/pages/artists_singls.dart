import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/viewmodels/artist_singls.dart';
import 'package:jrap_mobile/views/albums.dart';
import 'package:jrap_mobile/views/info/info_network.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';


const double HEIGHT_HEADER = 230 + kToolbarHeight;
const double HEIGHT_EXPAND = 180;

class ArtistSinglsPage extends StatefulWidget {
  final Artist artist;

  ArtistSinglsPage({Key key, this.artist}) : super(key: key);

  @override
  _ArtistSinglsPageState createState() => _ArtistSinglsPageState();

}

class _ArtistSinglsPageState extends State<ArtistSinglsPage> {
  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      //key: Keys.scaffoldKey[KeyType.album],
        appBar: AppBar(
          title: const Text("Все синглы"),
        ),
        body: LayoutBuilder(
            builder: (context, constraints)=> SafeArea(
                child: new Container(
                    color: Theme.of(context).primaryColor,
                    child: StoreConnector<AppState, ArtistSinglsViewModel>(
                      onInit: (store) {
                        store.dispatch(LoadArtistSinglsAction(widget.artist.ArtistId));
                      },
                      converter: (store) => ArtistSinglsViewModel.fromStore(store),
                      builder: (_, viewModel) => content(viewModel, constraints),
                    )
                )
            )
        )
    );
  }


  Widget content(ArtistSinglsViewModel viewModel, BoxConstraints constraints)
  {
    if(!viewModel.isLoading && !viewModel.isError) {
      return new Container(
          child: new Stack(
            fit: StackFit.expand,
            alignment: Alignment.topCenter,
            children: <Widget>[
              ListView.builder(
                itemCount: viewModel.data.length,
                itemBuilder: (context, index) {
                  return  AlbumView(viewModel.data[index], () => viewModel.navAlbumDetail(viewModel.data[index]));
                },
              ),
            ],
          )
      );
    }
    else if(viewModel.isError){
      return InfoNetworkView(() => viewModel.loadMore(widget.artist.ArtistId));
    }
    else return Center(
        child: new CircularProgressIndicator(),
      );
  }
}