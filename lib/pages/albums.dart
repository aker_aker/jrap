import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/albums_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/viewmodels/albums.dart';
import 'package:jrap_mobile/views/albums.dart';
import 'package:jrap_mobile/views/info/info_network.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class AlbumsPage extends StatelessWidget {
  static RefreshController _refreshController =  RefreshController(initialRefresh: false);


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        key: Keys.scaffoldKeys[KeyType.album],
        appBar: AppBar(
          title: Text(albums_title),
          leading: IconButton(  icon: const Icon(Icons.menu),
              tooltip: 'Меню',
              onPressed: () => Keys.scaffoldKeys[KeyType.home].currentState.openDrawer()
          ),
        ),
        body: SafeArea(
            child:  Container(
                color: Theme.of(context).primaryColor,
                child: StoreConnector<AppState, AlbumsViewModel>(
                  onInit: (store) {
                    store.dispatch(LoadAlbumsAction(0));
                  },
                  converter: (store) => AlbumsViewModel.fromStore(store),
                  builder: (_, viewModel) => content(viewModel),
                )
            )
        )
    );
  }

  Widget content(AlbumsViewModel viewModel)
  {
    if(!viewModel.isLoading && !viewModel.isError) {
      _refreshController.loadComplete();
      _refreshController.refreshCompleted();

      return
        SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            onRefresh: () =>  viewModel.pullRefresh(),
            onLoading: () =>  viewModel.loadMore(),
            controller: _refreshController,
            child: ListView.builder(
              itemCount: viewModel.data.length,
              itemBuilder: (context, index) {
                return AlbumView(viewModel.data[index],  () => viewModel.navAlbumDetail(viewModel.data[index]));
              },
            )
        );
    }
    else if(viewModel.isError){
      return InfoNetworkView(
              () => viewModel.loadMore()
      );
    }
    else return  const Center(
        child: CircularProgressIndicator(),
      );
  }
}