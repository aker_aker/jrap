import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/viewmodels/download_task.dart';
import 'package:jrap_mobile/views/download_task.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class TasksPage extends StatefulWidget {
  TasksPage({Key key}) : super(key: key);

  @override
  _TasksPageState createState() => _TasksPageState();
}


class _TasksPageState extends State<TasksPage> {
  static RefreshController _refreshController =  RefreshController(initialRefresh: false);

  Future<String> _asyncInputDialog(BuildContext context) async {
    String playlistName = '';
    return showDialog<String>(
      context: context,
      barrierDismissible: false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Новый плейлист'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        hintText: 'название'),
                    onChanged: (value) {
                      playlistName = value;
                    },
                  ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Сохранить'),
              onPressed: () {
                Navigator.of(context).pop(playlistName);
              },
            ),
            FlatButton(
              child: Text('Отмена'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Очередь загрузок'),
        actions: <Widget>[
          /*IconButton(
            icon: Icon(
              Icons.pause,
              color: Colors.white,
              size: 30.0,
            ),
            tooltip: 'Остановить всё',
            onPressed: () async {
              /*String playlistName = await _asyncInputDialog(context);
              if(playlistName!= null && playlistName.isNotEmpty)
                StoreProvider.of<AppState>(context).dispatch(new AddPlaylistAction(playlistName));*/
            },
          )*/
        ],
      ),
      body: LayoutBuilder(
          builder: (context, constraints) =>
              SafeArea(
                  child: new Container(
                      color: Theme
                          .of(context)
                          .primaryColor,
                      child: new StoreConnector<AppState, DownloadTaskViewModel>(
                        onInit: (store) {
                          store.dispatch(new LoadDownloadTaskAction());
                        },
                        converter: (store) =>  DownloadTaskViewModel.fromStore(store),
                        builder: (_, viewModel) => content(viewModel, constraints),
                      )
                  )
              )
      ),
    );
  }

  Widget content(DownloadTaskViewModel viewModel, BoxConstraints constraints) {

    if (viewModel.status == LoadingStatus.success) {

      return   Container(
          padding: EdgeInsets.only(bottom: 60.0),
          child:
          SmartRefresher(
              enablePullDown: false ,
              enablePullUp: true,
              onRefresh: () =>  viewModel.pullRefresh(),
              controller: _refreshController,
              child: ListView.builder(
                itemCount: viewModel.data.length,
                itemBuilder: (context, index) {
                  return (viewModel.data.length > 0) ? DownloadTaskView(viewModel.data[index]) :   SizedBox.shrink();
                },
              )
          )
      );
    }
    else
      return new Center(
        child: new CircularProgressIndicator(),
      );
  }
}