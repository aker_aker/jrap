import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/realeses_actions.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/pages/albums_detail.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/realeses.dart';
import 'package:jrap_mobile/views/album_card.dart';
import 'package:jrap_mobile/views/artist_card.dart';
import 'package:jrap_mobile/views/info/info_network.dart';
import 'package:jrap_mobile/views/singls_mini_card.dart';
import 'package:jrap_mobile/views/title_section.dart';

class ReleasesPage extends StatefulWidget {
  ReleasesPage({Key key}) : super(key: key);

  @override
  _ReleasesPageState createState() => _ReleasesPageState();
}


class _ReleasesPageState extends State<ReleasesPage> {

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      key: Keys.scaffoldKeys[KeyType.realeses],
        appBar:  AppBar(
          leading: IconButton(
                icon: const Icon(Icons.menu),
                onPressed: () { Keys.scaffoldKeys[KeyType.home].currentState.openDrawer(); },
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              ),
          title:  Text(realeses_title),

        ),
        body: LayoutBuilder(
          builder: (context, constraints)=>SafeArea(
            child: new Container(
            color: Theme.of(context).primaryColor,
            child: new StoreConnector<AppState, RealesesViewModel>(
                onInit: (store) {
                  store.dispatch(new LoadRealesesAction());
                },
                converter: (store) => RealesesViewModel.fromStore(store),
                builder: (_, viewModel) => content(viewModel, constraints),
              )
          )
        )
      )
    );
  }

  Widget content(RealesesViewModel viewModel, BoxConstraints constraints)
  {
   if(!viewModel.isLoading && !viewModel.isError)
   {
     final _width = MediaQuery.of(context).size.width;
     return Container(
      child: CustomScrollView(
          slivers: <Widget>[
            SliverList(
                delegate: SliverChildListDelegate(
                    [
                      const TitleSection(title: albums_title),
                      Container(
                          height: MediaQuery.of(context).size.width/2 +50,
                          width: _width,
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child:  ListView.builder(
                              itemBuilder: (BuildContext context, int index) {
                                return GestureDetector(
                                  child: AlbumCardView(
                                      album: viewModel.data.albums[index],
                                      height: (_width /2) -8.0,
                                      width: (_width /2) -8.0),
                                  onTap: () => viewModel.navAlbumDetail(viewModel.data.albums[index])
                                );
                              },
                              itemCount:  viewModel.data.albums.length,
                              scrollDirection:  Axis.horizontal,)
                      ),
                      const TitleSection(title: singls_title),
                      Container(
                          height: viewModel.data.singls.length * 80.0,
                          margin: EdgeInsets.all(0),
                          padding: EdgeInsets.all(0),
                          width: _width,
                          child: ListView(
                            scrollDirection:  Axis.vertical,
                            physics: const NeverScrollableScrollPhysics() ,
                            children:  viewModel.data.singls.map((item) =>
                                SinglsMiniCardView(item, () => viewModel.navAlbumDetail(item))
                            ).toList(),
                          )
                      ),
                      const TitleSection(title: artists_title),
                      Container(
                        height: 200.0,
                        width: _width,
                        child: ListView(
                          scrollDirection:  Axis.horizontal,
                          children:   viewModel.data.artists.map((item) =>
                              GestureDetector(
                                  child: ArtistCardView(artist:item),
                                  onTap: () => viewModel.navArtistDetail(item)
                              )).toList(),
                        )

                  ),
                ],
              ),
            ),
          ],
      ),
    );
   }
   else if(viewModel.isError){
     if(viewModel.data == null)
       return InfoNetworkView(() => viewModel.load());
   }

   return  Center(
      child: CircularProgressIndicator(),
    );
  }

}