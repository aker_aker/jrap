import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/viewmodels/artist_songs.dart';
import 'package:jrap_mobile/views/info/info_network.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:jrap_mobile/views/track.dart';

const double HEIGHT_HEADER = 230 + kToolbarHeight;
const double HEIGHT_EXPAND = 180;

class ArtistSongsPage extends StatefulWidget {
  final Artist artist;

  ArtistSongsPage({Key key, this.artist}) : super(key: key);

  @override
  _ArtistSongsPageState createState() => _ArtistSongsPageState();

}

class _ArtistSongsPageState extends State<ArtistSongsPage> {
  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      //key: Keys.scaffoldKey[KeyType.album],
        appBar: AppBar(
          title: const Text("Все треки"),
        ),
        body: LayoutBuilder(
            builder: (context, constraints)=> SafeArea(
                child: new Container(
                    color: Theme.of(context).primaryColor,
                    child: StoreConnector<AppState, ArtistSongsViewModel>(
                      onInit: (store) {
                        store.dispatch(LoadArtistSongsAction(widget.artist.ArtistId));
                      },
                      converter: (store) => ArtistSongsViewModel.fromStore(store),
                      builder: (_, viewModel) => content(viewModel, constraints),
                    )
                )
            )
        )
    );
  }


  Widget content(ArtistSongsViewModel viewModel, BoxConstraints constraints)
  {
    if(!viewModel.isLoading && !viewModel.isError) {
      return new Container(
          child: new Stack(
            fit: StackFit.expand,
            alignment: Alignment.topCenter,
            children: <Widget>[
              LazyLoadScrollView(
                onEndOfPage: () => viewModel.loadMore(viewModel.data.length),
                child: ListView.builder(
                  itemCount: viewModel.data.length,
                  itemBuilder: (context, index) {
                    return TrackView(
                        song: viewModel.data[index],
                        album: null,
                        index: index+1,
                    );
                  },
                ),
              ),
            ],
          )
      );
    }
    else if(viewModel.isError){
      return InfoNetworkView(() => viewModel.loadMore(widget.artist.ArtistId));
    }
    else return Center(
        child: new CircularProgressIndicator(),
      );
  }
}