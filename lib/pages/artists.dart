import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/artists.dart';
import 'package:jrap_mobile/views/artist.dart';
import 'package:jrap_mobile/views/info/info_network.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class ArtistsPage extends StatefulWidget {
  ArtistsPage({Key key}) : super(key: key);

  @override
  _ArtistsPageState createState() => _ArtistsPageState();
}

class _ArtistsPageState extends State<ArtistsPage> {
  static RefreshController _refreshController =  RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        appBar:  AppBar(
          title: new Text(artists_title),
          leading: IconButton(  icon: const Icon(Icons.menu),
              tooltip: 'Меню',
              onPressed: () => Keys.scaffoldKeys[KeyType.home].currentState.openDrawer()
          ),
        ),
        body: LayoutBuilder(
            builder: (context, constraints)=>SafeArea(
                child: new Container(
                    color: Theme.of(context).primaryColor,
                    child: new StoreConnector<AppState, ArtistsViewModel>(
                      onInit: (store) {
                        store.dispatch(LoadArtistsAction(0));
                      },
                      converter: (store) => ArtistsViewModel.fromStore(store),
                      builder: (_, viewModel) => content(viewModel, constraints),
                    )
                )
            )
        )
    );
  }

  Widget content(ArtistsViewModel viewModel, BoxConstraints constraints)
  {
    if(!viewModel.isLoading && !viewModel.isError) {
      _refreshController.loadComplete();
      _refreshController.refreshCompleted();

      return
        SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            onRefresh: () =>  viewModel.pullRefresh(),
            onLoading: () =>  viewModel.loadMore(),
            controller: _refreshController,
            child: ListView.builder(
              itemCount: viewModel.data.length,
              itemBuilder: (context, index) {
                return  ArtistView(viewModel.data[index], () => viewModel.navArtistDetail(viewModel.data[index]));
              },
            )
        );
    }
    else if(viewModel.isError){
      return InfoNetworkView(() => viewModel.loadMore(viewModel.data.length));
    }

    return Center(
      child: CircularProgressIndicator(),
    );
  }
}