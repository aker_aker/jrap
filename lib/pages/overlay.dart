import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/pages/albums.dart';
import 'package:jrap_mobile/pages/artists.dart';
import 'package:jrap_mobile/pages/releases.dart';
import 'package:jrap_mobile/pages/singls.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/viewmodels/home.dart';

class OverlayNavigator extends StatefulWidget {

  OverlayNavigator({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OverlayNavigatorState();
}

class _OverlayNavigatorState extends State<OverlayNavigator> {


  @override
  Widget build(BuildContext context)
  {
    return StoreConnector<AppState, HomeViewModel>(
      converter: (store) =>  HomeViewModel.fromStore(store),
      builder: (_, viewModel) => content(viewModel),
    );
  }

  Widget content(HomeViewModel viewModel)
  {
    return Scaffold(
        key: Keys.scaffoldKeys[KeyType.over],
        body: Stack(children: <Widget>[
          _buildOffstageNavigator(0, viewModel.tabIndex, ReleasesPage()),
          _buildOffstageNavigator(1, viewModel.tabIndex, AlbumsPage()),
          _buildOffstageNavigator(2, viewModel.tabIndex, SinglsPage()),
          _buildOffstageNavigator(3, viewModel.tabIndex, ArtistsPage()),
        ])
    );
  }

  Widget _buildOffstageNavigator(int tabItem, int currentTab, Widget nav) {
    return Offstage(
        offstage: currentTab != tabItem,
        child: nav
    );
  }

  /*Widget _buildOffstageNavigator(int tabItem, int currentTab, Navigator nav) {
    return Offstage(
        offstage: currentTab != tabItem,
        child: nav
    );
  }*/
}