import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/download_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/viewmodels/download.dart';
import 'package:jrap_mobile/views/download.dart';
import 'package:jrap_mobile/views/info/info_nofound.dart';


class DownloadPage extends StatefulWidget {
  DownloadPage({Key key}) : super(key: key);

  @override
  _DownloadPageState createState() => _DownloadPageState();
}

class _DownloadPageState extends State<DownloadPage> {

  Future<String> _asyncInputDialog(BuildContext context) async {
    String playlistName = '';
    return showDialog<String>(
      context: context,
      barrierDismissible: false,
      // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Новый плейлист'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        hintText: 'название'),
                    onChanged: (value) {
                      playlistName = value;
                    },
                  ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Сохранить'),
              onPressed: () {
                Navigator.of(context).pop(playlistName);
              },
            ),
            FlatButton(
              child: Text('Отмена'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Скачанные треки'),
      ),
      body: LayoutBuilder(
          builder: (context, constraints) =>
              SafeArea(
                  child:  Material(
                      color: Theme
                          .of(context)
                          .primaryColor,
                      child: new StoreConnector<AppState, DownloadViewModel>(
                        onInit: (store) {
                          store.dispatch(new LoadDownloadAction());
                        },
                        converter: (store) =>
                            DownloadViewModel.fromStore(store),
                        builder: (_, viewModel) =>
                            content(viewModel, constraints),
                      )
                  )
              )
      ),
    );
  }

  Widget content(DownloadViewModel viewModel, BoxConstraints constraints) {
    if (viewModel.status == LoadingStatus.success) {
      if (viewModel.data.length == 0) return InfoNotFoundView();

      return ListView.builder(
        padding: const EdgeInsets.symmetric(vertical: 8.0),
        itemCount: viewModel.data.length,
        itemBuilder: (BuildContext context, int index) {
          return DownloadView(viewModel.data[index], viewModel.delete, () => viewModel.selectSong(viewModel.data[index]));


        },
      );
    }

    return const Center(
      child: CircularProgressIndicator(),
    );
  }
}