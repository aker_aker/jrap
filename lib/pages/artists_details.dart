import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/like_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/pages/artists_albums.dart';
import 'package:jrap_mobile/pages/artists_singls.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/pages/albums_detail.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/viewmodels/artist_detail.dart';
import 'package:jrap_mobile/views/header_background.dart';
import 'package:jrap_mobile/views/singls_mini_card.dart';
import 'package:jrap_mobile/views/title_section.dart';


const double HEIGHT_HEADER = 230 + kToolbarHeight;
const double HEIGHT_EXPAND = 180;


class ArtistDetailPage extends StatefulWidget {
  Artist artist;

  ArtistDetailPage({Key key, this.artist}) : super(key: key);

  @override
  _ArtistDetailPageState createState() => _ArtistDetailPageState();

}

class _ArtistDetailPageState extends State<ArtistDetailPage> {
  ScrollController _scrollContoller;

  @override
  void initState(){
    super.initState();
    _scrollContoller = new ScrollController();
    _scrollContoller.addListener((){
       setState((){});
    });
  }

  @override
  void dispose(){
    _scrollContoller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: StoreConnector<AppState, ArtistDetailViewModel>(
            onInitialBuild: (viewModel) => viewModel.firstLoad(),
            converter: (store) => ArtistDetailViewModel.fromStore(store, widget.artist),
            builder: (_, viewModel) => content(context, viewModel)
        )
    );
  }

  Widget content(BuildContext context, ArtistDetailViewModel viewModel) {

    return  _Body(
      widget_context: context,
      artist: viewModel.artist,
      albumsArtist: viewModel.albums ?? [],
      singlsArtist: viewModel.singls ?? [],
      scrollContoller: _scrollContoller,
      loading: viewModel.isLoading,
      selectSectionAlbums: viewModel.navAlbums,
      selectSectionSings: viewModel.navSingls,
    );
  }
}


class _Body extends StatelessWidget {
  final Artist artist;
  final List<Album> albumsArtist;
  final List<Album> singlsArtist;
  final ScrollController scrollContoller;
  final bool loading;
  final BuildContext widget_context;

  final Function selectSectionSings;
  final Function selectSectionAlbums;

  const _Body({Key key, this.widget_context, @required this.loading, @required this.artist, this.albumsArtist, this.singlsArtist, @required this.scrollContoller, this.selectSectionSings, this.selectSectionAlbums})
      : assert(artist != null),
        assert(scrollContoller != null),
        super(key: key);

  void _showModalSheet() {
    /*showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: PlaylistBottomSheet(_playlist, this._edit, this._delete)
          );
        });*/
  }

  Widget _buildAlbumCart(BuildContext context,Album album, double widthItem ){

    return Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.all(0.0),
            margin: EdgeInsets.all(0.0),
            decoration:  BoxDecoration(
                border: Border.all(width: 1.5, color: Colors.white)
            ),
            child:  FadeInImage.assetNetwork(
              placeholder: "assets/images/nophoto.png",
              image: album.ImageUrl,
              fit: BoxFit.cover,
              width: widthItem,
              height: widthItem,
              //image: new CachedNetworkImageProvider(photos[int].url),
              fadeInDuration: new Duration(milliseconds: 100),
              fadeInCurve: Curves.linear,
            )
        ),
        Expanded(
          child:Padding(
            padding: EdgeInsets.only(top:8.0),
            child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(album.AlbumName,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(fontSize: 14, color:Colors.white, fontWeight: FontWeight.bold)
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 4.0),
                    child: Text(album.AlbumYear.toString(), maxLines: 1, style: TextStyle(fontSize: 12, color: Colors.white70)),
                  )
                ],
              ),
          ),
        )
      ],
    );
  }

  Widget _buildListContetn(BuildContext context, MainObjType section, List<Album> albumsArtist, List<Album> singlsArtist)
  {
    var mediaQueryData = MediaQuery.of(context);
    final double widthScreen = mediaQueryData.size.width;
    final double appBarHeight = kToolbarHeight;
    final double paddingBottom = mediaQueryData.padding.bottom + 80.0;
    final double heightScreen = mediaQueryData.size.height - paddingBottom - appBarHeight;
    final int _crossAxisCount = 2;
    final double _crossAxisSpacing = 8.0;
    double widthItem = (widthScreen-32-_crossAxisSpacing)/_crossAxisCount;

    if(section==MainObjType.album){
      return Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: <Widget>[
            TitleSection(title: "Альбомы", pBottom: 0.0,),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0),
              child: GridView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: _crossAxisCount,
                    crossAxisSpacing: _crossAxisSpacing,
                    childAspectRatio: widthScreen / heightScreen),
                itemBuilder: (_, _index) => GestureDetector(
                  child: _buildAlbumCart(context,albumsArtist[_index],widthItem),
                  onTap: () =>  Navigator.push(
                    widget_context,
                    NoAnimationMaterialPageRoute(
                      builder: (context) => AlbumDetailPage(album: albumsArtist[_index]),
                    ),
                  ),
                ),
                itemCount: (albumsArtist.length > 4) ? 4 : albumsArtist.length,
                physics: ScrollPhysics(),
              ),
            ),
            (artist.Info.CountAlbum > 4) ?
            ListTile(
              contentPadding: EdgeInsets.only(left: 0),
              title: Padding(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Text("Все альбомы",
                style: Theme.of(context).textTheme.title.merge(
                    TextStyle(
                      fontSize: 16.0,
                      color: Theme.of(context).accentColor,
                      fontWeight: FontWeight.bold,
                    )
                ),
              )),
              onTap: () => selectSectionAlbums()
            ) : SizedBox.shrink()
          ],
        );
    }
    else
    if(section == MainObjType.singl){
      return Column(
        children: <Widget>[
          TitleSection(title: "Синглы"),
          ListView.builder
            (
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: (singlsArtist.length > 4) ? 5 : singlsArtist.length,
              physics: const NeverScrollableScrollPhysics() ,
              itemBuilder: (BuildContext ctxt, int index) {
                if(index > 3){
                  return  ListTile(
                    contentPadding: EdgeInsets.only(left: 0),
                    title: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text("Все синглы",
                      //style: TextStyle(color: Theme.of(context).accentColor, fontWeight: FontWeight.bold, fontSize:  24.0)
                      style: Theme.of(context).textTheme.title.merge(
                          TextStyle(
                            fontSize: 16.0,
                            color: Theme.of(context).accentColor,
                            fontWeight: FontWeight.bold,
                          )
                      ),
                    )
                    ),
                    onTap: () => selectSectionSings()
                  );
                }
                return  SinglsMiniCardView(singlsArtist[index], () => null);
              }
          ),
        ],
      );
    }
  }

  _buildLikeButton(){
    double top = HEIGHT_EXPAND + 120.0;
    if(scrollContoller.hasClients){
      top = ((top - scrollContoller.offset) > (kToolbarHeight+20)) ? top - scrollContoller.offset : (kToolbarHeight+20);
    }

    return Positioned(
      top: top,
      child:Container(
        child:Center(
          child: FlatButton(
            onPressed: (){},
            child:
            Padding(
              padding: EdgeInsets.only(right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.favorite,
                    color: Colors.white,
                    size:  16,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text("Нравится", style: TextStyle(color: Colors.white)),
                  )
                ],
              ),
            ),
              color: Color.fromRGBO(222,103,59,1),
              splashColor: Colors.red,
              shape: StadiumBorder(),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    List<MainObjType> section = new List<MainObjType>();
    if(artist.Info!=null) {
      if (artist.Info.CountAlbum > 0 && albumsArtist.length > 0)
        section.add(MainObjType.album);
      if (artist.Info.CountSingls > 0 && singlsArtist.length > 0)
        section.add(MainObjType.singl);
    }

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        ArtistDetailList(
          token: 'artist_${artist.ArtistId}',
          albumsArtist: albumsArtist,
          singlsArtist: singlsArtist,
          onMusicTap: ArtistDetailList.defaultOnTap,
          leadingBuilder: ArtistDetailList.indexedLeadingBuilder,
          trailingBuilder: ArtistDetailList.defaultTrailingBuilder,
          child: CustomScrollView(
            controller: scrollContoller,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                  expandedHeight: HEIGHT_EXPAND,
                  pinned: true,
                  automaticallyImplyLeading: false,
                  forceElevated: true,
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  flexibleSpace:  FlexibleDetailBar(
                      background: HeaderBackground(imageUrl: artist.ImageUrl),
                      content: Container(),
                      builder: (context, t) =>
                          AppBar(
                            title: _buildAppBarTitle(),
                            titleSpacing: 0,
                            elevation: 54.0,
                            backgroundColor: Colors.transparent,
                          )
                  ),
                  bottom: MusicListHeader(),
              ),
              SliverToBoxAdapter(
                child: Container(
                  margin: EdgeInsets.only(top:60),
                  child:Center(
                    child: DefaultTextStyle(
                      style: Theme.of(context).primaryTextTheme.body1,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(height: 8),
                          Text(artist.ArtistNickName, style: TextStyle(fontSize: 17), overflow: TextOverflow.clip),
                          SizedBox(height: 8),
                          //Text("${album.AlbumYear}") //：${getFormattedTime(album["publishTime"])}
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              /*SliverToBoxAdapter(
                child: ButtonTheme(
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
                  child: RaisedButton(
                    onPressed: () {},
                    child: Row(
                      children: <Widget>[
                        Icon(  Icons.favorite, color: Theme.of(context).accentColor, size: 16.0),
                        Text("НРАВИТСЯ", style: TextStyle(color: Colors.white),)
                      ],
                    ),
                  ),
                )
              ),*/
              SliverToBoxAdapter(
                child: SizedBox(height: 60),
              ),
              (!loading) ? SliverList(
                delegate: SliverChildBuilderDelegate( (BuildContext _contex, int index){
                  return _buildListContetn(_contex, section[index], albumsArtist, singlsArtist);
                },
                  childCount: section.length,
                ),
              )
              : SliverFillRemaining(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              ),
            ],
          ),
        ),
        _buildImage(context),
        _buildLikeButton(),
        //Проиграть п=артиста пока отключили фн не доделан
        //_buildPlay(),
      ],
    );
  }

  Widget _buildImage(BuildContext context){
    double top = HEIGHT_EXPAND - 60;
    if(scrollContoller.hasClients){
      top -= scrollContoller.offset;
    }

    return Positioned(
      top: top,
      child: Container(
          width: MediaQuery.of(context).size.width,
          child: Align(
            alignment: Alignment.center,
            child: Container(
              width: 140,
              height: 140,
              decoration: new BoxDecoration(
                // Circle shape
                shape: BoxShape.circle,
                color: Colors.white,
                // The border you want
                border:  Border.all(
                  width: 2.0,
                  color: Colors.white,
                ),
              ),
              child:  Hero(
                tag: "artist_image_${artist.ArtistId}",
                child: ClipOval(
                  child: new FadeInImage.assetNetwork(
                    placeholder: "assets/images/nophoto.png",
                    image: artist.IsImage ? artist.ImageUrl : "assets/images/nophoto.png",
                    fit: BoxFit.cover,
                    height: 140.0,
                    width: 140.0,
                    //image: new CachedNetworkImageProvider(photos[int].url),
                    alignment: Alignment.topLeft,
                    fadeInDuration: new Duration(milliseconds: 100),
                    fadeInCurve: Curves.linear,
                  ),
                ),
              ),
            ),
          )
      ),
    );
  }

  Widget _buildAppBarTitle(){
    double top = HEIGHT_EXPAND - 60.0;
    if(scrollContoller.hasClients){
      top -= scrollContoller.offset;
    }
    return Text(top < -120 ? artist.ArtistNickName : '');
  }

}


class MusicListHeader extends StatelessWidget implements PreferredSizeWidget {
  MusicListHeader();

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  Size get preferredSize => const Size.fromHeight(20);
}

class ArtistDetailList extends StatelessWidget {
  static ArtistDetailList of(BuildContext context) {
    final list = context.ancestorWidgetOfExactType(MusicList);
    assert(list != null, 'you can only use [MusicTile] inside MusicList scope');
    return list;
  }

  static final Widget Function(BuildContext context, Song music)
  defaultTrailingBuilder = (context, music) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        IconButton(
            icon: Icon(Icons.more_vert),
            tooltip: 'MV',
            onPressed: () {}
        ),
      ],
    );
  };

  static final Widget Function(BuildContext context, Song music)
  indexedLeadingBuilder = (context, music) {
    int index =  MusicList.of(context).musics.indexOf(music) + 1;
    return _buildPlayingLeading(context, music) ??
        Container(
          margin: const EdgeInsets.only(left: 8, right: 8),
          width: 40,
          height: 40,
          child: Center(
            child: Text(
              index.toString(),
              style: Theme.of(context).textTheme.body2,
            ),
          ),
        );
  };

  static final Widget Function(BuildContext context, Song music, String coverImageUrl)
  coverLeadingBuilder = (context, music,coverImageUrl) {
    return _buildPlayingLeading(context, music) ??
        Container(
          margin: const EdgeInsets.only(left: 8, right: 8),
          width: 40,
          height: 40,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(3),
            child: Image(
              image: NetworkImage(coverImageUrl),
            ),
          ),
        );
  };

  //return null if current music is not be playing
  static Widget _buildPlayingLeading(BuildContext context, Song music) {

    return null;
    /*if (MusicList.of(context).token == PlayerState.of(context).value.token &&
        music == PlayerState.of(context).value.current) {
      */
    return Container(
      margin: const EdgeInsets.only(left: 8, right: 8),
      width: 40,
      height: 40,
      child: Center(
        child: Icon(Icons.volume_up, color: Theme.of(context).primaryColor),
      ),
    );
    /*}
    return null;
    */
  }

  static final void Function(BuildContext context, Song muisc) defaultOnTap =
      (context, music) {
    final list = ArtistDetailList.of(context);
    /*if (quiet.value.token == list.token &&
        quiet.value.isPlaying &&
        quiet.value.current == music) {
      //open playing page
      Navigator.pushNamed(context, ROUTE_PAYING);
    } else {
      quiet.playWithList(music, list.musics, list.token);
    }*/
  };

  final String token;

  final List<Album> albumsArtist;
  final List<Album> singlsArtist;

  final void Function(BuildContext context, Song muisc) onMusicTap;

  final Widget Function(BuildContext context, Song music) leadingBuilder;

  final Widget Function(BuildContext context, Song music) trailingBuilder;

  final bool supportAlbumMenu;

  final void Function(Song music) remove;

  final Widget child;

  ArtistDetailList(
      {Key key,
        this.token,
        this.albumsArtist,
        this.singlsArtist,
        this.onMusicTap,
        this.child,
        this.leadingBuilder,
        this.trailingBuilder,
        this.supportAlbumMenu = true,
        this.remove})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
///the same as [FlexibleSpaceBar]
class FlexibleDetailBar extends StatelessWidget {
  final Widget content;

  final Widget background;

  ///[t] 0.0 -> Expanded  1.0 -> Collapsed to toolbar
  final Widget Function(BuildContext context, double t) builder;

  const FlexibleDetailBar({
    Key key,
    @required this.content,
    @required this.builder,
    @required this.background,
  })  : assert(content != null),
        assert(builder != null),
        assert(background != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final FlexibleSpaceBarSettings settings =
    context.inheritFromWidgetOfExactType(FlexibleSpaceBarSettings);

    final List<Widget> children = <Widget>[];

    final double deltaExtent = settings.maxExtent - settings.minExtent;
    // 0.0 -> Expanded
    // 1.0 -> Collapsed to toolbar
    final double t =
    (1.0 - (settings.currentExtent - settings.minExtent) / deltaExtent)
        .clamp(0.0, 1.0);

    //背景添加视差滚动效果
    children.add(Positioned(
      top: -Tween<double>(begin: 0.0, end: deltaExtent / 4.0).transform(t),
      left: 0,
      right: 0,
      height: settings.maxExtent,
      child: background,
    ));

    children.add(Positioned(
      top: settings.currentExtent - settings.maxExtent,
      left: 0,
      right: 0,
      height: settings.maxExtent,
      child: Opacity(
        opacity: 1 - t,
        child: content,
      ),
    ));

    children.add(Column(children: <Widget>[builder(context, t)]));

    return ClipRect(child: Stack(children: children, fit: StackFit.expand));
  }
}

