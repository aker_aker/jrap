import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/pop.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/pages/artists_details.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/album_detail.dart';
import 'package:jrap_mobile/views/header_background.dart';
import 'package:jrap_mobile/views/sheet/track_sheet.dart';
import 'package:jrap_mobile/views/track.dart';

const double HEIGHT_HEADER = 230 + kToolbarHeight;
const double HEIGHT_EXPAND = 180;

class AlbumDetailPage extends StatefulWidget {
  final Album album;

  AlbumDetailPage({Key key, this.album}) : super(key: key);

  @override
  _AlbumDetailPageState createState() => _AlbumDetailPageState();

}

class _AlbumDetailPageState extends State<AlbumDetailPage> {
  ScrollController _scrollContoller;

  @override
  void initState(){
    super.initState();
    _scrollContoller = new ScrollController();
    _scrollContoller.addListener(()=>setState((){}));
  }

  @override
  void dispose(){
    _scrollContoller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: StoreConnector<AppState, AlbumDetailViewModel>(
        onInitialBuild: (viewModel) => viewModel.firstLoad(),
        /*onInit: (store) {
            store.dispatch(LoadAlbumDetailAction(widget.album));
        },*/
        converter: (store) => AlbumDetailViewModel.fromStore(store, widget.album),
        builder: (context, viewModel) => content(context,viewModel),
      )
    );
  }

  Widget content(BuildContext context, AlbumDetailViewModel viewModel) {

    return  _Body(
      album: viewModel.album ?? widget.album,
      musicList: viewModel.tracks ?? [],
      scrollContoller: _scrollContoller,
      play: viewModel.play,
      getPlaylist: viewModel.getPlaylists,
      playlists: viewModel.playlist,
      loadingStatusPlaylsit: viewModel.status_playlist,
      loading: viewModel.isLoading,
      selectSong: viewModel.selectSong,
    );
  }
}


class _Body extends StatelessWidget {
  final Album album;
  final List<Song> musicList;
  final ScrollController scrollContoller;
  final Function(List<Song>, InfoQueue) play;
  final List<Playlist> playlists;
  final Function getPlaylist;
  final Function(Song,InfoQueue) selectSong;
  final bool loading;
  LoadingStatus loadingStatusPlaylsit;

  _Body({this.loadingStatusPlaylsit, Key key, @required this.loading, @required this.album, @required this.musicList, @required this.scrollContoller, @required this.play,@required  this.getPlaylist, this.playlists, this.selectSong})
      : assert(album != null),
        assert(musicList != null),
        assert(scrollContoller != null),
        assert(play != null),
        assert(getPlaylist != null),
        super(key: key);

  void menuAction(BuildContext context, String item) async{
    if(item == action_add_album_playlist){
      getPlaylist();
      int playlisId = await asyncEditDialog(context);
      if(playlisId == -1) {
        String playlistName = await asyncInputDialog(context);
        if(playlistName!= null && playlistName.isNotEmpty) {
          StoreProvider.of<AppState>(context).dispatch(
              AddPlaylistAction(playlistName));
        }
      }
    }
    else if(item == action_nav_artist){
      await Navigator.push(
        context,
        NoAnimationMaterialPageRoute(
            builder: (context) => ArtistDetailPage(artist: album.AlbumArtists.first,),
        ),
      );
    }
    else if(item == action_nav_download){
      StoreProvider.of<AppState>(context).dispatch(RequestDownloadSong(musicList.first, album));
    }
  }


  @override
  Widget build(BuildContext context) {

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
         MusicList(
            token: 'album_${album.AlbumId}',
            musics: musicList,
            onMusicTap: MusicList.defaultOnTap,
            leadingBuilder: MusicList.indexedLeadingBuilder,
            child: CustomScrollView(
                controller: scrollContoller,
                slivers: <Widget>[
                   SliverAppBar(
                    expandedHeight: HEIGHT_EXPAND,
                    pinned: true,
                    automaticallyImplyLeading: false,
                    forceElevated: true,
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    flexibleSpace:   FlexibleDetailBar(
                        background:  HeaderBackground(imageUrl: album.ImageUrl),
                        content: SizedBox.shrink(),
                        builder: (context, t) =>
                             AppBar(
                              title: _buildAppBarTitle(),
                              titleSpacing: 0,
                              elevation: 54.0,
                              backgroundColor: Colors.transparent,
                            )
                    ),
                    bottom: MusicListHeader(),
                    actions: <Widget>[
                       PopupMenuButton<String>(
                         onSelected: (String item) => menuAction(context, item),
                         itemBuilder: (BuildContext context){

                           if(album.AlbumType==1 && (musicList!=null && musicList.length > 0 && musicList.first.IsDownloading))
                            action_albums_detail.add(action_nav_download);

                           return action_albums_detail.map((String item){
                             return PopupMenuItem<String>(
                               value: item,
                               child: Text(item),
                             );
                           }).toList();
                         },
                       ),
                    ]
                  ),
                   SliverToBoxAdapter(
                      child: Container(
                        height: 90,
                        margin: EdgeInsets.only(top:20),
                        child:Center(
                          child: DefaultTextStyle(
                            style: Theme.of(context).primaryTextTheme.body1,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                const SizedBox(height: 8),
                                Text(album.AlbumName, style: TextStyle(fontSize: 17), overflow: TextOverflow.clip),
                                const SizedBox(height: 8),
                                InkWell(
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 4, bottom: 4),
                                      child: Text(
                                          "${album.AlbumArtists.map((a) => a.ArtistNickName).join(', ')}",
                                          overflow: TextOverflow.clip,
                                          style: TextStyle(color: Colors.white70),
                                      ),
                                    ),
                                    onTap: () {
                                      //launchArtistDetailPage(context, artist);
                                    }),
                                const SizedBox(height: 8),
                                //Text("${album.AlbumYear}") //：${getFormattedTime(album["publishTime"])}
                              ],
                            ),
                          ),
                        ),
                      ),
                  ),
                  SliverToBoxAdapter(
                    child: SizedBox(height: 40),
                  ),
                  (!loading) ? SliverList(
                    delegate: SliverChildBuilderDelegate(
                            (context, index) =>
                                TrackView(
                                    song:musicList[index],
                                    album: album,
                                    index: index+1,
                                    selectSong: (musicList[index].IsVkLoad) ? () => selectSong(musicList[index], InfoQueue(Id: album.AlbumId, playObject: album, playType: PlayType.album))
                                        :  null
                                ),
                        childCount: musicList.length)
                  ) : SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ],
              ),
        ),
        _buildAlbumImage(context),
        _buildPlay(),
      ],
    );
  }

  Widget _buildAlbumImage(BuildContext context){
    double top = HEIGHT_EXPAND - 100.0;
    if(scrollContoller.hasClients){
      top -= scrollContoller.offset;
    }

    return Positioned(
      top: top,
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.center,
          child: Container(
              width: 140.0,
              height: 140.0,
              decoration: new BoxDecoration(
                border: Border.all(width: 1.5, color: Colors.white),
              ),
              child: Hero(
                tag: "album_image_${album.AlbumId}",
                child: CachedNetworkImage(
                  imageUrl: album.ImageUrl,
                  fit: BoxFit.fill,
                  fadeInCurve: Curves.linear,
                  placeholder: (context, url) =>
                      Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                  errorWidget: (context, url, error) =>
                      Image.asset("assets/images/nophoto.png", fit: BoxFit.cover),
                ),
                /*child:  new FadeInImage.assetNetwork(
                  placeholder: "assets/images/nophoto.png",
                  image: album.ImageUrl,
                  fit: BoxFit.fill,
                  width: 140.0,
                  height: 140.0,
                  //image: new CachedNetworkImageProvider(photos[int].url),
                  alignment: Alignment.topLeft,
                  fadeInDuration: new Duration(milliseconds: 100),
                  fadeInCurve: Curves.linear,
                ),*/
              ),
          ),
        )
      ),
    );
  }

  Widget _buildAppBarTitle(){
    double top = HEIGHT_EXPAND - 100.0;
    if(scrollContoller.hasClients){
      top -= scrollContoller.offset;
    }
    return Text(top < -120 ? album.AlbumName : '');
  }

  Widget _buildPlay(){

    double top = HEIGHT_EXPAND + 110.0;
    if(scrollContoller.hasClients){
      top = ((top - scrollContoller.offset) > (kToolbarHeight+20)) ? top - scrollContoller.offset : (kToolbarHeight+20);
    }

    return Positioned(
        top: top,
        child:Container(
          child:Center(
            child: FlatButton(
              onPressed: (){
                if(album.IsPlaying && !loading){
                  this.play(this.musicList, InfoQueue(playType: PlayType.album, Id: album.AlbumId));
                }
              },
              child:
                  Padding(
                  padding: EdgeInsets.only(right: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        (album.IsPlaying && !loading) ? Icons.play_arrow : Icons.lock,
                        color: Colors.white,
                        size:  album.IsPlaying ? 17 : 17,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 10),
                        child: Text("Проиграть", style: TextStyle(color: Colors.white)),
                      )
                    ],
                  ),
              ),
              color: Color.fromRGBO(222,103,59,1),
              splashColor: Colors.red,
              shape: StadiumBorder(),
            ),
          ),
        ),
    );
  }
}


class MusicListHeader extends StatelessWidget implements PreferredSizeWidget {
  MusicListHeader();

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  Size get preferredSize => const Size.fromHeight(20);
}

class MusicTile extends StatelessWidget {
  final Song music;

  void showModalSheet() {

    showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: SongBottomSheet(item: music,)
          );
        }
    );
  }

  MusicTile(this.music, {Key key}) : super(key: key);

  Widget _buildPadding(BuildContext context, Song music) {
    return SizedBox(width: 8);
  }

  @override
  Widget build(BuildContext context) {
    final list = MusicList.of(context);
    return Container(
      height: 56,
      child: InkWell(
        onTap: () {
          if (list.onMusicTap != null) list.onMusicTap(context, music);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (list.leadingBuilder ?? _buildPadding)(context, music),
            Expanded(
              child: _SimpleMusicTile(music),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                    icon: Icon(Icons.more_vert),
                    onPressed: () => showModalSheet()
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class _SimpleMusicTile extends StatelessWidget {
  final Song music;

  const _SimpleMusicTile(this.music, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      child: Row(
        children: <Widget>[
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Spacer(),
                  Text(
                    music.SongName,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.body1,
                  ),
                  Padding(padding: EdgeInsets.only(top: 3)),
                  Text(
                    music.ArtistTitle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  Spacer(),
                ],
              )),
        ],
      ),
    );
  }
}

class MusicList extends StatelessWidget
{
  static MusicList of(BuildContext context) {
    final list = context.ancestorWidgetOfExactType(MusicList);
    assert(list != null, 'you can only use [MusicTile] inside MusicList scope');
    return list;
  }

  static final Widget Function(BuildContext context, Song music)
  indexedLeadingBuilder = (context, music) {
    int index =  MusicList.of(context).musics.indexOf(music) + 1;
    return _buildPlayingLeading(context, music) ??
        Container(
          margin: const EdgeInsets.only(left: 8, right: 8),
          width: 40,
          height: 40,
          child: Center(
            child: Text(
              index.toString(),
              style: Theme.of(context).textTheme.body2,
            ),
          ),
        );
  };

  static final Widget Function(BuildContext context, Song music, String coverImageUrl)
  coverLeadingBuilder = (context, music,coverImageUrl) {
    return _buildPlayingLeading(context, music) ??
        Container(
          margin: const EdgeInsets.only(left: 8, right: 8),
          width: 40,
          height: 40,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(3),
            child: Image(
              image: NetworkImage(coverImageUrl),
            ),
          ),
        );
  };

  //return null if current music is not be playing
  static Widget _buildPlayingLeading(BuildContext context, Song music) {

    return null;
    /*if (MusicList.of(context).token == PlayerState.of(context).value.token &&
        music == PlayerState.of(context).value.current) {
      */
      return Container(
        margin: const EdgeInsets.only(left: 8, right: 8),
        width: 40,
        height: 40,
        child: Center(
          child: Icon(Icons.volume_up, color: Theme.of(context).primaryColor),
        ),
      );
    /*}
    return null;
    */
  }

  static final void Function(BuildContext context, Song muisc) defaultOnTap =
      (context, music) {
    final list = MusicList.of(context);
    /*if (quiet.value.token == list.token &&
        quiet.value.isPlaying &&
        quiet.value.current == music) {
      //open playing page
      Navigator.pushNamed(context, ROUTE_PAYING);
    } else {
      quiet.playWithList(music, list.musics, list.token);
    }*/
  };

  final String token;

  final List<Song> musics;

  final void Function(BuildContext context, Song muisc) onMusicTap;

  final Widget Function(BuildContext context, Song music) leadingBuilder;

  final Widget Function(BuildContext context, Song music) trailingBuilder;

  final bool supportAlbumMenu;

  final void Function(Song music) remove;

  final Widget child;

  MusicList(
      {Key key,
        this.token,
        @required this.musics,
        this.onMusicTap,
        this.child,
        this.leadingBuilder,
        this.trailingBuilder,
        this.supportAlbumMenu = true,
        this.remove})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
///the same as [FlexibleSpaceBar]
class FlexibleDetailBar extends StatelessWidget {
  final Widget content;

  final Widget background;

  ///[t] 0.0 -> Expanded  1.0 -> Collapsed to toolbar
  final Widget Function(BuildContext context, double t) builder;

  const FlexibleDetailBar({
    Key key,
    @required this.content,
    @required this.builder,
    @required this.background,
  })  : assert(content != null),
        assert(builder != null),
        assert(background != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final FlexibleSpaceBarSettings settings =
    context.inheritFromWidgetOfExactType(FlexibleSpaceBarSettings);

    final List<Widget> children = <Widget>[];

    final double deltaExtent = settings.maxExtent - settings.minExtent;
    // 0.0 -> Expanded
    // 1.0 -> Collapsed to toolbar
    final double t =
    (1.0 - (settings.currentExtent - settings.minExtent) / deltaExtent)
        .clamp(0.0, 1.0);

    children.add(Positioned(
      top: -Tween<double>(begin: 0.0, end: deltaExtent / 4.0).transform(t),
      left: 0,
      right: 0,
      height: settings.maxExtent,
      child: background,
    ));

    children.add(Positioned(
      top: settings.currentExtent - settings.maxExtent,
      left: 0,
      right: 0,
      height: settings.maxExtent,
      child: Opacity(
        opacity: 1 - t,
        child: content,
      ),
    ));

    children.add(Column(children: <Widget>[builder(context, t)]));

    return ClipRect(child: Stack(children: children, fit: StackFit.expand));
  }
}

