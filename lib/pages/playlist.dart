import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/utils/pop.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/playlists.dart';
import 'package:jrap_mobile/views/playlist.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class PlaylistsPage extends StatefulWidget {
  PlaylistsPage({Key key}) : super(key: key);

  Function editDialog;

  @override
  _PlaylistsPageState createState() => _PlaylistsPageState();
}

class _PlaylistsPageState extends State<PlaylistsPage> {
  static RefreshController _refreshController =  RefreshController(initialRefresh: false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(playlists_title),
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.add,
              color: Colors.white,
              size: 30.0,
            ),
            tooltip: 'Добавить новый плэйлист',
            onPressed: () async {
              String playlistName = await asyncInputDialog(context);
              if (playlistName != null && playlistName.isNotEmpty)
                StoreProvider.of<AppState>(context)
                    .dispatch(AddPlaylistAction(playlistName));
            },
          )
        ],
      ),
      body: LayoutBuilder(
          builder: (context, constraints) => SafeArea(
              child: Container(
                  color: Theme.of(context).primaryColor,
                  child: StoreConnector<AppState, PlaylistViewModel>(
                    onInit: (store) {
                      store.dispatch(new LoadPlaylistsAction());
                    },
                    converter: (store) => PlaylistViewModel.fromStore(store),
                    builder: (_, viewModel) => content(viewModel, constraints),
                  )))),
    );
  }

  Widget content(PlaylistViewModel viewModel, BoxConstraints constraints) {
    if (viewModel.status == LoadingStatus.success) {
      widget.editDialog = (item) async {
        String playlistName = await asyncEditNamePlaylistDialog(context, item);
        if (playlistName.isNotEmpty) {
          item.Name = playlistName;
          viewModel.edit(item);
        }
      };

      return Container(
          child: SmartRefresher(
              enablePullDown: false ,
              enablePullUp: true,
              onRefresh: () =>  viewModel.pullRefresh(),
              controller: _refreshController,
              child: ListView.builder(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                itemCount: viewModel.data.length,
                itemBuilder: (context, index) {
                  return (viewModel.data.length > 0)
                      ? PlaylistView(
                      viewModel.data[index],
                      widget.editDialog,
                      viewModel.delete,
                          () => viewModel.navPlaylistDetail(viewModel.data[index]))
                      : SizedBox.shrink();
                },
              )
          )
      );
    } else
      return new Center(
        child: new CircularProgressIndicator(),
      );
  }
}
