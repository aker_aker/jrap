import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/models/drawerItem.dart';
import 'package:jrap_mobile/pages/download.dart';
import 'package:jrap_mobile/pages/login.dart';
import 'package:jrap_mobile/pages/playlist.dart';
import 'package:jrap_mobile/pages/search.dart';
import 'package:jrap_mobile/pages/settings.dart';
import 'package:jrap_mobile/pages/tasks.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/home.dart';
import 'package:jrap_mobile/views/bottom_bar.dart';
import 'package:jrap_mobile/views/playing.dart';
import 'package:jrap_mobile/views/playing_bar.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';
import 'package:flutter/services.dart';

class HomePage extends StatefulWidget {

  HomePage({Key key}) : super(key: key);

  final drawerItems = [
    new DrawerItem("Плейлисты", Icons.playlist_play),
    new DrawerItem("Скачанные", Icons.file_download),
    new DrawerItem("Очередь загрузок", Icons.redo),
    new DrawerItem("Поиск", Icons.search),
    new DrawerItem("Нстройки", Icons.settings)
  ];



  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin {
  double _panelHeightClosed = 60.0;
  final double _bottomHeight = 60.0;
  PanelController controller;
  StreamController streamController;

  @override
  void initState() {
    super.initState();
    controller = PanelController();
    streamController = new StreamController<double>();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Future<bool> _willPop(BuildContext context) async {
    return !await Keys.navKey[KeyType.over].currentState.maybePop();
  }

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, HomeViewModel>(
      onInit: (store) {
        //store.dispatch(new LoadAlbumsAction(0));
      },
      converter: (store) => HomeViewModel.fromStore(store),
      builder: (_, viewModel) => content(viewModel),
    );
  }

  Widget content(HomeViewModel viewModel) {
    _panelHeightClosed = viewModel.isPlayer ? 60.0 : 0.0;

    var drawerOptions = <Widget>[];
    for (var i = 0; i < widget.drawerItems.length; i++) {
      var d = widget.drawerItems[i];
      drawerOptions.add(
          new ListTile(
              leading:  Icon(d.icon),
              title:  Text(d.title),
              //selected: i == _selectedDrawerIndex,
              onTap: () {
                Navigator.of(context).pop();
                viewModel.setDrawer(i);
              }
          )
      );
    }

    return Scaffold(
        key: Keys.scaffoldKeys[KeyType.home],
        drawer: Drawer(
          child: Column(
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountName: Text(
                    "${viewModel.user.firstName ?? ""} ${viewModel.user
                        .lastName ?? ""}"),
                accountEmail: Text(viewModel.user.email),
                otherAccountsPictures: <Widget>[
                  ClipOval(
                    child: Material(
                      color: Colors.white30,
                      child: InkWell(
                          splashColor: Theme.of(context).accentColor,
                          child: SizedBox(width: 56,
                              height: 56,
                              child: const Icon(Icons.exit_to_app, color: Colors.white,)),
                          onTap: () => viewModel.logOut()
                      ),
                    ),
                  )
                ],
                currentAccountPicture: Container(
                  width: 80,
                  height: 80,
                  decoration: BoxDecoration(
                    // Circle shape
                    shape: BoxShape.circle,
                    color: Colors.white,
                    // The border you want
                    border: new Border.all(
                      width: 2.0,
                      color: Colors.white,
                    ),
                  ),
                  child: ClipOval(
                      child: FadeInImage.assetNetwork(
                        placeholder: "assets/images/nophoto.png",
                        image: (viewModel.user.imageUrl != "" &&
                            viewModel.user.imageUrl != null)
                            ? viewModel.user.imageUrl
                            : "assets/images/nophoto.png",
                        fit: BoxFit.fill,
                        alignment: Alignment.center,
                        fadeInDuration: Duration(milliseconds: 10),
                        fadeInCurve: Curves.linear,
                        width: 80,
                        height: 80,
                      )),
                ),
              ),
              Column(children: drawerOptions)
            ],
          ),
        ),
        body: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(
                    bottom: _bottomHeight + _panelHeightClosed),
                child: AppRoutes.createOverScreen(),
            ),
            _slider(_panelHeightClosed),
            BottomBar(
                stream: streamController.stream,
                tabClick: viewModel.setTab,
                bottomHeight: _bottomHeight)
          ],
        )
    );
  }

  Widget _slider(double minHeight) {
    final _w = MediaQuery
        .of(context)
        .size
        .height;
    final _pt = MediaQuery
        .of(context)
        .padding
        .top;
    return SlidingUpPanel(
      controller: controller,
      maxHeight: _w - _pt,
      minHeight: minHeight + _bottomHeight,
      parallaxEnabled: false,
      body: null,
      panel: PlayingPage(close: () => controller.close()),
      collapsed: PlayerBar(),
      backdropColor: Colors.transparent,
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(18.0), topRight: Radius.circular(18.0)
      ),
      onPanelSlide: (double pos) =>
          streamController.add(controller.getPanelPosition()),
    );
  }
}

