import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/pages/settings_about.dart';
import 'package:jrap_mobile/pages/settings_help.dart';
import 'package:jrap_mobile/pages/settings_mail.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/settings.dart';


class SettingsPage extends StatefulWidget {
  SettingsPage({Key key}) : super(key: key);

  @override
  _SettingsPageState createState() => _SettingsPageState();
}


class _SettingsPageState extends State<SettingsPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Настройки'),
        ),
        body: LayoutBuilder(
            builder: (context, constraints)=>SafeArea(
                child: new Container(
                    color: Theme.of(context).primaryColor,
                    child: new StoreConnector<AppState, SettingsViewModel>(
                      converter: (store) => SettingsViewModel.fromStore(store),
                      builder: (_, viewModel) => content(viewModel, context),
                    )
                )
            )
        )
    );
  }

  Future<SettingStore> asyncStoreDialog(BuildContext context, SettingStore current) async {
    return showDialog<SettingStore>(
      context: context,
      barrierDismissible: false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext ctx) {
        return AlertDialog(
          contentPadding: EdgeInsets.all(0),
          title: Text('Где сохранять музыку', style: TextStyle(color: Colors.white)),
          backgroundColor: Color(0xff354657),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

                    RadioListTile(
                        title: Text(
                            "В памяти устройства",
                            style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                        ),
                        activeColor: Theme.of(context).accentColor,
                        selected: (current == SettingStore.store),
                        value: SettingStore.store,
                        onChanged: (SettingStore value){
                          Navigator.of(ctx).pop(value);
                        }
                    ),

                    RadioListTile(
                        title: Text(
                            "На карте памяти",
                            style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                        ),
                        activeColor: Theme.of(context).accentColor,
                        selected: (current == SettingStore.sd),
                        value: SettingStore.sd,
                        onChanged: (SettingStore value){
                          Navigator.of(ctx).pop(value);
                        }
                    )

            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Отмена'),
              onPressed: () {
                Navigator.of(ctx).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Widget content(SettingsViewModel viewModel, BuildContext context)
  {
    return Container(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverPadding(
            padding: const EdgeInsets.only(top: 16, bottom: 0, left: 8, right: 8),
            sliver: SliverGrid(
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: MediaQuery.of(context).size.width / 2 ,
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                childAspectRatio: 1.3,
              ),
              delegate: SliverChildBuilderDelegate((BuildContext context, int index) {
                  return (index==0) ? Container(
                      padding: const EdgeInsets.all(0),
                      child: FlatButton(
                          onPressed: (){
                            viewModel.editSetting(viewModel.getByKey("mode"), "1");
                          },
                          padding: EdgeInsets.all(0.0),
                          child: Column(
                            children: <Widget>[
                              Container(
                                width: 80,
                                height: 80,
                                decoration: BoxDecoration(
                                  // Circle shape
                                  shape: BoxShape.circle,
                                  color: (viewModel.settingMode ==  1) ? Theme.of(context).accentColor : Colors.transparent ,
                                  // The border you want
                                  border: new Border.all(
                                    width: 2.0,
                                    color: Colors.white,
                                  ),
                                ),
                                child:Icon(Icons.settings_input_antenna, color: Colors.white,size: 26),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: 16),
                                child: Center (
                                    child: Text("Wifi и моб. интернет", style: TextStyle(color: Colors.white, fontSize: 16))
                                ),
                              )
                            ],
                          )
                      ),
                      color: Colors.transparent
                  ) :  Container(
                    padding: const EdgeInsets.all(0),
                    child:   FlatButton(
                        onPressed: (){
                          viewModel.editSetting(viewModel.getByKey("mode"), "0");
                        },
                        padding: EdgeInsets.all(0.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: 80,
                              height: 80,
                              decoration: BoxDecoration(
                                // Circle shape
                                shape: BoxShape.circle,
                                color: (viewModel.settingMode ==  0) ? Theme.of(context).accentColor : Colors.transparent ,
                                // The border you want
                                border: new Border.all(
                                  width: 2.0,
                                  color: Colors.white,
                                ),
                              ),
                              child:Icon(Icons.settings_power, color: Colors.white, size: 26),
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 16),
                              child: Center (
                                  child: Text("Офлайн-режим", style: TextStyle(color: Colors.white, fontSize: 16))
                              ),
                            )
                          ],
                        )

                    ),
                    color: Colors.transparent,
                  );
                },
                childCount: 2,
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                Container(
                    height: 400,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 16.0, right: 16.0),
                    child: ListView(
                      shrinkWrap: true,
                      scrollDirection:  Axis.vertical,
                      physics: const NeverScrollableScrollPhysics() ,
                      children: <Widget>[
                        //Добавлять в начало
                        Padding(
                          padding: EdgeInsets.only(top: 8, bottom: 8),
                          child: ListTile(
                            contentPadding: EdgeInsets.all(0.0),
                            title: Text(
                                setting_music_playlist_title,
                                style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                            ),
                            subtitle:Text(
                                setting_music_playlist_subtitle
                                , style:  TextStyle(color:Colors.white70)
                            ),
                            trailing: Switch(
                              value: viewModel.settingSort,
                              onChanged: (value) {
                                viewModel.editSetting(viewModel.getByKey("sort"), value ? "1" : "0");
                              },
                              activeTrackColor: Colors.lightGreenAccent,
                              activeColor: Colors.green,
                            ),
                          ),
                        ),
                        //Уведомления
                        Padding(
                          padding: EdgeInsets.only(top: 8, bottom: 8),
                          child:  ListTile(
                            contentPadding: EdgeInsets.all(0.0),
                            title: Text(
                                setting_push_title,
                                style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                            ),
                            subtitle:Text(
                                setting_push_subtitle
                                , style:  TextStyle(color:Colors.white70)
                            ),
                            trailing: Switch(
                              value: viewModel.settingPush,
                              onChanged: (value) {
                                viewModel.editSetting(viewModel.getByKey("push"), value ? "1" : "0");
                              },
                              activeTrackColor: Colors.lightGreenAccent,
                              activeColor: Colors.green,
                            ),
                          ),
                        ),
                        //Сохранять музыку
                        Padding(
                          padding: EdgeInsets.only(top: 8, bottom: 8),
                          child:  InkWell(
                          child: ListTile(
                              contentPadding: EdgeInsets.all(0.0),
                              title: Text(
                                  setting_music_store_title,
                                  style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                              ),
                              subtitle: Text(
                                  setting_push_subtitle
                                  , style:  TextStyle(color:Colors.white70)
                              ),
                            ),
                            onTap: () async{
                                var _val = await asyncStoreDialog(context, viewModel.settingStore);
                                if(_val != null)
                                  viewModel.editSetting(viewModel.getByKey("store"), _val);
                            },
                          ),
                        ),
                        //Поделится приложением
                        //Помощь
                        InkWell(
                          child: ListTile(
                            contentPadding: EdgeInsets.all(0.0),
                            title: Text(
                                setting_help_title,
                                style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                            ),
                          ),
                          onTap: (){
                            Navigator.push(
                              context,
                              NoAnimationMaterialPageRoute(
                                builder: (context) => SettingsHelpPage(),
                              ),
                            );
                          },
                        ),
                        //Написать разработчика
                        InkWell(
                          child: ListTile(
                            contentPadding: EdgeInsets.all(0.0),
                            title: Text(
                                setting_message_dev_title,
                                style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                            ),
                          ),
                          onTap: (){
                            //показать
                            Navigator.push(
                              context,
                              NoAnimationMaterialPageRoute(
                                builder: (context) => SettingsMailPage(),
                              ),
                            );
                          },
                        ),
                        //О приложение
                        InkWell(
                          child: ListTile(
                            contentPadding: EdgeInsets.all(0.0),
                            title: Text(
                                setting_about_title,
                                style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                            ),
                          ),
                          onTap: (){
                            //показать
                            Navigator.push(
                              context,
                              NoAnimationMaterialPageRoute(
                                builder: (context) => SettingsAboutPage(),
                              ),
                            );
                          },
                        ),
                      ],
                    )
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}