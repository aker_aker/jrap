import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/download_action.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/viewmodels/download.dart';
import 'package:jrap_mobile/views/download.dart';


class AddTracksPage extends StatefulWidget {

  final Playlist playlist;

  AddTracksPage({Key key, @required this.playlist}) : super(key: key);

  @override
  _AddTracksPageState createState() => _AddTracksPageState();
}


class _AddTracksPageState extends State<AddTracksPage> {

  Future<String> _asyncInputDialog(BuildContext context) async {
    String playlistName = '';
    return showDialog<String>(
      context: context,
      barrierDismissible: false, // dialog is dismissible with a tap on the barrier
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Новый плейлист'),
          content: new Row(
            children: <Widget>[
              new Expanded(
                  child: new TextField(
                    autofocus: true,
                    decoration: new InputDecoration(
                        hintText: 'название'),
                    onChanged: (value) {
                      playlistName = value;
                    },
                  ))
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Сохранить'),
              onPressed: () {
                Navigator.of(context).pop(playlistName);
              },
            ),
            FlatButton(
              child: Text('Отмена'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Добавить трек'),
      ),
      body: LayoutBuilder(
          builder: (context, constraints) =>
              SafeArea(
                  child: new Container(
                      color: Theme
                          .of(context)
                          .primaryColor,
                      child: new StoreConnector<AppState, DownloadViewModel>(
                        onInit: (store) {
                          store.dispatch(new LoadDownloadAction());
                        },
                        converter: (store) =>  DownloadViewModel.fromStore(store),
                        builder: (_, viewModel) => content(viewModel, constraints),
                      )
                  )
              )
      ),
    );
  }

  Widget content(DownloadViewModel viewModel, BoxConstraints constraints) {

    if (viewModel.status == LoadingStatus.success) {

      return   Container(
          padding: EdgeInsets.only(bottom: 60.0),
          child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 8.0),
            itemCount: viewModel.data.length,
            itemBuilder: (BuildContext context, int index) {
              return (viewModel.data.length > 0)
                  ? InkWell(
                child: Container(
                    padding: new EdgeInsets.symmetric(vertical: 8.0, horizontal:  16.0),
                    child: DownloadView(viewModel.data[index], viewModel.delete, () => viewModel.selectSong(viewModel.data[index]))
                ),
                onTap: (){
                  /*Navigator.push(
                    context,
                    NoAnimationMaterialPageRoute(
                      builder: (context) => PlaylistDetailPage(playlist: viewModel.data[index]),
                    ),
                  );*/
                },
              )
                  : Container();
            },
          )
      );    }
    else
      return new Center(
        child: new CircularProgressIndicator(),
      );
  }
}