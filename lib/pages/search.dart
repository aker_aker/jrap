import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/search_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/viewmodels/search.dart';
import 'package:jrap_mobile/views/info/info_nofound.dart';
import 'package:jrap_mobile/views/search_panel.dart';


class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}


class _SearchPageState extends State<SearchPage> {

  TextEditingController editingController = TextEditingController();


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Поиск'),
        ),
      body: LayoutBuilder(
          builder: (context, constraints) =>  SafeArea(
              child:  Container(
                  color: Theme.of(context).primaryColor,
                  child: StoreConnector<AppState, SearchViewModel>(
                    onInit: (store) {
                      //выбрать ичсторию поиска

                      //Проставить LoadStatus.none
                      store.dispatch(FirstLoadSearchPage());
                    },
                    converter: (store) => SearchViewModel.fromStore(store),
                    builder: (_, viewModel) => content(viewModel, context),
                  )
              )
          )
      )
    );
  }

  Widget content(SearchViewModel viewModel, BuildContext context)
  {
    Widget _widget;
    if(!viewModel.isLoading)
    {
      if(!viewModel.notFound) {
        _widget = ListView.builder(
          shrinkWrap: true,
          itemCount: viewModel.data?.items?.length ?? 0,
          itemBuilder: (context, index) {
            if (viewModel.data.items.length > 0) {
              if (viewModel.data.items[index].cntAll > 0) {
                return SearchPanelView(viewModel.data.items[index]);
              }
            }
            return Container();
          },
        );
      }
      else if(viewModel.isFirstLoad && viewModel.text.isEmpty)
        _widget = Container();
      else if(viewModel.isFirstLoad && viewModel.text.isNotEmpty){
        _widget = ListView.builder(
            shrinkWrap: true,
            itemCount: 1,
            itemBuilder: (context, index) =>
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                  child: InfoNotFoundView(),
                )
        );
      }
      else
          _widget = ListView.builder(
            shrinkWrap: true,
            itemCount: 1,
            itemBuilder: (context, index) =>
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 16.0),
                 child: InfoNotFoundView(),
                )
          );
    }
    else
      _widget =  Center(child:  CircularProgressIndicator());

    if(viewModel.isFirstLoad && viewModel.text.isNotEmpty &&  editingController.text != viewModel.text)
      editingController..text = viewModel.text;

    return Center(
      child: Column(
        /*crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,*/
        children: <Widget>[
        Container(
          color: Colors.transparent,
            child: Padding(
              padding: EdgeInsets.only(left: 8.0,right: 8.0, bottom: 8.0),
              child: Card(
                child: ListTile(
                  leading:  Icon(Icons.search),
                  title: TextFormField(
                    controller: editingController,
                    //initialValue: viewModel.text ?? "",
                    decoration: InputDecoration(hintText: 'Поиск', border: InputBorder.none),
                    onChanged: (value) {
                      viewModel.search(value);
                    },
                  ),
                  trailing: editingController.text.length > 0 ? IconButton(
                    icon:  Icon(Icons.close),
                    onPressed: () {
                      editingController.clear();
                      viewModel.clear();
                    },
                  ) : null ,
                ),
              ),
            ),
          ),
          Expanded(
            child: _widget,
          )
        ],
      ),
    );

  }
}