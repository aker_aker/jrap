import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/auth_actions.dart';
import 'package:jrap_mobile/actions/db_actions.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/screen_status.dart';
import 'package:jrap_mobile/pages/home.dart';
import 'package:jrap_mobile/utils/colors.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/login.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage>  with SingleTickerProviderStateMixin {

  AnimationController _controllerAnim;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    _controllerAnim = new AnimationController(vsync: this, duration: const Duration(milliseconds: 1000));
    _animation = new Tween(begin: 200.0, end: 0.0).animate(
      new CurvedAnimation(
        parent: _controllerAnim,
        curve:Curves.ease,
      ),
    );

  }

  Widget build(BuildContext context) =>  Scaffold(
    body: LayoutBuilder(
      builder: (context, constraints)=>SafeArea(
        child: new Container(
          color: Theme.of(context).primaryColor,
          child: new StoreConnector<AppState,LoginViewModel>(
            onInit: (store){
              store.dispatch(ClearErrorsAction());
              store.dispatch(GetUserAction(
                  hasUserCallback:(){
                    store.dispatch(AuthCompleetAction(true));
                    store.dispatch(new NavigateReplaceAction(AppRoutes.home));
                    //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> HomePage()));
                  },
                  noUserCallback:(){
                    _animation.addStatusListener((status){
                      if(status == AnimationStatus.dismissed || status == AnimationStatus.completed){
                        store.dispatch(ChangeScreenStateAction(ScreenState.SINGIN));
                      }
                    });
                    _animation.addListener((){
                      setState((){});
                    });
                    _controllerAnim.forward();
                  }
              ));
            },
            converter: (store) => LoginViewModel.fromStore(store),
            builder: (_, viewModel) => content(viewModel,context),
          ),
        ),
      ),
    ),
  );

  Widget content(LoginViewModel viewModel, BuildContext context) =>
      (viewModel.status != LoadingStatus.loading || viewModel?.type == ScreenState.WELCOME)? new Container(
        child: new Stack(
          fit: StackFit.expand,
          alignment: Alignment.topCenter,
          children: <Widget>[
            new Align(
              alignment: Alignment.topCenter,
              child: new Transform(
                transform: new Matrix4.translationValues(0.0, _animation.value, 0.0),
                child: SingleChildScrollView(
                  child: new Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 65.0,right: 65.0,top: 16.0),
                        child: new Image(
                          image: AssetImage("assets/images/logo_auth.png"),
                          width: 50,
                          height: 50,
                        ),
                      ),

                      viewModel.type == ScreenState.SINGIN ? new Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 65.0,right: 65.0,top: 16.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                loginBtn((){
                                  viewModel.login(context);
                                }),
                                offlineBtn((){
                                  viewModel.login(context);
                                })
                              ],
                            ),
                          ),
                          //termsAndConditions(),
                          new Padding(padding: const EdgeInsets.all(16.0))
                        ],
                      ):Container()
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ):new Center(
        child:  CircularProgressIndicator(),
  );

  Widget actionButton(VoidCallback callback, int color, String label) =>
      GestureDetector(
        child: Container(
          width: 100.0,
          height: 44.0,
          color: Color(color),
          alignment: Alignment.center,
          child: Text(
            label,
            style: TextStyle(
                fontSize: 16.0,
                color: Colors.white
            ),
          ),
        ),
        onTap: callback,
      );

  Widget loginBtn(VoidCallback callback, {String label}) => actionButton(callback, primaryBlue, label ?? label_login);

  Widget offlineBtn(VoidCallback callback, {String label}) => actionButton(callback, primaryBlue, label ?? label_offline);

  Future delay(Function f) async{
    await new Future.delayed(new Duration(milliseconds: 4000), f());
  }

  int _getColor(LoadingStatus status, String error, FocusNode node){
    if(!node.hasFocus) {
      return default_text_gray;
    }else{
      return primaryBlue;
    }
  }

  /*Widget build(BuildContext context) {
    final container = AppStateContainer.of(context);
    var width = MediaQuery.of(context).size.width;

    return new Container(
      width: width,
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          new RaisedButton(
            onPressed: () => container.login(),
            color: Colors.white,
            child: new Container(
              width: 230.0,
              height: 50.0,
              alignment: Alignment.center,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  new Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: new Image.network(
                      'http://diylogodesigns.com/blog/wp-content/uploads/2016/04/google-logo-icon-PNG-Transparent-Background.png',
                      width: 30.0,
                    ),
                  ),
                  new Text(
                    'Sign in With Google',
                    textAlign: TextAlign.center,
                    style: new TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }*/
}