import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/keys.dart';

import 'package:jrap_mobile/utils/pop.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/playlist_detail.dart';
import 'package:jrap_mobile/views/header_background.dart';
import 'package:jrap_mobile/views/info/info_playlist_not_track.dart';
import 'package:jrap_mobile/views/track_playlist.dart';


const double HEIGHT_HEADER = 230 + kToolbarHeight;
const double HEIGHT_EXPAND = 180;

class PlaylistDetailPage extends StatefulWidget {
  final Playlist playlist;

  PlaylistDetailPage({Key key, @required this.playlist}) : super(key: key);

  @override
  _PlaylistDetailPageState createState() => _PlaylistDetailPageState();

}

class _PlaylistDetailPageState extends State<PlaylistDetailPage> {
  ScrollController _scrollContoller;

  @override
  void initState(){
    super.initState();
    _scrollContoller = new ScrollController();
    _scrollContoller.addListener(()=>setState((){}));
  }

  @override
  void dispose(){
    _scrollContoller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: new StoreConnector<AppState, PlaylistDetailViewModel>(
        onInit: (store) {
          //нуно запихнуть албум в стате
        },
        onInitialBuild: (viewModel) => viewModel.firstLoad(),
        converter: (store) => PlaylistDetailViewModel.fromStore(store, widget.playlist),
        builder: (_, viewModel) => content(context, viewModel)
      )
    );
  }

  Widget content(BuildContext context, PlaylistDetailViewModel viewModel) {
    return _Body(
        playlist: viewModel.playlist,
        musicList: viewModel.tracks ?? [],
        scrollContoller: _scrollContoller,
        play: viewModel.play,
        loading: viewModel.isLoading,
        selectSong: viewModel.selectSong,
      );
  }
}


class _Body extends StatelessWidget {
  final Playlist playlist;
  final List<Song> musicList;
  final ScrollController scrollContoller;
  final Function(List<Song>, InfoQueue) play;
  final bool loading;
  final Function(Song,InfoQueue) selectSong;
  //final Function(Playlist) editPlaylist;
  //final Function(Playlist) deletePlaylist;

  const _Body({Key key, @required this.loading, @required this.playlist, @required this.musicList, @required this.scrollContoller, @required this.play, this.selectSong})
      : assert(playlist != null),
        assert(musicList != null),
        assert(scrollContoller != null),
        assert(play != null),
        super(key: key);

  void _showModalSheet() {
    /*showModalBottomSheet(
        context: Keys.scaffoldKeys[KeyType.home].currentContext,
        builder: (BuildContext context)  {
          return Container(
              color: Theme.of(context).primaryColor,
              child: PlaylistBottomSheet(_playlist, this._edit, this._delete)
          );
        });*/
  }

  void menuAction(BuildContext context, String item) async{
    if(item == action_add_song_by_playlist){

    }
    else if(item == action_add_by_playlist){

    }
    else if(item == action_edit_playlist){
      String playlistName = await asyncEditNamePlaylistDialog(context, playlist);
      if(playlistName.isNotEmpty) {
        playlist.Name = playlistName;
        //viewModel.edit(item);
      }
    }
    else if(item == action_remove_playlist){

    }
  }

  void _selectSong(Song song){
    var _state = StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).state.playerState;

    if(
    (!_state.queue.isQueue) ||
        (_state.info.playType != PlayType.playlist) ||
        (_state.info.playType == PlayType.playlist && _state.info.Id != playlist.Id)
    )
    {
      StoreProvider.of<AppState>(Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
          AddQueueAndClearBySelectSong(
              musicList.where((item)=>item.IsVkLoad).toList(),
              song,
              InfoQueue(playType: PlayType.playlist, Id: playlist.Id)
          )
      );
    }
    else {
      if (song.IsVkLoad) {
        StoreProvider.of<AppState>(
            Keys.scaffoldKeys[KeyType.home].currentContext).dispatch(
            ChangeSong(song, false));
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
         MusicList(
            token: 'pl_${playlist.Id}',
            musics: musicList,
            onMusicTap: MusicList.defaultOnTap,
            leadingBuilder: MusicList.indexedLeadingBuilder,
            trailingBuilder: MusicList.defaultTrailingBuilder,
            child: CustomScrollView(
                controller: scrollContoller,
                slivers: <Widget>[
                  SliverAppBar(
                    expandedHeight: HEIGHT_EXPAND,
                    pinned: true,
                    automaticallyImplyLeading: false,
                    forceElevated: true,
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    flexibleSpace:  FlexibleDetailBar(
                        background: HeaderBackground(imageUrl: null),
                        content: Container(),
                        builder: (context, t) =>
                            AppBar(
                              title: _buildAppBarTitle(),
                              titleSpacing: 0,
                              elevation: 54.0,
                              backgroundColor: Colors.transparent,
                            )
                    ),
                    bottom: MusicListHeader(),
                    actions: <Widget>[
                       PopupMenuButton<String>(
                         onSelected: (String item) => menuAction(context, item),
                         itemBuilder: (BuildContext context){
                           return action_playlist_detail.map((String item){
                             return PopupMenuItem<String>(
                               value: item,
                               child: Text(item),
                             );
                           }).toList();
                         },
                       ),
                    ]
                  ),
                  SliverToBoxAdapter(
                    child: Container(
                      height: 90,
                      margin: EdgeInsets.only(top:0, left: 20, right: 20, bottom: 20),
                      child: Center(
                        child: DefaultTextStyle(
                          style: Theme.of(context).primaryTextTheme.body1,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(height: 8),
                              Expanded(
                                child:  Center(
                                  child: Text(
                                      playlist.Name,
                                      textAlign: TextAlign.center,
                                      maxLines: 2,
                                      style: TextStyle(fontSize: 17), overflow: TextOverflow.clip),
                                )
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  SliverToBoxAdapter(
                    child: SizedBox(height: 20),
                  ),
                  (!loading) ?  SliverList(
                    delegate: SliverChildBuilderDelegate(
                            (context, index) => TrackPlaylistView(
                              song: musicList[index],
                              selectSong: (musicList[index].IsVkLoad) ? () => selectSong(musicList[index], InfoQueue(Id: playlist.Id, playObject: playlist, playType: PlayType.playlist))
                                :  null ),
                        childCount: musicList.length)
                  ) : SliverFillRemaining(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),
                ],
              ),
        ),
        _buildAlbumImage(context),
        (!loading) ? (musicList.length > 0 ) ? _buildPlay() : _infoView(null) : Container()
      ],
    );
  }

  Widget _buildAlbumImage(BuildContext context){
    double top = HEIGHT_EXPAND - 100.0;
    if(scrollContoller.hasClients){
      top -= scrollContoller.offset;
    }

    return Positioned(
      top: top,
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.center,
          child: Container(
              decoration: new BoxDecoration(
                border: Border.all(width: 1.5, color: Colors.white),
              ),
              child: Hero(
                tag: "pl_image_${playlist.Id}",
                child:  new FadeInImage.assetNetwork(
                  placeholder: "assets/images/nophoto.png",
                  image: "assets/images/nophoto.png",
                  fit: BoxFit.fill,
                  width: 140.0,
                  height: 140.0,
                  //image: new CachedNetworkImageProvider(photos[int].url),
                  alignment: Alignment.topLeft,
                  fadeInDuration: new Duration(milliseconds: 100),
                  fadeInCurve: Curves.linear,
                ),
              ),
          ),
        )
      ),
    );
  }

  Widget _buildAppBarTitle(){
    double top = HEIGHT_EXPAND - 100.0;
    if(scrollContoller.hasClients){
      top -= scrollContoller.offset;
    }
    return Text(top < -120 ? playlist.Name : '');
  }

  Widget _buildPlay(){

    double top = HEIGHT_EXPAND + 100.0;

    if(scrollContoller.hasClients){
      top = ((top - scrollContoller.offset) > (kToolbarHeight+20)) ? top - scrollContoller.offset : (kToolbarHeight+20);
    }

    return Positioned(
        top: top,
        child:Container(
          child:Center(
            child: FlatButton(
              onPressed: (){
                //if(playlist.IsPlaying)
                if(playlist.IsPlaying && !loading)
                  this.play(this.musicList, InfoQueue(playType: PlayType.playlist, Id: playlist.Id));
              },
              child:
              Padding(
                padding: EdgeInsets.only(right: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(
                      (!loading) ? Icons.play_arrow : Icons.lock,
                      color: Colors.white,
                      size: 17,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 10),
                      child: Text("Проиграть", style: TextStyle(color: Colors.white)),
                    )
                  ],
                ),
              ),
              color: Color.fromRGBO(222,103,59,1),
              splashColor: Colors.red,
              shape: StadiumBorder(),
            ),
          ),
        ),
    );
  }

  Widget _infoView(Function addTrackByPlaylist){

    double top = HEIGHT_EXPAND + 110.0;

    if(scrollContoller.hasClients){
      top = ((top - scrollContoller.offset) > (kToolbarHeight+20)) ? top - scrollContoller.offset : (kToolbarHeight+20);
    }

    return Positioned(
      top: top,
      child:Container(
        child:Center(
          child: InfoPlaylistNotTrackView(playlist),
        ),
      ),
    );
  }
}


class MusicListHeader extends StatelessWidget implements PreferredSizeWidget {
  MusicListHeader();

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  @override
  Size get preferredSize => const Size.fromHeight(20);
}

class MusicTile extends StatelessWidget {
  final Song music;

  MusicTile(this.music, {Key key}) : super(key: key);

  Widget _buildPadding(BuildContext context, Song music) {
    return SizedBox(width: 8);
  }

  @override
  Widget build(BuildContext context) {
    final list = MusicList.of(context);
    return Container(
      height: 56,
      child: InkWell(
        onTap: () {
          if (list.onMusicTap != null) list.onMusicTap(context, music);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            (list.leadingBuilder ?? _buildPadding)(context, music),
            Expanded(
              child: _SimpleMusicTile(music),
            ),
            (list.trailingBuilder ?? _buildPadding)(context, music),
          ],
        ),
      ),
    );
  }
}

class _SimpleMusicTile extends StatelessWidget {
  final Song music;

  const _SimpleMusicTile(this.music, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      child: Row(
        children: <Widget>[
          Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Spacer(),
                  Text(
                    music.SongName,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.body1,
                  ),
                  Padding(padding: EdgeInsets.only(top: 3)),
                  Text(
                    music.ArtistTitle,
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  Spacer(),
                ],
              )),
        ],
      ),
    );
  }
}

class MusicList extends StatelessWidget {
  static MusicList of(BuildContext context) {
    final list = context.ancestorWidgetOfExactType(MusicList);
    assert(list != null, 'you can only use [MusicTile] inside MusicList scope');
    return list;
  }

  static final Widget Function(BuildContext context, Song music)
  defaultTrailingBuilder = (context, music) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        IconButton(
          icon: Icon(Icons.more_vert),
          tooltip: 'MV',
          onPressed: () {}
        ),
      ],
    );
  };

  static final Widget Function(BuildContext context, Song music)
  indexedLeadingBuilder = (context, music) {
    int index =  MusicList.of(context).musics.indexOf(music) + 1;
    return _buildPlayingLeading(context, music) ??
        Container(
          margin: const EdgeInsets.only(left: 8, right: 8),
          width: 40,
          height: 40,
          child: Center(
            child: Text(
              index.toString(),
              style: Theme.of(context).textTheme.body2,
            ),
          ),
        );
  };

  static final Widget Function(BuildContext context, Song music, String coverImageUrl)
  coverLeadingBuilder = (context, music,coverImageUrl) {
    return _buildPlayingLeading(context, music) ??
        Container(
          margin: const EdgeInsets.only(left: 8, right: 8),
          width: 40,
          height: 40,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(3),
            child: Image(
              image: NetworkImage(coverImageUrl),
            ),
          ),
        );
  };

  //return null if current music is not be playing
  static Widget _buildPlayingLeading(BuildContext context, Song music) {

    return null;
    /*if (MusicList.of(context).token == PlayerState.of(context).value.token &&
        music == PlayerState.of(context).value.current) {
      */
      return Container(
        margin: const EdgeInsets.only(left: 8, right: 8),
        width: 40,
        height: 40,
        child: Center(
          child: Icon(Icons.volume_up, color: Theme.of(context).primaryColor),
        ),
      );
    /*}
    return null;
    */
  }

  static final void Function(BuildContext context, Song muisc) defaultOnTap =
      (context, music) {
    final list = MusicList.of(context);
    /*if (quiet.value.token == list.token &&
        quiet.value.isPlaying &&
        quiet.value.current == music) {
      //open playing page
      Navigator.pushNamed(context, ROUTE_PAYING);
    } else {
      quiet.playWithList(music, list.musics, list.token);
    }*/
  };

  final String token;

  final List<Song> musics;

  final void Function(BuildContext context, Song muisc) onMusicTap;

  final Widget Function(BuildContext context, Song music) leadingBuilder;

  final Widget Function(BuildContext context, Song music) trailingBuilder;

  final bool supportAlbumMenu;

  final void Function(Song music) remove;

  final Widget child;

  MusicList(
      {Key key,
        this.token,
        @required this.musics,
        this.onMusicTap,
        this.child,
        this.leadingBuilder,
        this.trailingBuilder,
        this.supportAlbumMenu = true,
        this.remove})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return child;
  }
}
///the same as [FlexibleSpaceBar]
class FlexibleDetailBar extends StatelessWidget {
  final Widget content;

  final Widget background;

  ///[t] 0.0 -> Expanded  1.0 -> Collapsed to toolbar
  final Widget Function(BuildContext context, double t) builder;

  const FlexibleDetailBar({
    Key key,
    @required this.content,
    @required this.builder,
    @required this.background,
  })  : assert(content != null),
        assert(builder != null),
        assert(background != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final FlexibleSpaceBarSettings settings =
    context.inheritFromWidgetOfExactType(FlexibleSpaceBarSettings);

    final List<Widget> children = <Widget>[];

    final double deltaExtent = settings.maxExtent - settings.minExtent;
    // 0.0 -> Expanded
    // 1.0 -> Collapsed to toolbar
    final double t =
    (1.0 - (settings.currentExtent - settings.minExtent) / deltaExtent)
        .clamp(0.0, 1.0);

    //背景添加视差滚动效果
    children.add(Positioned(
      top: -Tween<double>(begin: 0.0, end: deltaExtent / 4.0).transform(t),
      left: 0,
      right: 0,
      height: settings.maxExtent,
      child: background,
    ));

    children.add(Positioned(
      top: settings.currentExtent - settings.maxExtent,
      left: 0,
      right: 0,
      height: settings.maxExtent,
      child: Opacity(
        opacity: 1 - t,
        child: content,
      ),
    ));

    children.add(Column(children: <Widget>[builder(context, t)]));

    return ClipRect(child: Stack(children: children, fit: StackFit.expand));
  }
}
