import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/singls_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/utils/strings.dart';
import 'package:jrap_mobile/viewmodels/singls.dart';
import 'package:jrap_mobile/views/albums.dart';
import 'package:jrap_mobile/views/info/info_network.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';


class SinglsPage extends StatelessWidget {
  static RefreshController _refreshController = RefreshController(
      initialRefresh: false);

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
        key: Keys.scaffoldKeys[KeyType.singls] ,
        appBar:  AppBar(
          title: Text(singls_title),
          leading: IconButton(  icon: const Icon(Icons.menu),
              tooltip: 'Меню',
              onPressed: () => Keys.scaffoldKeys[KeyType.home].currentState.openDrawer()
          ),
        ),
        body: LayoutBuilder(
            builder: (context, constraints)=>SafeArea(
                child: new Container(
                    color: Theme.of(context).primaryColor,
                    child: new StoreConnector<AppState, SinglsViewModel>(
                      onInit: (store) {
                        store.dispatch(LoadSinglsAction(0));
                      },
                      converter: (store) => SinglsViewModel.fromStore(store),
                      builder: (_, viewModel) => content(viewModel, constraints),
                    )
                )
            )
        )
    );
  }

  Widget content(SinglsViewModel viewModel, BoxConstraints constraints)
  {
    if(!viewModel.isLoading && !viewModel.isError) {
      _refreshController.loadComplete();
      _refreshController.refreshCompleted();

      return
        SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            onRefresh: () =>  viewModel.pullRefresh(),
            onLoading: () =>  viewModel.loadMore(),
            controller: _refreshController,
            child: ListView.builder(
              itemCount: viewModel.data.length,
              itemBuilder: (context, index) {
                return AlbumView(viewModel.data[index], () => viewModel.navAlbumDetail(viewModel.data[index]));
              },
            )
        );
    }
    else if(viewModel.isError){
      return InfoNetworkView(() => viewModel.loadMore());
    }

    return  const Center(
      child: CircularProgressIndicator(),
    );
  }
}