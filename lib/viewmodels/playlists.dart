
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';

class PlaylistViewModel {
  final List<Playlist> data;
  final LoadingStatus status;
  final Function delete;
  final Function edit;
  final Function navPlaylistDetail;
  final Function pullRefresh;

  PlaylistViewModel({
    this.data,
    this.status,
    this.delete,
    this.edit,
    this.navPlaylistDetail,
    this.pullRefresh
  });

  static PlaylistViewModel fromStore(Store<AppState> store) {
    return PlaylistViewModel(
        data: store.state.playlistsState.data,
        status: store.state.playlistsState.loadingStatus,
        edit:(Playlist playlist){
          store.dispatch(UpdatePlaylistAction(playlist.Id, playlist.Name));
        },
        delete:(Playlist playlist){
          store.dispatch(DeletePlaylistAction(playlist.Id));
        },
        navPlaylistDetail:(Playlist playlist){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.playlist_detail, arguments: playlist));
        },
        pullRefresh: (){
          store.dispatch(new LoadPlaylistsAction());
        }
    );
  }
}