
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/actions/realeses_actions.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/realeses.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';

class RealesesViewModel {
  final Realeses data;
  final LoadingStatus status;
  final Function load;
  final Function navAlbumDetail;
  final Function navArtistDetail;

  RealesesViewModel({
    this.data,
    this.status,
    this.load,
    this.navAlbumDetail,
    this.navArtistDetail,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is RealesesViewModel &&
              data == other.data &&
              status == other.status &&
              load == other.load;

  @override
  int get hashCode => data.hashCode ^ status.hashCode ^ load.hashCode;

  bool get isLoading => (this.status == LoadingStatus.loading);
  bool get isError => (this.status == LoadingStatus.error);

  static RealesesViewModel fromStore(Store<AppState> store) {
    return RealesesViewModel(
      data: store.state.realesesState.data,
      status: store.state.realesesState.loadingStatus,
      load:(){
        store.dispatch(new LoadRealesesAction());
      },
      navAlbumDetail:(Album album){
        store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.album_detail, arguments: album));
      },
      navArtistDetail:(Artist artist){
        store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.artist_detail, arguments: artist));
      }
    );
  }
}
