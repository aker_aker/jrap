
import 'package:jrap_mobile/actions/download_action.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/models/task_info.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:redux/redux.dart';


class DownloadViewModel {
  final List<DownloadSong> data;
  final LoadingStatus status;
  final Function delete;
  final Function loadMore;
  final Function selectSong;

  DownloadViewModel({
    this.data,
    this.status,
    this.delete,
    this.loadMore,
    this.selectSong,
  });

  static DownloadViewModel fromStore(Store<AppState> store) {
    return DownloadViewModel(
        data: store.state.downloadState.data_page,
        status: store.state.downloadState.loadingStatus,
        delete:(DownloadSong item){
          store.dispatch(DeleteDownloadSongAction(item));
        },
        loadMore: ()
        {
          store.dispatch(LoadDownloadAction());
        },
        selectSong:(DownloadSong dSong){
          store.dispatch(
            AddQueueAndClear([dSong.song], new InfoQueue(Id: dSong.song.SongId, playObject: dSong, playType: PlayType.downloadSong))
          );
        }

    );
  }
}