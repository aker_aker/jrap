

import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/models/info_queue.dart';

import 'package:jrap_mobile/states/app_state.dart';

import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:redux/redux.dart';

class PlaylistDetailViewModel {
  final List<Song> tracks;
  final LoadingStatus status;
  final Function firstLoad;
  final Function play;
  final Playlist playlist;

  final Function deletePlaylist;
  final Function editPlaylist;
  final Function selectSong;

  PlaylistDetailViewModel({
    this.tracks,
    this.status,
    this.firstLoad,
    this.play,
    this.playlist,
    this.deletePlaylist,
    this.editPlaylist,
    this.selectSong,
  });

  bool get isLoading{
    return this.status == LoadingStatus.loading;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is PlaylistDetailViewModel &&
              tracks == other.tracks &&
              playlist == other.playlist &&
              status == other.status;

  @override
  int get hashCode =>  tracks.hashCode ^ status.hashCode ;

  static PlaylistDetailViewModel fromStore(Store<AppState> store, Playlist playlist) {
    return PlaylistDetailViewModel(
        tracks:
            (store.state.playlistDetailState.tracks==null ||
             store.state.playlistDetailState.playlist == null ||
            store.state.playlistDetailState.playlist?.Id != playlist.Id) ? [] : store.state.playlistDetailState.tracks,
        status:  (store.state.playlistDetailState.tracks==null ||
            store.state.playlistDetailState.playlist == null ||
            store.state.playlistDetailState.playlist?.Id != playlist.Id) ? LoadingStatus.loading : store.state.playlistDetailState.loadingStatus,
        playlist: playlist,
        firstLoad: (){
          store.dispatch(new LoadPlaylistDetailAction(playlist));
        },
        play: (List<Song> _tracks, InfoQueue _info){
          store.dispatch(
              AddQueueAndClear(_tracks.where((item)=>item.IsVkLoad).toList(), _info)
          );
        },
        editPlaylist:(Playlist playlist){
          store.dispatch(new UpdatePlaylistAction(playlist.Id, playlist.Name));
        },
        deletePlaylist:(Playlist playlist){
          store.dispatch(new DeletePlaylistAction(playlist.Id));
        },
        selectSong:(Song selectSong, InfoQueue info){
          store.dispatch(SelectSong(selectSong, info));
        }
    );
  }
}
