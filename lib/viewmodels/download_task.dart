import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/task_info.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:redux/redux.dart';


class DownloadTaskViewModel {
  final List<DownloadTaskInfo> data;
  final LoadingStatus status;
  final Function deleteTask;
  final Function(TaskInfo) cancelTask;
  final Function(TaskInfo) resumeTask;
  final Function(TaskInfo) pauseTask;
  final Function(TaskInfo) retryTask;
  final Function pullRefresh;


  DownloadTaskViewModel({
    this.data,
    this.status,
    this.deleteTask,
    this.cancelTask,
    this.resumeTask,
    this.pauseTask,
    this.retryTask,
    this.pullRefresh,
  });

  static DownloadTaskViewModel fromStore(Store<AppState> store) {
    return DownloadTaskViewModel(
        data: store.state.downloadTaskState.data_page,
        status: store.state.downloadTaskState.loadingStatus,
        deleteTask:(TaskInfo taskInfo){
          store.dispatch(TaskDownloadDelete(taskInfo));
        },
        cancelTask:(TaskInfo taskInfo){
          store.dispatch(TaskDownloadCancel(taskInfo));
        },
        resumeTask:(TaskInfo taskInfo){
          store.dispatch(TaskDownloadResume(taskInfo));
        },
        pauseTask:(TaskInfo taskInfo){
          store.dispatch(TaskDownloadPause(taskInfo));
        },
        retryTask:(TaskInfo taskInfo){
          store.dispatch(TaskDownloadRetry(taskInfo));
        },
      pullRefresh:(){
          store.dispatch(new LoadDownloadTaskAction());
        },

    );
  }
}