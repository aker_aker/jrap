
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';

class ArtistDetailViewModel {
  final List<Song> tracks;
  final List<Album> albums;
  final List<Album> singls;
  final Artist artist;
  final LoadingStatus status;
  final Function firstLoad;
  final Function navAlbumDetail;
  final Function navArtistDetail;
  final Function navAlbums;
  final Function navSingls;

  ArtistDetailViewModel({
    this.tracks,
    this.albums,
    this.singls,
    this.artist,
    this.status,
    this.firstLoad,
    this.navAlbumDetail,
    this.navArtistDetail,
    this.navAlbums,
    this.navSingls
  });

  bool get isLoading{

    return this.status == LoadingStatus.loading;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is ArtistDetailViewModel &&
              tracks == other.tracks &&
              albums == other.albums &&
              singls == other.singls &&
              artist == other.artist &&
              status == other.status;

  @override
  int get hashCode => albums.hashCode ^ tracks.hashCode ^ status.hashCode ^ singls.hashCode ^ artist.hashCode;

  static ArtistDetailViewModel fromStore(Store<AppState> store, Artist artist) {
    return ArtistDetailViewModel(
        tracks: store.state.artistDetailState.tracks,
        status: (store.state.artistDetailState.artist == null || store.state.artistDetailState.artist.ArtistId != artist.ArtistId) ? LoadingStatus.loading : store.state.artistDetailState.loadingStatus,
        albums: store.state.artistDetailState.albums,
        singls: store.state.artistDetailState.singls,
        artist: artist ?? store.state.artistDetailState.artist,
        firstLoad: (){
          store.dispatch(LoadArtistDetailAction(artist));
        },
        navAlbums:(){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.artist_detail_albums, arguments: store.state.artistDetailState.artist));
        },
        navSingls:(){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.artist_detail_singls, arguments: store.state.artistDetailState.artist));
        },
        navAlbumDetail:(Album album){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.album_detail, arguments: album));
        },
        navArtistDetail:(Artist artist){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.artist_detail, arguments: artist));
        }
    );
  }
}
