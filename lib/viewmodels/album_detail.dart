
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:redux/redux.dart';

class AlbumDetailViewModel {
  final List<Song> tracks;
  final Album album;
  final LoadingStatus status;
  final Function firstLoad;
  final Function play;
  final Function getPlaylists;
  final LoadingStatus status_playlist;
  final List<Playlist> playlist;
  final Function selectSong;

  AlbumDetailViewModel({
    this.tracks,
    this.album,
    this.status,
    this.firstLoad,
    this.play,
    this.getPlaylists,
    this.status_playlist,
    this.playlist,
    this.selectSong,
  });

  bool get isLoading{
    return this.status == LoadingStatus.loading;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AlbumDetailViewModel &&
              tracks == other.tracks &&
              album == other.album &&
              status_playlist == other.status_playlist &&
              status == other.status;

  @override
  int get hashCode => album.hashCode ^ tracks.hashCode ^ status.hashCode ^ status_playlist.hashCode;

  static AlbumDetailViewModel fromStore(Store<AppState> store, Album album) {
    return AlbumDetailViewModel(
        tracks: (store.state.albumDetailState.album == null  || store.state.albumDetailState.album.AlbumId != album.AlbumId) ? [] : store.state.albumDetailState.tracks,
        status: (store.state.albumDetailState.album== null || store.state.albumDetailState.album.AlbumId != album.AlbumId) ? LoadingStatus.loading : store.state.albumDetailState.loadingStatus,
        album: album,
        status_playlist: store.state.albumDetailState.loadingPlaylistStatus,
        playlist: store.state.playlistsState.data ?? [],
        firstLoad: (){
          store.dispatch(LoadAlbumDetailAction(album));
        },
        play: (List<Song> _tracks, InfoQueue _info){
            store.dispatch(
                AddQueueAndClear(_tracks.where((item)=>item.IsVkLoad).toList(), _info)
            );
        },
        getPlaylists: (){
          store.dispatch(LoadPlaylistsAction());
        },
        selectSong:(Song selectSong, InfoQueue info){
          store.dispatch(SelectSong(selectSong, info));
        }
    );
  }
}
