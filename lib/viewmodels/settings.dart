
import 'package:jrap_mobile/actions/search_action.dart';
import 'package:jrap_mobile/actions/settings_action.dart';
import 'package:jrap_mobile/models/search_all.dart';
import 'package:jrap_mobile/models/setting.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:redux/redux.dart';

class SettingsViewModel {
  final List<SettingUser> data;
  final LoadingStatus status;
  final Function clear;

  //изменение настройики
  final Function editSetting;

  bool get isLoading {
    return this.status == LoadingStatus.loading;
  }

  SettingStore get settingStore {
    SettingUser _val  = this.data.where((item) => item.SettingName == "store").first;
    return SettingStore.values.firstWhere((f)=> f.toString() == _val.SettingVal, orElse: () => SettingStore.store);
  }

  bool get settingPush {
    SettingUser _val  = this.data.where((item) => item.SettingName == "push").first;
    return  (_val.SettingVal == "1") ? true : false;
  }

  bool get settingSort {
    SettingUser _val  = getByKey("sort");
    return  (_val.SettingVal == "1") ? true : false;
  }

  int get settingMode {
    SettingUser _val  = getByKey("mode");
    return  int.parse(_val.SettingVal);
  }

  SettingUser getByKey(String key){
    return data.firstWhere((x) =>  x.SettingName == key);
  }

  SettingsViewModel({
    this.data,
    this.status,
    this.clear,
    this.editSetting
  });

  static SettingsViewModel fromStore(Store<AppState> store) {
    return SettingsViewModel(
      data: store.state.settingsState.data,
      status: store.state.realesesState.loadingStatus,
      clear: (){
        store.dispatch(SearcClearTextAction());
      },
      editSetting: (SettingUser _setting, String newValue){
        _setting.SettingVal = newValue;
        store.dispatch(UpdateSettingUser(_setting));
      }
    );
  }
}