
import 'package:jrap_mobile/actions/search_action.dart';
import 'package:jrap_mobile/models/search_all.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:redux/redux.dart';


class SearchViewModel {
  final SearchAll data;
  final LoadingStatus status;
  final Function search;
  final Function clear;
  final String text;

  bool get isLoading {
    return this.status == LoadingStatus.loading;
  }

  bool get notFound {
    if(data.items.length > 0) {
      var _cnt = data.items.where((item) => item.cntAll > 0).length;
      if(_cnt > 0)
         return false;
    }
    return true;
  }

  bool get isFirstLoad {
    return this.status == LoadingStatus.none;
  }

  SearchViewModel({
    this.data,
    this.status,
    this.search,
    this.clear,
    this.text,
  });

  static SearchViewModel fromStore(Store<AppState> store) {
    return SearchViewModel(
      data: store.state.searchState.data,
      status: store.state.searchState.loadingStatus,
      search: (String text){
        store.dispatch(SearchQuery(text));
      },
      clear: (){
        store.dispatch(SearcClearTextAction());
      },
      text: store.state.searchState.text
    );
  }
}