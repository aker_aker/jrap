
import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';

class ArtistsViewModel {
  final List<Artist> data;
  final LoadingStatus status;
  final Function loadMore;
  final Function navArtistDetail;
  final Function pullRefresh;

  ArtistsViewModel({
    this.data,
    this.status,
    this.loadMore,
    this.pullRefresh,
    this.navArtistDetail,
  });

  bool get isLoading => (this.status == LoadingStatus.loading);
  bool get isError => (this.status == LoadingStatus.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is ArtistsViewModel &&
              data == other.data &&
              loadMore == other.loadMore &&
              status == other.status;

  @override
  int get hashCode => data.hashCode ^ status.hashCode ;

  static ArtistsViewModel fromStore(Store<AppState> store) {
    return ArtistsViewModel(
      data: store.state.artistsState.data,
      status: store.state.artistsState.loadingStatus,
      loadMore:(){
         double page =  store.state.artistsState.data.length / 10;
         store.dispatch(new LoadArtistsAction(page.round()));
      },
      pullRefresh:(){
        store.dispatch(new LoadArtistsAction(0));
      },
      navArtistDetail:(Artist artist){
        store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.artist_detail, arguments: artist));
      }
    );
  }
}
