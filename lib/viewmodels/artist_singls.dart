import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:jrap_mobile/viewmodels/albums.dart';
import 'package:redux/redux.dart';

class ArtistSinglsViewModel {
  final List<Album> data;
  final LoadingStatus status;
  final Function(int) loadMore;
  final Function navAlbumDetail;


  ArtistSinglsViewModel({
    this.data,
    this.status,
    this.loadMore,
    this.navAlbumDetail,
  });

  bool get isLoading => (this.status == LoadingStatus.loading);
  bool get isError => (this.status == LoadingStatus.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AlbumsViewModel &&
              data == other.data &&
              loadMore == other.loadMore &&
              status == other.status;

  @override
  int get hashCode => data.hashCode ^ status.hashCode ;

  static ArtistSinglsViewModel fromStore(Store<AppState> store) {
    return ArtistSinglsViewModel(
      data: store.state.artistDetailState.singls,
      status: store.state.albumsState.loadingStatus,
      loadMore:(int ArtistId){
         store.dispatch(new LoadArtistSinglsAction(ArtistId));
      },
      navAlbumDetail:(Album album){
        store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.album_detail, arguments: album));
      },
    );
  }
}
