import 'package:flutter/material.dart';
import 'package:jrap_mobile/actions/auth_actions.dart';
import 'package:jrap_mobile/pages/home.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/screen_status.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';
//import 'package:vk_login_plugin/vk_login_plugin.dart';


class LoginViewModel{

  final LoadingStatus status;
  final ScreenState type;

  final Function(BuildContext) login;
  final Function(BuildContext) offline;
  final Function() comletLogin;
  final Function clearError;
  final Function navigateToRegistration;

  bool isAuthenticated;
  final String token;

  LoginViewModel({this.status,
    this.type,
    this.login,
    this.offline,
    this.clearError,
    this.navigateToRegistration,
    this.isAuthenticated,
    this.comletLogin,
    this.token});


  static LoginViewModel fromStore(Store<AppState> store){
    return LoginViewModel(
        status: store.state.authState.loadingStatus,
        type: store.state.authState.type,
        isAuthenticated: store.state.authState.isAuthenticated,
        login:(BuildContext context) {
          store.dispatch( AuthVKAction(
            hasTokenCallback: (){
              //Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context)=> HomePage()));
              Keys.navKey[KeyType.main].currentState..pushNamedAndRemoveUntil('/home', (_) => false);
            },
            noTokenCallback: (){
            })
          );
        },
        offline:(BuildContext context) {
          store.dispatch( AuthOffline());
        },
        clearError: () => store.dispatch(ClearErrorsAction()),
        navigateToRegistration: () => store.dispatch(NavigateToRegistrationAction()),
        comletLogin: (){
          //Navigator.pushNamedAndRemoveUntil(Keys.navKey[KeyType.init].currentContext, '/home', (_) => false);
          Keys.navKey[KeyType.main].currentState..pushNamedAndRemoveUntil('/home', (_) => false);
          //Navigator.of(null).pushReplacement(MaterialPageRoute(builder: (context)=> HomePage()));
        }
    );
  }
}
