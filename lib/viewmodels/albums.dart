
import 'package:jrap_mobile/actions/albums_action.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';

class AlbumsViewModel {
  final List<Album> data;
  final LoadingStatus status;
  final Function loadMore;
  final Function pullRefresh;
  final Function navAlbumDetail;

  AlbumsViewModel({
    this.data,
    this.status,
    this.loadMore,
    this.pullRefresh,
    this.navAlbumDetail
  });

  bool get isLoading => (this.status == LoadingStatus.loading);
  bool get isError => (this.status == LoadingStatus.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AlbumsViewModel &&
              data == other.data &&
              loadMore == other.loadMore &&
              pullRefresh == other.pullRefresh &&
              status == other.status;

  @override
  int get hashCode => data.hashCode ^ status.hashCode ^ pullRefresh.hashCode ;

  static AlbumsViewModel fromStore(Store<AppState> store) {
    return AlbumsViewModel(
      data: store.state.albumsState.data,
      status: store.state.albumsState.loadingStatus,
      loadMore:(){
         double page =  store.state.albumsState.data.length / 10;
         store.dispatch(new LoadAlbumsAction(page.round()));
      },
      pullRefresh:(){
        store.dispatch(new LoadAlbumsAction(0));
      },
      navAlbumDetail:(Album album){
        store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.album_detail, arguments: album));
      },
    );
  }
}
