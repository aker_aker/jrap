import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/viewmodels/albums.dart';
import 'package:redux/redux.dart';

class ArtistSongsViewModel {
  final List<Song> data;
  final LoadingStatus status;
  final Function(int) loadMore;



  ArtistSongsViewModel({
    this.data,
    this.status,
    this.loadMore,
  });

  bool get isLoading => (this.status == LoadingStatus.loading);
  bool get isError => (this.status == LoadingStatus.error);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AlbumsViewModel &&
              data == other.data &&
              loadMore == other.loadMore &&
              status == other.status;

  @override
  int get hashCode => data.hashCode ^ status.hashCode ;

  static ArtistSongsViewModel fromStore(Store<AppState> store) {
    return ArtistSongsViewModel(
      data: store.state.artistDetailState.tracks,
      status: store.state.albumsState.loadingStatus,
      loadMore:(int ArtistId){

         store.dispatch(new LoadArtistAlbumsAction(ArtistId));
      },
    );
  }
}
