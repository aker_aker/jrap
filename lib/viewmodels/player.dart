import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/duration_range.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/queue.dart';
import 'package:jrap_mobile/utils/enums.dart';
import 'package:redux/redux.dart';

class PlayerViewModel {
  final Queue queue;
  final Function downloadSong;
  final InfoQueue info;

  final Function pause;
  final Function play;
  final Function next;
  final Function prev;
  final Function changePlayMode;
  final Function changeShuffleMode;

  final RepeatMode mode;
  final ShuffleMode shuffleMode;
  final PlaybackState playbackState;
  final bool playWhenReady;

  final Function seekTo;
  final List<DurationRange> buffered;

  PlayerViewModel({
    this.queue,
    this.info,
    this.prev,
    this.next,
    this.pause,
    this.play,
    this.downloadSong,
    this.changePlayMode,
    this.changeShuffleMode,
    this.mode,
    this.shuffleMode,
    this.playbackState,
    this.playWhenReady,
    this.seekTo,
    this.buffered = const [],
  });

  bool get isPlaying =>
      (playbackState == PlaybackState.ready) && playWhenReady ;//&& !hasError;

  ///audio is buffering
  bool get isBuffering => playbackState!=null && playbackState == PlaybackState.buffering;// && !hasError;

  static PlayerViewModel fromStore(Store<AppState> store)
  {
    return PlayerViewModel(
        queue: store.state.playerState.queue,
        info: store.state.playerState.info,
        mode: store.state.playerState.mode,
        shuffleMode: store.state.playerState.shuffleMode,
        playbackState: store.state.playerState.statePlayer,
        playWhenReady: store.state.playerState.playWhenReady,
        play:(){
          store.dispatch(new PlaySong());
        },
        pause:(){
          store.dispatch(new PauseSong());
        },
        prev:(){
          store.dispatch(new PrevSong());
        },
        next:(){
          store.dispatch(new NextSong());
        },
        downloadSong: (Song song, Album album){
          store.dispatch(RequestDownloadSong(song, album));
        },
        changePlayMode: (){
          store.dispatch(ChangePlayMode());
        },
        changeShuffleMode: (){
          store.dispatch(ChangeShuffleMode());
        },
        seekTo: (int val){
          store.dispatch(SeekTo(val));
        },

    );
  }
}