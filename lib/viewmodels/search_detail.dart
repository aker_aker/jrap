

import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/models/search.dart';
import 'package:jrap_mobile/routes.dart';

import 'package:jrap_mobile/states/app_state.dart';

import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';

class SearchDetailViewModel {
  final LoadingStatus status;
  final String search;
  final List data;
  final SearchBlock<dynamic> block;
  final Function(int) loadMore;
  final navAlbumDetail;
  final navArtistDetail;

  SearchDetailViewModel({
    this.status,
    this.search,
    this.data,
    this.block,
    this.loadMore,
    this.navAlbumDetail,
    this.navArtistDetail
  });

  bool get isLoading {
    return this.status == LoadingStatus.loading;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is SearchDetailViewModel &&
              status == other.status &&
              data == other.data &&
              block == other.block &&
              search == other.search;

  @override
  int get hashCode =>  status.hashCode ^ search.hashCode ^ data.hashCode ^ block.hashCode;

  static SearchDetailViewModel fromStore(Store<AppState> store,  SearchBlock<dynamic> block) {
    return SearchDetailViewModel(
        data: block?.items,
        block: block,
        search: store.state.searchState.text,
        status: store.state.searchDetailState.loadingStatus,
        loadMore:(int cnt){
          //double page =  cntAlbum / 10;
          //store.dispatch(new LoadAlbumsAction(page.round()));
        },
        navAlbumDetail:(Album album){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.album_detail, arguments: album));
        },
        navArtistDetail:(Artist artist){
          store.dispatch(NavigatePushAction(keyNav: KeyType.over, routeName: AppRoutes.artist_detail, arguments: artist));
        }
    );
  }
}
