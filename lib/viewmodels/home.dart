
import 'package:flutter/cupertino.dart';
import 'package:jrap_mobile/actions/auth_actions.dart';
import 'package:jrap_mobile/actions/home_action.dart';
import 'package:jrap_mobile/actions/navigation_action.dart';
import 'package:jrap_mobile/models/user.dart';
import 'package:jrap_mobile/pages/download.dart';
import 'package:jrap_mobile/pages/playlist.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/utils/keys.dart';
import 'package:redux/redux.dart';

class HomeViewModel {
  final User user;
  final int tabIndex;
  final int curTabIndex;
  final bool isPlayer;
  final int curDrawerIndex;


  final Function setTab;
  final Function logOut;
  final Function setDrawer;


  HomeViewModel({
    this.tabIndex,
    this.curTabIndex,
    this.curDrawerIndex,
    this.isPlayer,
    this.user,
    this.setTab,
    this.logOut,
    this.setDrawer,
  });


  static HomeViewModel fromStore(Store<AppState> store) {
    return HomeViewModel(
        tabIndex: store.state.homeState.tabIndex,
        curTabIndex: store.state.homeState.curTabIndex,
        isPlayer: store.state.homeState.isPlayer,
        user: store.state.authState.user,
        logOut: (){
          store.dispatch(LogOut());
        },
        setTab: (int tabIndex, int curretnTab){
          store.dispatch(NavigatePopUntilAction(AppRoutes.overlay));
          store.dispatch(SetTab(tabIndex,curretnTab));
        },
        setDrawer: (int index) {
          if(index == 0) {
            store.dispatch(NavigatePushAction(routeName:AppRoutes.playlists, keyNav:KeyType.over));
          }
          else if(index == 1){
            store.dispatch(NavigatePushAction(routeName:AppRoutes.download, keyNav:KeyType.over));
          }
          else if(index == 2){
            store.dispatch(NavigatePushAction(routeName:AppRoutes.tasks, keyNav:KeyType.over));
          }
          else if(index == 3){
            store.dispatch(NavigatePushAction(routeName:AppRoutes.search, keyNav:KeyType.over));
          }
          else if(index == 4){
            store.dispatch(NavigatePushAction(routeName:AppRoutes.setting, keyNav:KeyType.over));
          }
        }
    );
  }
}
