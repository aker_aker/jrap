import 'dart:core';

const vk_str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMN0PQRSTUVWXYZO123456789+/=";

dynamic vk_o(str) {
  var len = str.length;
  var result = '';
  var i = 0;
  for (var s = 0, index2 = 0; s < len; s++) {
    var sym_index = vk_str.indexOf(str[s]);
    if (sym_index >= 0) {
      i = ((index2 % 4) != 0) ? ((i << 6) + sym_index) : sym_index;
      if ((index2 % 4) != 0) {
        index2++;
        var shift = -2 * index2 & 6;
        result += String.fromCharCode((0xFF & (i >> shift)));
      }
      else {
        index2++;
      }
    }
  }
  return result;
}

dynamic vk_s(str, int start) {
  int len = str.length;
  var sb = str;
  if (len > 0) {
    var cur = start.abs();
    var shuffle_pos = new List(len);
    for (var i = len - 1; i >= 0; i--) {
      cur = ((len * (i + 1)) ^ cur + i) % len;
      shuffle_pos[i] = cur;
    }

    for (var i = 1; i < len; i++) {
      var offset = shuffle_pos[len - i - 1];
      var prev = sb[i];
//sb[i] = sb[offset];
      sb = sb.substring(0, i) + sb[offset] + sb.substring(i + 1);
      sb = sb.substring(0, offset) + prev + sb.substring(offset + 1);
    }
  }

  return sb;
}

dynamic vk_i(String str, int i) {
  str = str.substring(0, str.length - 5);
  return vk_s(str, i ^ 7263307);
}

String decode(str) {
  var vals = str.split('#');
  vals[0] = str.split('?extra=')[1];

  String tstr = vk_o(vals[0]);
  String ops = vk_o(vals[1]);

  var ops_arr = ops.split(String.fromCharCode(9)); // explode(chr(9), $ops);
  var len = ops_arr.length;
  for (var i = len - 1; i >= 0; i--) {
    var args_arr = ops_arr[i].split(String.fromCharCode(11));
    var op_ind = args_arr[0];
    args_arr.removeAt(0);
    switch (op_ind) {
      case "i":
        tstr = vk_i(tstr, int.parse(args_arr[0]));
        break;
    }
  }
  return tstr;
}