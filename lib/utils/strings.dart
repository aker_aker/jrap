const email = "Email";
const password ="Password";
const signup = "Sign Up";
const label_login = "Log In";
const label_offline= "Offline";


const forgot_password = "Forgot password?";
const terms_and_cond_label ="By signing up you agree to";
const terms = "Terms & Conditions";
const policy = "Privacy Policy";
const and_with_spaces = " and ";
const create_account ="Create account";
const retype_password = "Re-Type Password";
const select_gender = "Select Gender";
const undefined = "Unidentified";
const male = "Male";
const female = "Female";
const create = "Create";

//Errors
const email_error = "Email is not valaid.";
const password_error = "Password should be at least 6 symbols long.";
const password_match_error = "Passwords are not match.";
const code_error = "Code is not valid.";

//Title
const realeses_title = "Новинки";
const albums_title = "Альбомы";
const singls_title = "Синглы";
const artists_title = "Артисты";
const search_title = "Поиск";
const playlists_title = "Плейлисты";

//Меню в деталях альбома
const action_add_album_playlist = "Добавить в плейлист";
const action_nav_artist = "Исполнитель";
const action_nav_download = "Скачать";
const List<String> action_albums_detail = <String>[
  action_add_album_playlist,
  action_nav_artist
];

//Меню в деталях плейлиста
const action_add_song_by_playlist = "Добавить треки";
const action_add_by_playlist = "Добавить в плейлист";
const action_edit_playlist= "Редактировать";
const action_remove_playlist = "Удалить";
const List<String> action_playlist_detail = <String>[
  action_add_song_by_playlist,
  //action_add_by_playlist,
  action_edit_playlist,
  action_remove_playlist
];

//Инфо
const info_search_not_fount_title = "Ничего не найдено";
const info_search_not_fount_subtitle = "Может, вы неправильно набрали какое-то слово?";
const info_network_no_connection_title = "Проблемы со связью";
const info_network_no_connection_subtitle = "Проверьте настройки сети\nили обновите экран.";
const info_no_connection_text = "Нет подключения к интернету";
const info_no_connection_retry= "Обнвить";
const info_playlist_empty_playlist = "В этом плейлисте ещё нет треков.\nХотите добавить их?";

const playlist_add_tracks_to_current_playlist = "Добавить треки";


//Настройки
const setting_music_store_title = "Где сохранить музыку";
const setting_music_playlist_title = "Добавлять в начало";
const setting_music_playlist_subtitle = "Все новые треки будут появляться в начале списка";
const setting_push_title = "Получать уведомления";
const setting_push_subtitle = "Получать сообщения о новых альбомах, треках и других важных вещах";
const setting_help_title = "Помощь";
const setting_message_dev_title = "Написать разработчикам";
const setting_about_title = "О приложение";
