import 'package:flutter/material.dart';

enum KeyType {
  main,
  home,
  over,
  album,
  realeses,
  artist,
  singls,
  backdrop
}

class Keys{

  static final Map<KeyType, GlobalKey<ScaffoldState>> scaffoldKeys = {
    KeyType.home:  GlobalKey<ScaffoldState>(),
    KeyType.singls:  GlobalKey<ScaffoldState>(),
    KeyType.album:  GlobalKey<ScaffoldState>(),
    KeyType.over:  GlobalKey<ScaffoldState>(),
    KeyType.realeses:  GlobalKey<ScaffoldState>(),
  };

  static final Map<KeyType, GlobalKey<NavigatorState>> navKey = {
    KeyType.main:  GlobalKey<NavigatorState>(),
    KeyType.over:  GlobalKey<NavigatorState>(),
    /*KeyType.album: new GlobalKey<NavigatorState>(),
    KeyType.artist: new GlobalKey<NavigatorState>(),
    KeyType.realeses: new GlobalKey<NavigatorState>(),
    KeyType.singls: new GlobalKey<NavigatorState>(),
    KeyType.over: new GlobalKey<NavigatorState>(),*/
  };
}