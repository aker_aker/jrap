import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/viewmodels/playlists.dart';
import 'package:permission_handler/permission_handler.dart';


Future<bool> checkPermission() async {
  if (/*widget.platform == TargetPlatform.android*/true) {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);
    if (permission != PermissionStatus.granted) {
      Map<PermissionGroup, PermissionStatus> permissions =
      await PermissionHandler()
          .requestPermissions([PermissionGroup.storage]);
      if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
        return true;
      }
    } else {
      return true;
    }
  } else {
    return true;
  }
  return false;
}

Future<String> asyncInputDialog(BuildContext context) async {
  String playlistName = '';
  return showDialog<String>(
    context: context,
    barrierDismissible: false, // dialog is dismissible with a tap on the barrier
    builder: (BuildContext ctx) {
      return AlertDialog(
        title: Text('Новый плейлист', style: TextStyle(color: Colors.white)),
        backgroundColor: Color(0xff354657),
        content: Row(
          children: <Widget>[
             Expanded(
                child: TextField(
                  style: TextStyle(color:Colors.white),
                  autofocus: true,
                  decoration: InputDecoration(
                      hintStyle: TextStyle(fontSize: 20.0, color: Colors.white70),
                      fillColor: Colors.white,
                      hintText: 'название'
                    ),
                    onChanged: (value) {
                      playlistName = value;
                    },
                )
            )
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Сохранить'),
            onPressed: (){
              /*if(isReturn == false) {
                if(playlistName!= null && playlistName.isNotEmpty) {
                  StoreProvider.of<AppState>(context).dispatch(AddPlaylistAction(playlistName));
                  Navigator.of(context).pop();
                }
              }
              else*/
              if(playlistName!= null && playlistName.isNotEmpty) {
                Navigator.of(ctx).pop(playlistName);
              }
            },
          ),
          FlatButton(
            child: Text('Отмена'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          ),
        ],
      );
    },
  );
}

Future<int> asyncEditDialog(BuildContext context) async {
  String playlistName = "";
  return showDialog<int>(
    context: context,
    barrierDismissible: false, // dialog is dismissible with a tap on the barrier
    builder: (BuildContext ctx) {
      return  StoreConnector<AppState, PlaylistViewModel>(
        onInit: (store) =>  store.dispatch(LoadPlaylistsAction()),
        converter: (store) => PlaylistViewModel.fromStore(store),
        builder: (context, viewModel) {
          return AlertDialog(
            contentPadding: EdgeInsets.all(0),
            titlePadding: EdgeInsets.all(0),
            backgroundColor:  Color(0xff354657),
            title: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
                  child: Text('Добавить в плейлист', style:TextStyle(color: Colors.white)),
                ),
                InkWell(
                  child: ListTile(
                      leading: Icon(Icons.add, color: Colors.white,),
                      title: Text('Новый плейлист', textAlign: TextAlign.start, style:TextStyle(color: Colors.white)),
                  ),
                  onTap:() async {
                    Navigator.of(context).pop(-1);
                  }
                )
              ],
            ) ,
            content:  Container(
              width: double.maxFinite,
              height: 300.0,
              child:  (viewModel.status  == LoadingStatus.success) ?
              ListView.builder(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  itemCount: viewModel.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return InkWell(
                        child: Container(
                            padding: new EdgeInsets.symmetric(vertical: 8.0, horizontal:  16.0),
                            child: (viewModel.data.length > 0)  ?
                            ListTile(
                                contentPadding: EdgeInsets.all(0.0),
                                title: Text(
                                    viewModel.data[index].Name,
                                    style:  TextStyle(color:Colors.white, fontWeight: FontWeight.bold)
                                ),
                                subtitle: (viewModel.data[index].CountTracks > 0) ?
                                    Text("${viewModel.data[index].CountTracks} - трека",
                                        style:  TextStyle(color:Colors.white70)) : Container(),
                            ) : Container(),
                        ),
                        onTap: (){
                           Navigator.of(ctx).pop(viewModel.data[index].Id);
                        },
                      );
                  })
                  :
                  Center(
                    child: new CircularProgressIndicator(),
                  ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Отмена'),
                onPressed: () {
                  Navigator.of(ctx).pop();
                },
              ),
            ],
          );
        },
      );
    },
  );
}

Future<String> asyncEditNamePlaylistDialog(BuildContext context, Playlist pl) async {
  String playlistName = pl.Name;
  return showDialog<String>(
    context: context,
    barrierDismissible: false, // dialog is dismissible with a tap on the barrier
    builder: (BuildContext ctx) {
      return AlertDialog(
        title: Text('Новое название'),
        content: new Row(
          children: <Widget>[
            new Expanded(
                child: new TextField(
                  autofocus: true,
                  controller: TextEditingController(text: playlistName),
                  decoration: new InputDecoration(hintText: 'Название'),
                  onChanged: (value) {
                    playlistName = value;
                  },
                ))
          ],
        ),
        actions: <Widget>[
          FlatButton(
            child: Text('Сохранить'),
            onPressed: () {
              Navigator.of(ctx).pop(playlistName);
            },
          ),
          FlatButton(
            child: Text('Отмена'),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          ),
        ],
      );
    },
  );
}