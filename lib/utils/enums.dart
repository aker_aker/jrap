enum PlayType {
  album,
  song,
  playlist,
  artist,
  downloadSong,
  none,
}

//Описывает глобальные типы
enum MainObjType {
  album,
  song,
  artist,
  singl
}


enum ConnectivityStatus {
  WiFi,
  Cellular,
  Offline
}

enum SettingStore { store, sd }

enum RepeatMode {
  //
  off,
  ///aways play single song
  single,

  ///play current list sequence
  sequence,

  ///random to play next song
}

enum  ShuffleMode {
  noshuffle,

  shuffle,
}


enum PlaybackState { none, buffering, ready, ended }

enum PlayerStateAspect {
  ///the position of playing
  position,

  ///the playing state
  playbackState,

  ///the current playing
  music,

  ///the current playing playlist
  playlist,

  ///the play mode of playlist
  playMode,
}