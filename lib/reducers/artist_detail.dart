import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/artist_detail.dart';
import 'package:redux/redux.dart';


final artistDetailReducer = combineReducers<ArtistDetailState>([
  TypedReducer<ArtistDetailState, ArtistDetailLoadCompletAction>(_artistDetailLoadCompletAction),
  TypedReducer<ArtistDetailState, ArtistAlbumsLoadCompletAction>(_artistAlbumsLoadCompletAction),
  TypedReducer<ArtistDetailState, ArtisSinglstLoadCompletAction>(_artisSinglstLoadCompletAction),
  TypedReducer<ArtistDetailState, ArtistSongsLoadCompletAction> (_artistSongsLoadCompletAction),

  TypedReducer<ArtistDetailState, LoadArtistSinglsAction> (_loadArtistSinglsAction),
  TypedReducer<ArtistDetailState, LoadArtistSongsAction> (_loadArtistSongsAction),
  TypedReducer<ArtistDetailState, LoadArtistAlbumsAction> (_loadArtistAlbumsAction),
]);


ArtistDetailState _artistDetailLoadCompletAction(ArtistDetailState state, ArtistDetailLoadCompletAction action) =>
    state.copyWith(loadingStatus: LoadingStatus.success, artist: action.artist, albums: action.detail.albums_top, singls: action.detail.singls_top, artist_related: action.detail.artist_related);

/******************/

ArtistDetailState _loadArtistSinglsAction(ArtistDetailState state, LoadArtistSinglsAction action) =>  state.copyWith(loadingStatus: LoadingStatus.loading);
ArtistDetailState _loadArtistSongsAction(ArtistDetailState state, LoadArtistSongsAction action) => state.copyWith(loadingStatus: LoadingStatus.loading);
ArtistDetailState _loadArtistAlbumsAction(ArtistDetailState state, LoadArtistAlbumsAction action) => state.copyWith(loadingStatus: LoadingStatus.loading);

/*****************/
ArtistDetailState _artistAlbumsLoadCompletAction(ArtistDetailState state, ArtistAlbumsLoadCompletAction action) =>
    state.copyWith(loadingStatus: LoadingStatus.success, albums: action.data);

ArtistDetailState _artisSinglstLoadCompletAction(ArtistDetailState state, ArtisSinglstLoadCompletAction action) =>
    state.copyWith(loadingStatus: LoadingStatus.success, singls: action.data);

ArtistDetailState _artistSongsLoadCompletAction(ArtistDetailState state, ArtistSongsLoadCompletAction action) =>
    state.copyWith(loadingStatus: LoadingStatus.success, tracks: action.data);



