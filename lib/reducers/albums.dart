import 'package:jrap_mobile/actions/albums_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/albums.dart';
import 'package:redux/redux.dart';


final albumsReducer = combineReducers<AlbumsState>([
  TypedReducer<AlbumsState, AlbumsLoadCompletAction>(_albumsLoadCompletAction),
  TypedReducer<AlbumsState, AlbumsPullRefreshCompletAction>(_albumsPullRefreshCompletAction)
]);

AlbumsState _albumsLoadCompletAction(AlbumsState state, AlbumsLoadCompletAction action) => state.copyWith(loadingStatus: action.loadingStatus, data: action.data);

AlbumsState _albumsPullRefreshCompletAction(AlbumsState state, AlbumsPullRefreshCompletAction action) => state.pullRefresh(loadingStatus: action.loadingStatus, data: action.data);

