import 'package:jrap_mobile/actions/playlist_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/playlists.dart';
import 'package:redux/redux.dart';


final playlistsReducer = combineReducers<PlaylistsState>([
  TypedReducer<PlaylistsState, PlaylistLoadStartAction>(_loadPlaylistsStartAction),
  TypedReducer<PlaylistsState, PlaylistLoadCompletAction>(_playlistssLoadCompletAction)
]);

PlaylistsState _loadPlaylistsStartAction(PlaylistsState state, PlaylistLoadStartAction action) {
  return state.copyWith(loadingStatus: LoadingStatus.loading);
}

PlaylistsState _playlistssLoadCompletAction(PlaylistsState state, PlaylistLoadCompletAction action)
  => state.copyWith(loadingStatus: LoadingStatus.success, data: action.data);

