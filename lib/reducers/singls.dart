import 'package:jrap_mobile/actions/singls_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/singls.dart';
import 'package:redux/redux.dart';


final singlsReducer = combineReducers<SinglsState>([
  TypedReducer<SinglsState, SinglsLoadCompletAction>(_singlsLoadCompletAction),
  TypedReducer<SinglsState, SinglsPullRefreshCompletAction>(_singlsPullRefreshCompletAction)
]);

SinglsState _singlsLoadCompletAction(SinglsState state, SinglsLoadCompletAction action) => state.copyWith(loadingStatus: action.loadingStatus, data: action.data);


SinglsState _singlsPullRefreshCompletAction(SinglsState state, SinglsPullRefreshCompletAction action) => state.pullRefresh(loadingStatus: action.loadingStatus, data: action.data);

