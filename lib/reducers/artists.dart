import 'package:jrap_mobile/actions/artists_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/artists.dart';
import 'package:redux/redux.dart';


final artistsReducer = combineReducers<ArtistsState>([
  TypedReducer<ArtistsState, ArtistsLoadCompletAction>(_artistsLoadCompletAction)
]);

ArtistsState _artistsLoadCompletAction(ArtistsState state, ArtistsLoadCompletAction action) => state.copyWith(loadingStatus: action.loadingStatus, data: action.data);
