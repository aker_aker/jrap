import 'package:jrap_mobile/actions/search_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/search.dart';
import 'package:redux/redux.dart';

final searchReducer = combineReducers<SearchState>([
  TypedReducer<SearchState, SearcCompletAction>(_searchCompletAction),
  TypedReducer<SearchState, SearcClearTextAction>(_searchClearTextAction),
  TypedReducer<SearchState, FirstLoadSearchPage>(_firstLoadPageAction),
]);

SearchState _searchCompletAction(SearchState state, SearcCompletAction action) => state.copyWith(loadingStatus: LoadingStatus.success, data: action.data, text: action.text);
SearchState _searchClearTextAction(SearchState state, SearcClearTextAction action) => state.copyWith(loadingStatus: LoadingStatus.success, text: "");
SearchState _firstLoadPageAction(SearchState state, FirstLoadSearchPage action) => state.copyWith(loadingStatus: LoadingStatus.none);