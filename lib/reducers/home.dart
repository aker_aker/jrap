import 'package:jrap_mobile/actions/auth_actions.dart';
import 'package:jrap_mobile/actions/home_action.dart';
import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/states/home.dart';
import 'package:redux/redux.dart';


final homeReducer = combineReducers<HomeState>([
  TypedReducer<HomeState, SetTab>(_setTabIndex),
  TypedReducer<HomeState, AddQueueAndClear>(_addQueueAndClear),
  TypedReducer<HomeState, SelectSong>(_selectSongr),

  TypedReducer<HomeState, AuthOffline>(_authOffline)
]);

HomeState _setTabIndex(HomeState state, SetTab action) =>state.copyWith(tabIndex: action.tabIndex);
HomeState _addQueueAndClear(HomeState state, AddQueueAndClear action) => state.copyWith(isPlayer: true);
HomeState _selectSongr(HomeState state, SelectSong action) => state.copyWith(isPlayer: true);
HomeState _authOffline(HomeState state, AuthOffline action) => state.copyWith(isOffline: true);