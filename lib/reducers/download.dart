import 'package:jrap_mobile/actions/download_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/download.dart';
import 'package:redux/redux.dart';


final downloadReducer = combineReducers<DownloadState>([
  TypedReducer<DownloadState, LoadDownloadCompleetAction>(_downloadSongLoadCompletAction),
  TypedReducer<DownloadState, ComleetDeleteDownloadSongAction>(_comleetDeleteDownloadSongAction),

]);

DownloadState _downloadSongLoadCompletAction(DownloadState state, LoadDownloadCompleetAction action) => state.copyWith(loadingStatus: LoadingStatus.success, data_page: action.data);
DownloadState _comleetDeleteDownloadSongAction(DownloadState state, ComleetDeleteDownloadSongAction action) => state.deleteCopyWith(dsong: action.dsong);
