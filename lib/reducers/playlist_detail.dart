import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/playlist_detail.dart';
import 'package:redux/redux.dart';


final playlistDetailReducer = combineReducers<PlaylistDetailState>([
  TypedReducer<PlaylistDetailState, PlaylistDetailLoadCompletAction>(_playlistDetailLoadCompletAction)
]);

PlaylistDetailState _playlistDetailLoadCompletAction(PlaylistDetailState state, PlaylistDetailLoadCompletAction action) => state.copyWith(loadingStatus: LoadingStatus.success, tracks: action.tracks, playlist: action.playlist);
