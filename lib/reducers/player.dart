import 'package:jrap_mobile/actions/player_action.dart';
import 'package:jrap_mobile/states/player.dart';
import 'package:redux/redux.dart';


final playerReducer = combineReducers<JRPlayerState>([
  TypedReducer<JRPlayerState, AddQueueComplete>(_addQueueComplete),
  TypedReducer<JRPlayerState, NextSongComplete>(_nextSongComplete),
  TypedReducer<JRPlayerState, PrevSongComplete>(_prevSongComplete),
  TypedReducer<JRPlayerState, PlayerStateChanged>(_playerStateChanged),
  TypedReducer<JRPlayerState, ChangePlayerState>(_changePlayerState),
  TypedReducer<JRPlayerState, ChangePlayModeComplete>(_changePlayModeComplete),
  TypedReducer<JRPlayerState, ChangeSongCompleet>(_changeSong),
]);

JRPlayerState _addQueueComplete(JRPlayerState state, AddQueueComplete action) => state.copyWith(queue: action.queue);
JRPlayerState _nextSongComplete(JRPlayerState state, NextSongComplete action) => state.copyWith(queue: action.queue);
JRPlayerState _prevSongComplete(JRPlayerState state, PrevSongComplete action) => state.copyWith(queue: action.queue);
JRPlayerState _playerStateChanged(JRPlayerState state, PlayerStateChanged action) => state.copyWith(playWhenReady: action.playWhenReady, statePlayer: action.newState);
JRPlayerState _changePlayerState(JRPlayerState state, ChangePlayerState action) => state.copyWith(queue: action.queue, info: action.info);

JRPlayerState _changePlayModeComplete(JRPlayerState state, ChangePlayModeComplete action) => state.copyWith(mode: action.mode);

JRPlayerState _changeSong(JRPlayerState state, ChangeSongCompleet action)
{
  var _state = state.copyWith();
  _state.queue.SelectSong(action.selectSong);
  return _state;
}

