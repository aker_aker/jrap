import 'package:jrap_mobile/actions/auth_actions.dart';
import 'package:jrap_mobile/actions/db_actions.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/auth.dart';
import 'package:redux/redux.dart';
/*import 'package:reduxsample/models/loading_status.dart';
import 'package:reduxsample/redux/auth/auth_state.dart';
import 'auth_actions.dart';
import 'screen_state.dart';
import 'screen.dart';*/

final authReducer = combineReducers<AuthState>([
  TypedReducer<AuthState,ClearErrorsAction>(_clearErrorsAction),
  TypedReducer<AuthState,AuthVKAction>(_authVKAction),
  TypedReducer<AuthState,AuthCompleetAction>(_authCompleetAction),
  TypedReducer<AuthState,SetUserAction>(_setUserAction),
  TypedReducer<AuthState,ChangeScreenStateAction>(_changeScreenStateAction),
]);

AuthState _setUserAction(AuthState state, SetUserAction action) => state.copyWith(user: action.user);


AuthState _changeScreenStateAction(AuthState state, ChangeScreenStateAction action) =>
    state.copyWith(type: action.type);

AuthState _clearErrorsAction(AuthState state, ClearErrorsAction action) =>
    state.copyWith(loadingStatus: LoadingStatus.success);

AuthState _authVKAction(AuthState state, AuthVKAction action) => state;

AuthState _authCompleetAction(AuthState state, AuthCompleetAction action) => state;


/*
AuthState _validateEmail(AuthState state, ValidateEmailAction action){
  return state.copyWith(email: action.email);
}

AuthState _validatePassword(AuthState state, ValidatePasswordAction action) =>
    state.copyWith(password: action.password);

AuthState _validateLoginFieldsAction(AuthState state, ValidateLoginFields action) => state;

AuthState _validatePasswordMatch(AuthState state, ValidatePasswordMatchAction action) =>
    state.copyWith(password: action.password,retypePassword: action.confirmPassword);

AuthState _validateCodeAction(AuthState state, ValidateCodeAction action) =>
    state.copyWith(code: action.code);

AuthState _changeLoadingStatusAction(AuthState state, ChangeLoadingStatusAction action) =>
    state.copyWith(loadingStatus: action.status);

AuthState _emailErrorAction(AuthState state, EmailErrorAction action){
  if(action.screen == Screen.SIGNUP){
    return state.copyWith(emailError: action.message);
  }else return state;
}

AuthState _passwordErrorAction(AuthState state, PasswordErrorAction action){
  if (action.screen == Screen.SIGNUP) {
    return state.copyWith(passwordError: action.message);
  }else return state;
}

AuthState _retypePasswordErrorAction(AuthState state, RetypePasswordErrorAction action) {
  if (action.screen == Screen.SIGNUP) {
    return state.copyWith(retypePasswordError: action.message);
  }else return state;
}

AuthState _codeErrorAction(AuthState state, CodeErrorAction action) =>
    state.copyWith(codeError: action.message);

AuthState _changeGenderAction(AuthState state, ChangeGenderAction action) =>
    state.copyWith(gender: action.gender);

AuthState _signInAction(AuthState state, SignInAction action) => state;

AuthState _signUpAction(AuthState state, SignUpAction action) => state;

AuthState _saveToken(AuthState state, SaveTokenAction action) =>
    state.copyWith(token: action.token);

AuthState _requestCodeAction(AuthState state, RequestCodeAction action) => state;

AuthState _confirmCodeAction(AuthState state, ConfirmForgotPasswordAction action) => state;

AuthState _navigateToRegistrationAction(AuthState state,NavigateToRegistrationAction action) => state;
*/

