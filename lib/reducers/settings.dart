import 'package:jrap_mobile/actions/settings_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/settings.dart';
import 'package:redux/redux.dart';

final settingsReducer = combineReducers<SettingsState>([
  TypedReducer<SettingsState, LoadSettingUserCompletAction>(_loadSettingCompletAction),
]);

SettingsState _loadSettingCompletAction(SettingsState state, LoadSettingUserCompletAction action) => state.copyWith(data: action.data, loadingStatus: LoadingStatus.success);

