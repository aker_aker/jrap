import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/download_task.dart';
import 'package:redux/redux.dart';


final downloadTaskReducer = combineReducers<DownloadTaskState>([
  TypedReducer<DownloadTaskState, AddTaskCompleet>(_addTaskCompletAction),
  TypedReducer<DownloadTaskState, RefreshTaskCompleet>(_refreshTaskCompletAction),
  TypedReducer<DownloadTaskState, LoadDownloadTaskCompleetAction>(_downloadTaskLoadCompletAction),
  TypedReducer<DownloadTaskState, RefreshTaskAfterDeleteAction>(_refreshTaskAfterDeleteAction),
  TypedReducer<DownloadTaskState, TaskDownloadResumeComplete>(_taskDownloadResumeComplete),

]);

DownloadTaskState _downloadTaskLoadCompletAction(DownloadTaskState state, LoadDownloadTaskCompleetAction action) => state.copyWith(loadingStatus: LoadingStatus.success, data: action.data);
DownloadTaskState _addTaskCompletAction(DownloadTaskState state, AddTaskCompleet action) => state.copySingleWith(task: action.task);
DownloadTaskState _refreshTaskCompletAction(DownloadTaskState state, RefreshTaskCompleet action) => state.copyWithRefreshTask(taskId: action.id, status: action.status,progress: action.progress);
DownloadTaskState _refreshTaskAfterDeleteAction(DownloadTaskState state, RefreshTaskAfterDeleteAction action) => state.copyWithAfterDelete(taskId: action.taskId);
DownloadTaskState _taskDownloadResumeComplete(DownloadTaskState state, TaskDownloadResumeComplete action) => state.copyWithRefreshId(taskId: action.taskId, newTaskId: action.newTaskId);
