import 'package:jrap_mobile/actions/details_action.dart';
import 'package:jrap_mobile/actions/task_action.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/album_detail.dart';
import 'package:redux/redux.dart';


final albumsDetailReducer = combineReducers<AlbumDetailState>([
  TypedReducer<AlbumDetailState, AlbumDetailLoadCompletAction>(_albumDetailLoadCompletAction),
  TypedReducer<AlbumDetailState, AddTaskCompleet>(_addSonginDownload)
]);

AlbumDetailState _albumDetailLoadCompletAction(AlbumDetailState state, AlbumDetailLoadCompletAction action) => state.copyWith(loadingStatus: LoadingStatus.success, tracks: action.tracks, album: action.album);
AlbumDetailState _addSonginDownload(AlbumDetailState state, AddTaskCompleet action) => state.songStartDownload(task: action.task);

