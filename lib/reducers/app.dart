import 'package:jrap_mobile/reducers/download.dart';
import 'package:jrap_mobile/reducers/download_task.dart';
import 'package:jrap_mobile/reducers/home.dart';
import 'package:jrap_mobile/reducers/search.dart';
import 'package:jrap_mobile/reducers/search_detail.dart';
import 'package:jrap_mobile/reducers/settings.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:jrap_mobile/reducers/album_detail.dart';
import 'package:jrap_mobile/reducers/albums.dart';
import 'package:jrap_mobile/reducers/artist_detail.dart';
import 'package:jrap_mobile/reducers/artists.dart';
import 'package:jrap_mobile/reducers/auth.dart';
import 'package:jrap_mobile/reducers/navigation.dart';
import 'package:jrap_mobile/reducers/player.dart';
import 'package:jrap_mobile/reducers/playlist_detail.dart';
import 'package:jrap_mobile/reducers/playlists.dart';
import 'package:jrap_mobile/reducers/realeses.dart';
import 'package:jrap_mobile/reducers/singls.dart';


AppState appReducer(AppState state, dynamic action) =>
    new AppState(
      authState: authReducer(state.authState,action),
      route: navigationReducer(state.route, action),
      albumsState: albumsReducer(state.albumsState,action),
      realesesState: realesesReducer(state.realesesState, action),
      singlsState: singlsReducer(state.singlsState,action),
      artistsState: artistsReducer(state.artistsState,action),
      albumDetailState: albumsDetailReducer(state.albumDetailState, action),
      playlistDetailState: playlistDetailReducer(state.playlistDetailState, action),
      playlistsState: playlistsReducer(state.playlistsState, action),
      artistDetailState: artistDetailReducer(state.artistDetailState, action),
      playerState: playerReducer(state.playerState, action),
      homeState: homeReducer(state.homeState, action),
      downloadState: downloadReducer(state.downloadState, action),
      downloadTaskState: downloadTaskReducer(state.downloadTaskState, action),
      searchState:  searchReducer(state.searchState, action),
      settingsState: settingsReducer(state.settingsState, action),
      searchDetailState: searchDetailReducer(state.searchDetailState,action)
    );