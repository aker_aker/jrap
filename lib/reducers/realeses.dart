import 'package:jrap_mobile/actions/realeses_actions.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/states/realeses.dart';
import 'package:redux/redux.dart';


final realesesReducer = combineReducers<RealesesState>([
  //TypedReducer<RealesesState, LoadRealesesAction>(_loadRealesesAction),
  TypedReducer<RealesesState, RealesesLoadCompletAction>(_realesesLoadCompletAction)
]);
/*
AuthState _validateEmail(AuthState state, ValidateEmailAction action){
  return state.copyWith(email: action.email);
}

AuthState _validatePassword(AuthState state, ValidatePasswordAction action) =>
    state.copyWith(password: action.password);

AuthState _validateLoginFieldsAction(AuthState state, ValidateLoginFields action) => state;

AuthState _validatePasswordMatch(AuthState state, ValidatePasswordMatchAction action) =>
    state.copyWith(password: action.password,retypePassword: action.confirmPassword);

AuthState _validateCodeAction(AuthState state, ValidateCodeAction action) =>
    state.copyWith(code: action.code);

AuthState _changeLoadingStatusAction(AuthState state, ChangeLoadingStatusAction action) =>
    state.copyWith(loadingStatus: action.status);

AuthState _emailErrorAction(AuthState state, EmailErrorAction action){
  if(action.screen == Screen.SIGNUP){
    return state.copyWith(emailError: action.message);
  }else return state;
}

AuthState _passwordErrorAction(AuthState state, PasswordErrorAction action){
  if (action.screen == Screen.SIGNUP) {
    return state.copyWith(passwordError: action.message);
  }else return state;
}

AuthState _retypePasswordErrorAction(AuthState state, RetypePasswordErrorAction action) {
  if (action.screen == Screen.SIGNUP) {
    return state.copyWith(retypePasswordError: action.message);
  }else return state;
}

AuthState _codeErrorAction(AuthState state, CodeErrorAction action) =>
    state.copyWith(codeError: action.message);

AuthState _changeGenderAction(AuthState state, ChangeGenderAction action) =>
    state.copyWith(gender: action.gender);

AuthState _signInAction(AuthState state, SignInAction action) => state;

AuthState _signUpAction(AuthState state, SignUpAction action) => state;

AuthState _saveToken(AuthState state, SaveTokenAction action) =>
    state.copyWith(token: action.token);

AuthState _requestCodeAction(AuthState state, RequestCodeAction action) => state;

AuthState _confirmCodeAction(AuthState state, ConfirmForgotPasswordAction action) => state;

AuthState _navigateToRegistrationAction(AuthState state,NavigateToRegistrationAction action) => state;
*/

RealesesState _loadRealesesAction(RealesesState state, LoadRealesesAction action) => state.copyWith(loadingStatus: LoadingStatus.loading);

RealesesState _realesesLoadCompletAction(RealesesState state, RealesesLoadCompletAction action) => state.copyWith(loadingStatus: action.loadingStatus, data: action.data);
