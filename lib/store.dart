import 'dart:async';
import 'package:jrap_mobile/middleware/albums_middleware.dart';
import 'package:jrap_mobile/middleware/artists_middleware.dart';
import 'package:jrap_mobile/middleware/download_middleware.dart';
import 'package:jrap_mobile/middleware/like_middleware.dart';
import 'package:jrap_mobile/middleware/local_storage_middleware.dart';
import 'package:jrap_mobile/middleware/navigation_middleware.dart';
import 'package:jrap_mobile/middleware/player_middleware.dart';
import 'package:jrap_mobile/middleware/playlists_middleware.dart';
import 'package:jrap_mobile/middleware/realeses_middleware.dart';
import 'package:jrap_mobile/middleware/search_middleware.dart';
import 'package:jrap_mobile/middleware/settings_meddleware.dart';
import 'package:jrap_mobile/middleware/singls_middleware.dart';
import 'package:jrap_mobile/middleware/task_middleware.dart';
import 'package:jrap_mobile/states/app_state.dart';
import 'package:redux/redux.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:jrap_mobile/reducers/app.dart';

Future<Store<AppState>> createStore() async
{
  var prefs = await SharedPreferences.getInstance();
  List<Middleware<AppState>> _midd = [
    LoggingMiddleware.printer(),
    LocalStorageMiddleware(prefs),
    RealesesMiddleware(),
    AlbumsMiddleware(),
    SinglsMiddleware(),
    ArtistsMiddleware(),
    PlaylistsMiddleware(),
    DownloadMiddleware(),
    SearchMiddleware(),
    LikeMiddleware(),
    TaskMiddleware(),
    SettingsMiddleware(),
    NavigationMiddleware(),
    PlayerMiddleware(),
  ];

  return Store(
    appReducer,
    initialState: AppState.initial(),
    middleware:_midd,
  );
}
