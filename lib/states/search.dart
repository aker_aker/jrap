

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/search_all.dart';

@immutable
class SearchState {
  final SearchAll data;
  final LoadingStatus loadingStatus;
  final String text;

  SearchState({
    @required this.loadingStatus,
    this.data,
    this.text
  });

  SearchState copyWith({
    LoadingStatus loadingStatus,
    SearchAll data,
    String text,
  }) {
    return new SearchState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: data ?? this.data,
        text: text ?? this.text
    );
  }

  factory SearchState.initial(){
    return  SearchState(
        loadingStatus: LoadingStatus.none,
        data: SearchAll(),
        text: ""
    );
  }
}