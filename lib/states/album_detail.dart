

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';
import 'package:jrap_mobile/models/task_info.dart';

@immutable
class AlbumDetailState {
  final List<Song> tracks;
  final Album album;
  final LoadingStatus loadingStatus;
  final LoadingStatus loadingPlaylistStatus;

  AlbumDetailState({
    @required this.loadingStatus,
    this.tracks,
    this.album,
    this.loadingPlaylistStatus
  });

  AlbumDetailState copyWith({
    LoadingStatus loadingStatus,
    List<Song> tracks,
    Album album,
    LoadingStatus loadingPlaylistStatus
  }) {

    if(tracks!=null && tracks.length > 0){
      album.IsPlaying = tracks.where((item)=>item.IsVkLoad).length > 0;
    }
    else{
      album.IsPlaying = false;
    }

    return AlbumDetailState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        tracks: tracks ?? this.tracks,
        album: album ?? this.album,
        loadingPlaylistStatus: loadingPlaylistStatus ?? this.loadingPlaylistStatus
    );
  }

  AlbumDetailState songStartDownload({
    DownloadTaskInfo task
  }) {

    if(task!=null){
      tracks.where((x)=>x.SongId == task.song.SongId).forEach((y)=>y.IsDownloading =true);
    }

    return  AlbumDetailState(
        loadingStatus: this.loadingStatus,
        tracks: this.tracks,
        album: this.album,
        loadingPlaylistStatus:this.loadingPlaylistStatus
    );
  }


  factory AlbumDetailState.initial(){
    return  AlbumDetailState(
        loadingStatus: LoadingStatus.loading,
        loadingPlaylistStatus: LoadingStatus.none,
        tracks: new List<Song>()
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AlbumDetailState &&
              runtimeType == other.runtimeType &&
              album == other.album &&
              tracks == other.tracks &&
              loadingPlaylistStatus == other.loadingPlaylistStatus &&
              loadingStatus == other.loadingStatus;

  @override
  int get hashCode =>
      album.hashCode ^
      tracks.hashCode ^
      loadingPlaylistStatus.hashCode ^
      loadingStatus.hashCode;
}