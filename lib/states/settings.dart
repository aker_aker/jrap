

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/setting.dart';

@immutable
class SettingsState {
  final List<SettingUser> data;
  final LoadingStatus loadingStatus;

  SettingsState({
    @required this.loadingStatus,
    this.data,
  });

  SettingsState copyWith({
    LoadingStatus loadingStatus,
    List<SettingUser> data,
  }) {
    return SettingsState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: data ?? this.data,
    );
  }

  factory SettingsState.initial(){
    return SettingsState(
        loadingStatus: LoadingStatus.none,
        data: [],
    );
  }
}