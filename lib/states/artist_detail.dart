import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';

@immutable
class ArtistDetailState {
  final List<Song> tracks;
  final List<Album> albums;
  final List<Album> singls;
  final Artist artist;
  final List<Artist> artist_related;
  final LoadingStatus loadingStatus;

  ArtistDetailState({
    @required this.loadingStatus,
    this.tracks,
    this.albums,
    this.singls,
    this.artist,
    this.artist_related
  });

  ArtistDetailState copyWith({
    LoadingStatus loadingStatus,
    List<Song> tracks,
    List<Album> albums,
    List<Album> singls,
    Artist artist,
    List<Artist> artist_related,
  }) {

    return ArtistDetailState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        tracks: tracks ?? this.tracks,
        albums: albums ?? this.albums,
        singls: singls ?? this.singls,
        artist: artist ?? this.artist,
        artist_related: artist_related ?? this.artist_related
    );
  }

  factory ArtistDetailState.initial(){
    return  ArtistDetailState(
        loadingStatus: LoadingStatus.loading,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is ArtistDetailState &&
              runtimeType == other.runtimeType &&
              artist == other.artist &&
              singls == other.singls &&
              albums == other.albums &&
              tracks == other.tracks &&
              loadingStatus == other.loadingStatus;

  @override
  int get hashCode =>
      artist.hashCode ^
      tracks.hashCode ^
      albums.hashCode ^
      singls.hashCode ^
      loadingStatus.hashCode;
}