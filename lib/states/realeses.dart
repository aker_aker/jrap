

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/realeses.dart';

@immutable
class RealesesState {
  final Realeses data;
  final LoadingStatus loadingStatus;

  RealesesState({
    @required this.loadingStatus,
    this.data
  });

  RealesesState copyWith({
    LoadingStatus loadingStatus,
    Realeses data,
  }) {
    return  RealesesState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: data ?? this.data
    );
  }

  factory RealesesState.initial(){
    return  RealesesState(
      loadingStatus: LoadingStatus.loading,
    );
  }
}