

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';

@immutable
class AlbumsState {
  final List<Album> data;
  final LoadingStatus loadingStatus;

  AlbumsState({
    @required this.loadingStatus,
    this.data
  });

  AlbumsState copyWith({
    LoadingStatus loadingStatus,
    List<Album> data,
  }) {
    if(data!=null)
      data.insertAll(0, this.data);

    return  AlbumsState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: data ?? this.data
    );
  }

  AlbumsState pullRefresh({
    LoadingStatus loadingStatus,
    List<Album> data,
  }) {

    var firestAlbumId = this.data[0].AlbumId;

    if(data != null)
    {
      for(var i = 0; i < data.length; i++){
        if(data[i].AlbumId > firestAlbumId)
        {
          this.data.insert(0, data[i]);
        }
      }
    }
    return  AlbumsState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: this.data
    );
  }

  factory AlbumsState.initial(){
    return  AlbumsState(
      loadingStatus: LoadingStatus.loading,
      data: new List<Album>()
    );
  }
}