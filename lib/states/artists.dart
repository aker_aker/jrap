

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/artist.dart';
import 'package:jrap_mobile/models/loading_status.dart';

@immutable
class ArtistsState {
  final List<Artist> data;
  final LoadingStatus loadingStatus;

  ArtistsState({
    @required this.loadingStatus,
    this.data
  });

  ArtistsState copyWith({
    LoadingStatus loadingStatus,
    List<Artist> data,
  }) {
    if(data!=null)
      data.insertAll(0, this.data);

    return new ArtistsState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: data ?? this.data
    );
  }

  factory ArtistsState.initial(){
    return  ArtistsState(
      loadingStatus: LoadingStatus.loading,
      data: new List<Artist>()
    );
  }
}