import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/screen_status.dart';
import 'package:jrap_mobile/models/user.dart';
import 'package:meta/meta.dart';
import 'package:vk_login_plugin/vk_login_plugin.dart';


@immutable
class AuthState {
  final LoadingStatus loadingStatus;
  final ScreenState type;
  final bool isAuthenticated;
  final User user;

  AuthState({
    @required this.loadingStatus,
    this.type,
    this.isAuthenticated,
    this.user,
  });


  factory AuthState.initial()
  {
    return new AuthState(
      loadingStatus: LoadingStatus.success,
      type: ScreenState.WELCOME,
      isAuthenticated: false,
    );
  }

  AuthState copyWith({
    LoadingStatus loadingStatus,
    ScreenState type,
    bool isAuthenticated,
    User user,
  }) {
    return  AuthState(
      loadingStatus: loadingStatus ?? this.loadingStatus,
      type: type ?? this.type,
      isAuthenticated: isAuthenticated ?? this.isAuthenticated,
      user: user ?? this.user,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is AuthState &&
              loadingStatus == other.loadingStatus;

  @override
  int get hashCode =>
      loadingStatus.hashCode;

}