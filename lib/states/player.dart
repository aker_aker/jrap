import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/info_queue.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/queue.dart';
import 'package:jrap_mobile/utils/enums.dart';

@immutable
class JRPlayerState {
  final Queue queue;
  final InfoQueue info;
  final RepeatMode mode;
  final ShuffleMode shuffleMode;
  final PlaybackState statePlayer;
  final bool playWhenReady;
  //final LoadingStatus loadingStatus;

  JRPlayerState({
    //@required this.loadingStatus,
    this.queue,
    this.info,
    this.mode,
    this.shuffleMode,
    this.statePlayer,
    this.playWhenReady
  });

  JRPlayerState copyWith({
    LoadingStatus loadingStatus,
    Queue queue,
    InfoQueue info,
    RepeatMode mode,
    ShuffleMode shuffleMode,
    PlaybackState statePlayer,
    bool playWhenReady,
  }) {
    return JRPlayerState(
        //loadingStatus: loadingStatus ?? this.loadingStatus,
        queue: queue ?? this.queue,
        info: info ?? this.info,
        mode: mode ?? this.mode,
        shuffleMode: shuffleMode ?? this.shuffleMode,
        statePlayer: statePlayer  ?? this.statePlayer,
        playWhenReady : playWhenReady ?? this.playWhenReady,
    );
  }

  factory JRPlayerState.initial(){
    return  JRPlayerState(
        //loadingStatus: LoadingStatus.loading,
        queue: Queue(),
        info: InfoQueue(),
        mode: RepeatMode.sequence,
        shuffleMode: ShuffleMode.noshuffle,
        statePlayer: PlaybackState.none,
        playWhenReady: false
    );
  }
}