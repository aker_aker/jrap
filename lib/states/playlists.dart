
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';

@immutable
class PlaylistsState {
  final List<Playlist> data;
  final LoadingStatus loadingStatus;

  PlaylistsState({
    @required this.loadingStatus,
    this.data
  });

  PlaylistsState copyWith({
    LoadingStatus loadingStatus,
    List<Playlist> data,
  }) {
    
    return  PlaylistsState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        data: data ?? this.data
    );
  }

  factory PlaylistsState.initial(){
    return  PlaylistsState(
        loadingStatus: LoadingStatus.loading,
        data: new List<Playlist>()
    );
  }
}