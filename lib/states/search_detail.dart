import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/search.dart';

@immutable
class SearchDetailState {
  final LoadingStatus loadingStatus;
  final String search;
  final List data;
  final SearchBlock<dynamic> block;

  SearchDetailState({
    @required this.loadingStatus,
    @required this.search,
    @required this.data,
    @required this.block,
  });

  SearchDetailState copyWith({
    String search,
    LoadingStatus loadingStatus,
    List data,
    SearchBlock block,
  }) {
    return SearchDetailState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        search: search ?? this.search,
        data: data ?? this.data,
        block: block ?? this.block
    );
  }

  factory SearchDetailState.initial(){
    return  SearchDetailState(
        loadingStatus: LoadingStatus.none,
        search: "",
        data: [],
        block: null,
    );
  }
}