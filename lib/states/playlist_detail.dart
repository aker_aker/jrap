

import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/playlist.dart';
import 'package:jrap_mobile/models/song.dart';

@immutable
class PlaylistDetailState {
  final List<Song> tracks;
  final Playlist playlist;
  final LoadingStatus loadingStatus;


  PlaylistDetailState({
    @required this.loadingStatus,
    this.tracks,
    this.playlist,
  });

  PlaylistDetailState copyWith({
    LoadingStatus loadingStatus,
    List<Song> tracks,
    Playlist playlist,
  }) {

    bool _isPlaying = false;
    if(tracks!=null && tracks.length > 0)
      _isPlaying = tracks.where((item)=>item.IsVkLoad).length > 0;

    playlist.IsPlaying = _isPlaying;

    return PlaylistDetailState(
        loadingStatus: loadingStatus ?? this.loadingStatus,
        tracks: tracks ?? this.tracks,
        playlist: playlist ?? this.playlist,
    );
  }

  factory PlaylistDetailState.initial(){
    return  PlaylistDetailState(
        loadingStatus: LoadingStatus.loading,
        tracks: new List<Song>()
    );
  }
}