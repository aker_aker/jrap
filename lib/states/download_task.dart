

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/models/download_task.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/task_info.dart';

@immutable
class DownloadTaskState {
  final List<DownloadTaskInfo> data_page;
  final LoadingStatus loadingStatus;

  DownloadTaskState({
    this.data_page,
    this.loadingStatus,
  });

  DownloadTaskState copyWith({
    List<DownloadTaskInfo> data,
    LoadingStatus loadingStatus,
  }) {

    return  DownloadTaskState(
        data_page: data ?? this.data_page,
        loadingStatus: loadingStatus ?? this.loadingStatus
    );
  }

  DownloadTaskState copyWithAfterDelete({
    String taskId
  }) {

    data_page.removeWhere((item)=> item.task.taskId == taskId);

    return  DownloadTaskState(
        data_page: data_page,
        loadingStatus: loadingStatus ?? this.loadingStatus
    );
  }


  DownloadTaskState copySingleWith({
   DownloadTaskInfo task
  }) {

    data_page.add(task);

    return  DownloadTaskState(
        data_page: this.data_page,
        loadingStatus: this.loadingStatus
    );
  }

  DownloadTaskState copyWithRefreshId({
    String taskId,
    String newTaskId,
  }) {

    for(var i =0; i < data_page.length; i++){
      if(data_page[i].task.taskId == taskId)
      {
        data_page[i].task.taskId = newTaskId;
        break;
      }
    }

    return  DownloadTaskState(
        data_page:  this.data_page,
        loadingStatus: this.loadingStatus
    );
  }

  DownloadTaskState copyWithRefreshTask({
    String taskId,
    DownloadTaskStatus status,
    int progress
  }) {

    for(var i =0; i < data_page.length; i++){
      if(data_page[i].task.taskId == taskId)
        {
          data_page[i].task.status = status;
          data_page[i].task.progress = progress;
          break;
        }
    }

    return  DownloadTaskState(
        data_page:  this.data_page,
        loadingStatus: this.loadingStatus
    );
  }

  factory DownloadTaskState.initial(){
    return  DownloadTaskState(
      data_page: List<DownloadTaskInfo>(),
      loadingStatus: LoadingStatus.loading,
    );
  }

  DownloadTaskState deleteCopyWith({
    TaskInfo task
  }) {
    data_page.removeWhere((item) => item.task.id == task.id);

    return  DownloadTaskState(
        data_page:  this.data_page,
        loadingStatus: this.loadingStatus
    );
  }


  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is DownloadTaskState &&
              runtimeType == other.runtimeType &&
              data_page == other.data_page &&
              loadingStatus == other.loadingStatus;

  @override
  int get hashCode =>
      data_page.hashCode ^
      loadingStatus.hashCode;

}