

import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:jrap_mobile/models/download_song.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/task_info.dart';

@immutable
class DownloadState {
  final List<DownloadSong> data_page;
  final LoadingStatus loadingStatus;

  DownloadState({
    this.data_page,
    this.loadingStatus,
  });

  DownloadState copyWith({
    List<DownloadSong> data_page,
    LoadingStatus loadingStatus,
  }) {
    return  DownloadState(
        data_page: data_page ?? this.data_page,
        loadingStatus: loadingStatus ?? this.loadingStatus
    );
  }

  factory DownloadState.initial(){
    return  DownloadState(
      data_page: List<DownloadSong>(),
      loadingStatus: LoadingStatus.loading,
    );
  }

  DownloadState deleteCopyWith({
    DownloadSong dsong
  }) {

    data_page.removeWhere((item) => item.song.SongId == dsong.song.SongId);

    return  DownloadState(
        data_page:  this.data_page,
        loadingStatus: this.loadingStatus
    );
  }



  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is DownloadState &&
              runtimeType == other.runtimeType &&
              data_page == other.data_page &&
              loadingStatus == other.loadingStatus;

  @override
  int get hashCode =>
      data_page.hashCode ^
      loadingStatus.hashCode;

}