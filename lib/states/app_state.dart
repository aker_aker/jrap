//import 'package:flutter_vk_login/flutter_vk_login.dart';
import 'package:jrap_mobile/routes.dart';
import 'package:jrap_mobile/states/albums.dart';
import 'package:jrap_mobile/states/artist_detail.dart';
import 'package:jrap_mobile/states/auth.dart';
import 'package:jrap_mobile/states/download.dart';
import 'package:jrap_mobile/states/download_task.dart';
import 'package:jrap_mobile/states/home.dart';
import 'package:jrap_mobile/states/player.dart';
import 'package:jrap_mobile/states/playlist_detail.dart';
import 'package:jrap_mobile/states/playlists.dart';
import 'package:jrap_mobile/states/realeses.dart';
import 'package:jrap_mobile/states/search.dart';
import 'package:jrap_mobile/states/search_detail.dart';
import 'package:jrap_mobile/states/settings.dart';
import 'package:jrap_mobile/states/singls.dart';
import 'package:jrap_mobile/states/artists.dart';
import 'package:jrap_mobile/states/album_detail.dart';
import 'package:meta/meta.dart';


@immutable
class AppState {

  final AuthState authState;
  final List<String> route;
  final RealesesState realesesState;
  final AlbumsState albumsState;
  final SinglsState singlsState;
  final ArtistsState artistsState;
  final AlbumDetailState albumDetailState;
  final PlaylistsState playlistsState;
  final PlaylistDetailState playlistDetailState;
  final ArtistDetailState artistDetailState;
  final JRPlayerState playerState;
  final HomeState homeState;
  final DownloadState downloadState;
  final DownloadTaskState downloadTaskState;
  final SearchState searchState;
  final SearchDetailState searchDetailState;
  final SettingsState settingsState;
  bool isLoading;

  AppState({
    this.isLoading = false,
	  @required this.authState,
    this.route = const [AppRoutes.login],
    @required this.realesesState,
    @required this.albumsState,
    @required this.singlsState,
    @required this.artistsState,
    @required this.albumDetailState,
    @required this.playlistDetailState,
    @required this.artistDetailState,
    @required this.playerState,
    @required this.playlistsState,
    @required this.homeState,
    @required this.downloadState,
    @required this.searchState,
    @required this.searchDetailState,
    @required this.downloadTaskState,
    @required this.settingsState,
  });
  
  factory AppState.initial(){
    return AppState(
      authState: AuthState.initial(),
      realesesState: RealesesState.initial(),
      albumsState:  AlbumsState.initial(),
      singlsState:  SinglsState.initial(),
      artistsState:  ArtistsState.initial(),
      albumDetailState: AlbumDetailState.initial(),
      playlistsState: PlaylistsState.initial(),
      playlistDetailState: PlaylistDetailState.initial(),
      artistDetailState: ArtistDetailState.initial(),
      playerState: JRPlayerState.initial(),
      homeState: HomeState.initial(),
      downloadState: DownloadState.initial(),
      downloadTaskState: DownloadTaskState.initial(),
      searchState: SearchState.initial(),
      settingsState: SettingsState.initial(),
      searchDetailState: SearchDetailState.initial()
    );
  }

   AppState copyWith({
    AuthState authState,
    RealesesState realesesState,
    AlbumsState albumsState,
    SinglsState singlsState,
    ArtistsState artistsState,
    AlbumDetailState albumDetailState,
    PlaylistsState playlistsState,
    PlaylistDetailState playlistDetailState,
    ArtistDetailState artistDetailState,
    JRPlayerState playerState,
    HomeState homeState,
    DownloadState downloadState,
    DownloadTaskState downloadTaskState,
    SearchState searchState,
    SettingsState settingsState,
    SearchDetailState searchDetailState,
  }){
    return AppState(
      authState: authState ?? this.authState,
      route: route ?? this.route,
      realesesState: realesesState ?? this.realesesState,
      albumsState: albumsState ?? this.albumsState,
      singlsState: singlsState ?? this.singlsState,
      artistsState: artistsState ?? this.artistsState,
      albumDetailState: albumDetailState ?? this.albumDetailState,
      playlistDetailState: playlistDetailState ?? this.playlistDetailState,
      playlistsState: playlistsState ?? this.playlistsState,
      artistDetailState: artistDetailState ?? this.artistDetailState,
      playerState: playerState ?? this.playerState,
      homeState: homeState ?? this.homeState,
      downloadState: downloadState ?? this.downloadState,
      downloadTaskState: downloadTaskState ?? this.downloadTaskState,
      searchState: searchState?? this.searchState,
      settingsState: settingsState?? this.settingsState,
      searchDetailState: searchDetailState ?? this.searchDetailState
    );
  }

@override
  bool operator ==(Object other) =>
    identical(this, other) ||
    other is AppState &&
    runtimeType == other.runtimeType &&
    authState == other.authState &&
    realesesState == other.realesesState &&
    albumsState == other.albumsState &&
    singlsState == other.singlsState &&
    artistsState == other.artistsState &&
    albumDetailState == other.albumDetailState &&
    playlistDetailState == other.playlistDetailState &&
    playlistsState== other.playlistsState &&
    artistDetailState == other.artistDetailState &&
    playerState == other.playerState &&
    homeState == other.homeState &&
    downloadState == other.downloadState &&
    downloadTaskState == other.downloadTaskState &&
    searchState == other.searchState &&
    settingsState == other.settingsState &&
    searchDetailState == other.searchDetailState &&
    route == other.route;

@override
int get hashCode =>
    authState.hashCode ^
    albumsState.hashCode ^
    realesesState.hashCode ^
    singlsState.hashCode ^
    artistsState.hashCode ^
    albumDetailState.hashCode ^
    playlistDetailState.hashCode ^
    artistDetailState.hashCode ^
    playerState.hashCode ^
    playlistsState.hashCode ^
    downloadState.hashCode ^
    downloadTaskState.hashCode ^
    searchState.hashCode ^
    settingsState.hashCode ^
    searchDetailState.hashCode ^
    route.hashCode;
}