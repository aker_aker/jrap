
import 'package:flutter/material.dart';
import 'package:jrap_mobile/models/album.dart';
import 'package:jrap_mobile/models/loading_status.dart';
import 'package:jrap_mobile/models/song.dart';

@immutable
class HomeState {
  final int tabIndex;
  final int curTabIndex;
  final bool isPlayer;
  final bool isOffline;

  HomeState({
    this.tabIndex,
    this.curTabIndex,
    this.isPlayer,
    this.isOffline,
  });

  HomeState copyWith({
    int tabIndex,
    int curTabIndex,
    double position,
    bool isPlayer,
    bool isOffline
  }) {

    return HomeState(
        tabIndex: tabIndex ?? this.tabIndex,
        curTabIndex: curTabIndex ?? this.curTabIndex,
        isPlayer: isPlayer ?? this.isPlayer,
        isOffline: isOffline ?? this.isOffline
    );
  }

  factory HomeState.initial(){
    return  HomeState(
        tabIndex: 0,
        curTabIndex:0,
        isPlayer: false,
        isOffline: false
    );
  }
}